﻿var RHandler, resultG = new Array,
    Obook = null,
    Rbook = null,
    ObookSFM = null,
    RbookSFM = null,
    divclspmatrix = 0,
    rtfResult = null,
    lccRtfResult = null,
    gdsRtfResult = null,
    rtfResultArray = null,
    lccRtfResultArray = null,
    gdsRtfResultArray = null,
    CommonResultArray = new Array,
    CommanRTFArray = new Array,
    rStatus = 0,
    isSRF = !1,
    totprevFare = 0,
    totcurrentFare = 0,
    totcurrentFareSFM = 0,
    ORTFFare = 0,
    RRTFFare = 0,
    ORTFLineNo = "",
    RRTFLineNo = "",
    ORTFVC = "",
    RRTFVC = "",
    roundtripNrml1 = !1,
    applyfilterStatus = !1,
    gdsJsonRTF = new Array,
    RTFFirstTime = 0,
    SRFResultAfterReprice = new Array,
    SRFReprice = !1;

function DiplayMsearch1(e) {
    $("#" + e).show()
}

function DiplayMsearch(e) {
    $(".fade").each(function () {
        $(this).hide()
    })
}

function mtrxx() {
    if (null != Obook) {
        $(".list-item").each(function () {
            $(this).find(".mrgbtm").removeClass("fltbox01").addClass("fltbox"), $(this).find(".mrgbtmG").removeClass("fltbox02")
        });
        var e = Obook[0].LineNumber.split("api")[0];
        $("#main_" + e + "_O").removeClass("fltbox").addClass("fltbox01"), $("#main1_" + e + "_O").addClass("fltbox02"), "checked" != $("#main_" + e + "_O").find("input[type='radio']").attr("checked") && $("#main_" + e + "_O").find("input[type='radio']").attr("checked", "checked"), "checked" != $("#main1_" + e + "_O").find("input[type='radio']").attr("checked") && $("#main1_" + e + "_O").find("input[type='radio']").attr("checked", "checked")
    }
    if (null != Rbook) {
        $(".list-itemR").each(function () {
            $(this).find(".mrgbtm").removeClass("fltbox01").addClass("fltbox"), $(this).find(".mrgbtmG").removeClass("fltbox02")
        });
        var t = Rbook[0].LineNumber.split("api1")[0];
        $("#main_" + t + "_R").removeClass("fltbox").addClass("fltbox01"), $("#main_" + t + "_R").find("input[type='radio']").attr("checked", "checked"), $("#main1_" + t + "_R").addClass("fltbox02"), $("#main1_" + t + "_R").find("input[type='radio']").attr("checked", "checked")
    }
}
$(document).ready(function () {
    (RHandler = new ResHelper).BindEvents()
}), $(document).ready(function () {
    $("#ModifySearch").click(function () {
        $("#Modsearch").slideDown()
    }), $("#mdclose").click(function () {
        $("#Modsearch").slideUp()
    })
});
var ResHelper = function () {
    this.flight = $("flight"), this.txtDepCity1 = $("#txtDepCity1"), this.txtArrCity1 = $("#txtArrCity1"), this.btnSearch = $("#btnSearch"), this.hidtxtDepCity1 = $("#hidtxtDepCity1"), this.hidtxtArrCity1 = $("#hidtxtArrCity1"), this.rooms = $("#rooms"), this.rdbOneWay = $("#rdbOneWay"), this.rdbRoundTrip = $("#rdbRoundTrip"), this.txtDepDate = $("#txtDepDate"), this.txtRetDate = $("#txtRetDate"), this.hidtxtDepDate = $("#hidtxtDepDate"), this.hidtxtRetDate = $("#hidtxtRetDate"), this.trRetDateRow = $("#trRetDateRow"), this.TripType = $("input[name=TripType]:checked"), this.Adult = $("#Adult"), this.Child = $("#Child"), this.Infant = $("#Infant"), $("#sapnTotPax").val(parseInt($(this.Adult).val()) + parseInt($(this.Child).val()) + parseInt($(this.Infant).val()) + " Traveller"), this.Cabin = $("#Cabin"), this.txtAirline = $("#txtAirline"), this.hidtxtAirline = $("#hidtxtAirline"), this.chkNonstop = $("#chkNonstop"), this.chkAdvSearch = $("#chkAdvSearch"), this.trAdvSearchRow = $("#trAdvSearchRow"), this.LCC_RTF = $("#LCC_RTF"), this.GDS_RTF = $("#GDS_RTF"), this.DivResult = $("#divResult"), this.searchquery = $("#searchquery"), this.airlineFilter = $("#airlineFilter"), this.stopFilter = $("#stopFlter"), this.airlineFilterR = $("#airlineFilterR"), this.stopFilterR = $("#stopFlterR"), this.divFromR = $("#divFrom1"), this.divToR = $("#divTo1"), this.RoundTripH = $("#RoundTripH"), this.OnewayH = $("#onewayH"), this.MainSF = $("#MainSF"), this.MainSFR = $("#MainSFR"), this.flterO = $(".fo"), this.flterR = $(".fr"), this.flterTab = $("#flterTab"), this.flterTabO = $("#flterTabO"), this.flterTabR = $("#flterTabR"), this.RadioSelect = $("input:radio"), this.RtfFltSelectDiv = $("#fltselct"), this.RtfFltSelectDivO = $("#fltgo"), this.RtfFltSelectDivR = $("#fltbk"), this.RtfTotalPayDiv = $("#totalPay"), this.RtfBookBtn = $("#FinalBook"), this.PrevDaySrch = $("#PrevDay"), this.NextDaySrch = $("#NextDay"), this.SearchTextDiv = $("#divSearchText"), this.RTFTextFrom = $("#RTFTextFrom"), this.RTFTextTo = $("#RTFTextTo"), this.DivMatrix = $("#divMatrix"), this.DivMatrixRtfO = $("#divMatrixRtfO"), this.DivMatrixRtfR = $("#divMatrixRtfR"), this.DivRefinetitle = $("#refinetitle"), this.SearchTextDiv1 = $("#divSearchText1"), this.fltbox12 = $(".fltbox"), this.clspMatrix = $("#clspMatrix"), this.divMatrix = $(".divMatrix"), this.divMatrixO = $("#divMatrix"), this.hdnOnewayOrRound = $("#hdnOnewayOrRound"), this.RtfFromPrevDay = $("#RtfFromPrevDay"), this.RtfFromNextDay = $("#RtfFromNextDay"), this.RtfToPrevDay = $("#RtfToPrevDay"), this.RtfToNextDay = $("#RtfToNextDay"), this.prexnt = $("#prexnt"), this.rtfResultDiv = $("#rtfResultDiv"), this.lccRTFDiv = $("#lccRTFDiv"), this.gdsRTFDiv = $("#gdsRTFDiv"), this.divFromResult = $("#divFrom"), this.fltrDiv = $("#lftdv1"), this.DivLoadP = $("#DivLoadP"), this.DivColExpnd = $("#lftdv"), this.DisplaySearchinput = $("#displaySearchinput"), this.nextdate = $("#next-date"), this.prevdate = $("#prev-date"), this.ondt = $("#on-dt"), this.rndt = $("#rn-dt"), this.rpdt = $("#rp-dt"), this.opdt = $("#op-dt"), this.txtDepCity2 = $("#txtDepCity2"), this.hidtxtDepCity2 = $("#hidtxtDepCity2"), this.txtArrCity2 = $("#txtArrCity2"), this.hidtxtArrCity2 = $("#hidtxtArrCity2"), this.txtDepDate2 = $("#txtDepDate2"), this.txtDepCity3 = $("#txtDepCity3"), this.hidtxtDepCity3 = $("#hidtxtDepCity3"), this.txtArrCity3 = $("#txtArrCity3"), this.hidtxtArrCity3 = $("#hidtxtArrCity3"), this.txtDepDate3 = $("#txtDepDate3"), this.txtDepCity4 = $("#txtDepCity4"), this.hidtxtDepCity4 = $("#hidtxtDepCity4"), this.txtArrCity4 = $("#txtArrCity4"), this.hidtxtArrCity4 = $("#hidtxtArrCity4"), this.txtDepDate4 = $("#txtDepDate4"), this.txtDepCity5 = $("#txtDepCity5"), this.hidtxtDepCity5 = $("#hidtxtDepCity5"), this.txtArrCity5 = $("#txtArrCity5"), this.hidtxtArrCity5 = $("#hidtxtArrCity5"), this.txtDepDate5 = $("#txtDepDate5"), this.txtDepCity6 = $("#txtDepCity6"), this.hidtxtDepCity6 = $("#hidtxtDepCity6"), this.txtArrCity6 = $("#txtArrCity6"), this.hidtxtArrCity6 = $("#hidtxtArrCity6"), this.txtDepDate6 = $("#txtDepDate6"), this.AirlineFareType = $("#AirlineFareType"), this.AirlineFareTypeR = $("#AirlineFareTypeR")
};

function Close(e) {
    $("#" + e).hide()
}

function getTheDays(e) {
    var t = e,
        i = t.getMonth(),
        a = t.getFullYear(),
        r = t.getDate(),
        s = new Date(a, i, r),
        l = new Array;
    l[0] = "Sun", l[1] = "Mon", l[2] = "Tue", l[3] = "Wed", l[4] = "Thu", l[5] = "Fri", l[6] = "Sat";
    var n = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    return l[s.getDay()] + ", " + r + " " + n[i]
}

function lzw_decode(e) {
    for (var t, i = {}, a = (e + "").split(""), r = a[0], s = r, l = [r], n = 256, o = 1; o < a.length; o++) {
        var d = a[o].charCodeAt(0);
        t = d < 256 ? a[o] : i[d] ? i[d] : s + r, l.push(t), r = t.charAt(0), i[n] = s + r, n++, s = t
    }
    return l.join("")
}

function str2ab(e) {
    for (var t = new ArrayBuffer(2 * e.length), i = new Uint16Array(t), a = 0, r = e.length; a < r; a++) i[a] = e.charCodeAt(a);
    return t
}

function toggleFD(e) {
    $("#" + e).is(":visible") ? ($("#BIn_" + e).hide(), $("#FD_" + e).hide()) : $("#" + e).slideToggle()
}

function displayFS(e, t) {
    if ($(".selectbtn").removeClass("selectbtn"), $(t).addClass("selectbtn"), $(".summary").hide(), $.trim(e).search("RTF") > 0) var i = (s = e.split("_"))[2] + "_" + s[3],
        a = JSLINQ(CommanRTFArray).Where(function (e) {
            return e.AirLineName == i.replace("-", " ")
        }).Select(function (e) {
            return e.fltJson
        }),
        r = JSLINQ(a.items[0][0]).Where(function (e) {
            return e.LineNumber == s[1]
        }).Select(function (e) {
            return e
        });
    else {
        var s = e.split("_");
        a = JSLINQ(CommonResultArray[0]).Where(function (e) {
            return e.AirLineName == s[2].replace("-", " ")
        }).Select(function (e) {
            return e
        });
        if (4 == s.length && "R" == s[3]) a = JSLINQ(CommonResultArray[1]).Where(function (e) {
            return e.AirLineName == s[2].replace("-", " ")
        }).Select(function (e) {
            return e
        }), r = JSLINQ(a.items).Where(function (e) {
            return e.LineNumber == s[1]
        }).Select(function (e) {
            return e
        });
        else r = JSLINQ(a.items).Where(function (e) {
            return e.LineNumber == s[1]
        }).Select(function (e) {
            return e
        })
    }
    return strResult = "<table border='0' cellpadding='0' cellspacing='0' id='FareBreak' class='table table-striped breakup w100'>", strResult += "<tr>", strResult += "<th>Pax Type</th>", strResult += "<th>Base Fares</th>", strResult += "<th>Fuel Surcharge</th>", strResult += "<th>Other Tax</td>", strResult += "<th>Total Fare</th></tr></thead>", strResult += "<tbody><tr><td class='bld'>ADT</td>", strResult = strResult + "<td>" + r.items[0].AdtBfare + "</td>", strResult = strResult + "<td>" + r.items[0].AdtFSur + "</td>", strResult = strResult + "<td>" + parseFloat(r.items[0].AdtTax - r.items[0].AdtFSur) + "</td>", strResult = strResult + "<td>" + parseFloat(r.items[0].AdtFare) + "</td></tr>", r.items[0].Child > 0 && (strResult += "<tr><td class='bld'>CHD</td>", strResult = strResult + "<td>" + r.items[0].ChdBFare + "</td>", strResult = strResult + "<td>" + r.items[0].ChdFSur + "</td>", strResult = strResult + "<td>" + parseFloat(r.items[0].ChdTax - r.items[0].ChdFSur) + "</td>", strResult = strResult + "<td>" + parseFloat(r.items[0].ChdFare) + "</td></tr>"), r.items[0].Infant > 0 && (strResult += "<tr><td class='bld'>INF</td>", strResult = strResult + "<td>" + r.items[0].InfBfare + "</td>", strResult = strResult + "<td>" + r.items[0].InfFSur + "</td>", strResult = strResult + "<td>" + parseFloat(r.items[0].InfTax - r.items[0].InfFSur) + "</td>", strResult = strResult + "<td>" + r.items[0].InfFare + "</td></tr>"), strResult += "<tr Style='display:none'>", strResult = strResult + "<td><span class='bld1'>Service Tax:</span><br />" + r.items[0].STax + "</td>", strResult += "<td><span class='bld1'>Tran. Fee:</span><br />0</td>", strResult = strResult + "<td><span class='bld1'>Commission:</span><br />" + r.items[0].TotDis + "</td>", strResult = strResult + "<td><span class='bld1'>Cash Back:</span><br />" + r.items[0].TotCB + "</td>", strResult = strResult + "<td><span class='bld1'>TDS:</span><br />" + r.items[0].TotTds + "</td>", strResult += "</tr>", strResult += "<tr>", strResult = strResult + "<td colspan='4' class='bld1'>" + r.items[0].Adult + " ADT,&nbsp;&nbsp;&nbsp; " + r.items[0].Child + " CHD,&nbsp;&nbsp;&nbsp; " + r.items[0].Infant + " INF &nbsp;&nbsp;&nbsp;</td>", strResult = strResult + "<td colspan='' class='text-right blue'>Total Fare: " + r.items[0].TotalFare + "</td>", strResult += "</tr>", strResult += "<tr>", strResult += "<td colspan='4' class='bld1'>&nbsp;</td>", strResult = strResult + "<td colspan='' class='text-right blue'>Net Fare: " + r.items[0].NetFare + "</td>", strResult += "</tr>", strResult += "</tbody>", strResult += "</table>", $("#" + e).html(strResult), $("#" + e).show(), !1
}

function displayFDt(t, i) {
    $(".selectbtn").removeClass("selectbtn"), $(i).addClass("selectbtn"), $(".summary").hide();
    var a = "";
    if (4 == (m = t.split("_")).length)
        if ("RTF" == m[3]) {
            var r = (m = t.split("_"))[2] + "_" + m[3],
                s = JSLINQ(gdsJsonRTF).Where(function (e) {
                    return e.fltName == r
                }).Select(function (e) {
                    return e
                }),
                l = JSLINQ(s.items[0][0]).Where(function (e) {
                    return e.LineNumber == m[1]
                }).Select(function (e) {
                    return e
                }),
                n = JSLINQ(l.items).Where(function (e) {
                    return "1" == e.Flight
                }).Select(function (e) {
                    return e
                }).items,
                o = JSLINQ(l.items).Where(function (e) {
                    return "2" == e.Flight
                }).Select(function (e) {
                    return e
                }).items;
            if (a = "<div>", n.length > 0) {
                a += '<div class="depcity">';
                for (var d = 0; d < n.length; d++) a += '<div class="lft w25"><img alt="" src="' + UrlBase + "/Images/AirLogo/sm" + n[d].MarketingCarrier + '.gif" /><br />' + n[d].MarketingCarrier + " - " + n[d].FlightIdentification + "</div>", a += '<div class="lft w25"><span>' + n[d].DepartureLocation + "&nbsp;" + [n[d].DepartureTime.replace(":", "").slice(0, 2), ":", n[d].DepartureTime.replace(":", "").slice(2)].join("") + "</span><br />" + n[d].DepartureCityName + "<br />" + n[d].Departure_Date + "<br />" + n[d].DepartureAirportName + " Airport, Terminal " + n[d].DepartureTerminal + "</div>", a += '<div class="lft w25 dvsrc"><i class="fa fa-plane-departure"></i><br/> ' + n[0].TotDur + "</div>", a += '<div class="lft w25"><span>' + [n[d].ArrivalTime.replace(":", "").slice(0, 2), ":", n[d].ArrivalTime.replace(":", "").slice(2)].join("") + "&nbsp;" + n[d].ArrivalLocation + "</span><br />" + n[d].ArrivalCityName + "<br />" + n[d].Arrival_Date + "<br />" + n[d].ArrivalAirportName + " Airport, Terminal  " + n[d].ArrivalTerminal + '</div><div class="clear"></div>';
                a += "</div>"
            }
            if (o.length > 0) {
                a += '<div class="depcity">';
                for (var c = 0; c < o.length; c++) a += '<div class="lft w25"><img alt="" src="' + UrlBase + "/Images/AirLogo/sm" + o[c].MarketingCarrier + '.gif" /><br />' + o[c].MarketingCarrier + " - " + o[c].FlightIdentification + "</div>", a += '<div class="lft w25"><span>' + o[c].DepartureLocation + "&nbsp;" + [o[c].DepartureTime.replace(":", "").slice(0, 2), ":", o[c].DepartureTime.replace(":", "").slice(2)].join("") + "</span><br />" + o[c].DepartureCityName + "<br />" + o[c].Departure_Date + "<br />" + o[c].DepartureAirportName + " Airport, Terminal " + o[c].DepartureTerminal + "</div>", a += '<div class="lft w25 dvsrc"><i class="fa fa-plane-departure"></i><br/> ' + o[0].TotDur + "</div>", a += '<div class="lft w25"><span>' + [o[c].ArrivalTime.replace(":", "").slice(0, 2), ":", o[c].ArrivalTime.replace(":", "").slice(2)].join("") + "&nbsp;" + o[c].ArrivalLocation + "</span><br />" + o[c].ArrivalCityName + "<br />" + o[c].Arrival_Date + "<br />" + o[c].ArrivalAirportName + " Airport, Terminal  " + o[c].ArrivalTerminal + '</div><div class="clear"></div>';
                a += "</div>"
            }
            a += '</div><div class="clear"></div>'
        } else {
            var p;
            if ("O" == $.trim(m[3])) {
                s = JSLINQ(CommonResultArray[0]).Where(function (e) {
                    return e.AirLineName == m[2].replace("-", " ")
                }).Select(function (e) {
                    return e
                });
                p = JSLINQ(s.items).Where(function (e) {
                    return e.LineNumber == m[1]
                }).Select(function (e) {
                    return e
                })
            } else if ("R" == $.trim(m[3])) {
                s = JSLINQ(CommonResultArray[1]).Where(function (e) {
                    return e.AirLineName == m[2].replace("-", " ")
                }).Select(function (e) {
                    return e
                });
                p = JSLINQ(s.items).Where(function (e) {
                    return e.LineNumber == m[1]
                }).Select(function (e) {
                    return e
                }), tripsatus = "R"
            }
            n = JSLINQ(p.items).Where(function (e) {
                return "1" == e.Flight
            }).Select(function (e) {
                return e
            }), o = JSLINQ(p.items).Where(function (e) {
                return "2" == e.Flight
            }).Select(function (e) {
                return e
            });
            if (a += '<div style="">', n.items.length > 0) {
                try {
                    parseInt(n.items[0].AvailableSeats1) <= 5 && (a += '<div class="colorwht lft" style="background:#43566f; padding:2px 5px; border-radius:4px; position:relative; top:6px;">' + n.items[0].AvailableSeats1 + ' Seat(s) Left!</div><div class="clear1"></div>')
                } catch (e) { }
                a += '<div class="depcity">';
                for (d = 0; d < n.items.length; d++) a += 0 == d ? '<div class="lft w25"><div><span class="">' + n.items[0].DepartureCityName + "-" + n.items[n.items.length - 1].ArrivalCityName + '</span>&nbsp;<span class="">' + MakeupTotDur(n.items[0].TotDur) + '</span></div><div class="clear1"></div>' : '<div class="clear1"></div><hr /><div class="clear1"></div><div class="lft w24">', a += '<div class="lft w50"><img alt="" src="' + UrlBase + "/Images/AirLogo/sm" + n.items[d].MarketingCarrier + '.gif" /><br />' + n.items[d].MarketingCarrier + " - " + n.items[d].FlightIdentification + "</div>", a += '<div class="lft w50 bld textaligncenter"><p>' + calFlightDur(n.items[d].DepartureTime.replace(":", ""), n.items[d].ArrivalTime.replace(":", "")) + " HRS</p></div></div>", a += '<div class="lft w25" style="text-align:right;"><span>' + n.items[d].DepartureLocation + "&nbsp;" + [n.items[d].DepartureTime.replace(":", "").slice(0, 2), ":", n.items[d].DepartureTime.replace(":", "").slice(2)].join("") + "</span><br />" + n.items[d].DepartureCityName + "<br />" + n.items[d].Departure_Date + "<br />" + TerminalAirportInfo(n.items[d].DepartureTerminal, n.items[d].DepartureAirportName) + "</div>", a += '<div class="lft w25 dvsrc"><i class="fa fa-plane-departure"></i></div>', a += '<div class="lft w25"><span>' + [n.items[d].ArrivalTime.replace(":", "").slice(0, 2), ":", n.items[d].ArrivalTime.replace(":", "").slice(2)].join("") + "&nbsp;" + n.items[d].ArrivalLocation + "</span><br />" + n.items[d].ArrivalCityName + "<br />" + n.items[d].Arrival_Date + "<br />" + TerminalAirportInfo(n.items[d].ArrivalTerminal, n.items[d].ArrivalAirportName) + '</div><div class="clear"></div>'
            }
            if (o.items.length > 0) {
                a += '</div><div class="depcity1"><span>' + o.items[0].DepartureCityName + "-" + o.items[o.items.length - 1].ArrivalCityName + "</span>&nbsp;" + o.items[0].TotDur + '<div class="clear"></div>';
                for (c = 0; c < o.items.length; c++) a += '<div class="lft w25"><img alt="" src="' + UrlBase + "/Images/AirLogo/sm" + o.items[c].MarketingCarrier + '.gif" /><br />' + o.items[c].MarketingCarrier + " - " + o.items[c].FlightIdentification + "</div>", a += '<div class="lft w10 f20 bld textaligncenter"><p>' + e.calFlightDur(o.items[c].DepartureTime.replace(":", ""), o.items[c].ArrivalTime.replace(":", "")) + " HRS</p></div>", a += '<div class="lft w25" style="text-align:right;"><span>' + o.items[c].DepartureLocation + "&nbsp;" + [o.items[c].DepartureTime.replace(":", "").slice(0, 2), ":", o.items[c].DepartureTime.replace(":", "").slice(2)].join("") + "</span><br />" + o.items[c].DepartureCityName + "<br />" + o.items[c].Departure_Date + "<br />" + TerminalAirportInfo(o.items[c].DepartureTerminal, o.items[c].DepartureAirportName) + "</div>", a += '<div class="lft w10 dvsrc"><i class="fa fa-plane-departure"></i></div>', a += '<div class="lft w25"><span>' + [o.items[c].ArrivalTime.replace(":", "").slice(0, 2), ":", o.items[c].ArrivalTime.replace(":", "").slice(2)].join("") + "&nbsp;" + o.items[c].ArrivalLocation + "</span><br />" + o.items[c].ArrivalCityName + "<br />" + o.items[c].Arrival_Date + "<br />" + TerminalAirportInfo(o.items[c].ArrivalTerminal, o.items[c].ArrivalAirportName) + '</div><div class="clear"></div>'
            }
            a += '<div class="clear"></div>', a += "</div></div>", a += "</div>", a += "</div>", a += '<div class="clear"></div>'
        }
    else {
        r = (m = t.split("_"))[2] + "_" + m[3], r = (m = t.split("_"))[2] + "_" + m[3], s = JSLINQ(CommonResultArray[0]).Where(function (e) {
            return e.AirLineName == m[2].replace("-", " ")
        }).Select(function (e) {
            return e
        }), l = JSLINQ(s.items).Where(function (e) {
            return e.LineNumber == m[1]
        }).Select(function (e) {
            return e
        });
        for (var m, u = JSLINQ(l.items).Select(function (e) {
            return e.Flight
        }).items.unique(), h = (a = "<div>", 1); h <= u.length; h++) {
            if ((n = JSLINQ(l.items).Where(function (e) {
                return e.Flight == h.toString()
            }).Select(function (e) {
                return e
            }).items).length > 0) {
                a += '<div class="depcity">';
                for (d = 0; d < n.length; d++) a += '<div class="w25 lft"><img alt="" src="' + UrlBase + "/Images/AirLogo/sm" + n[d].MarketingCarrier + '.gif" /><br />' + n[d].MarketingCarrier + " - " + n[d].FlightIdentification + "</div>", a += '<div class="lft w25"><span>' + n[d].DepartureLocation + "&nbsp;" + [n[d].DepartureTime.replace(":", "").slice(0, 2), ":", n[d].DepartureTime.replace(":", "").slice(2)].join("") + "</span><br />" + n[d].DepartureCityName + "<br />" + n[d].Departure_Date + "<br />" + n[d].DepartureAirportName + " Airport, Terminal " + n[d].DepartureTerminal + "</div>", a += '<div class="lft w25 dvsrc"><i class="fa fa-plane-departure"></i><br/>' + n[0].TotDur + "</div>", a += '<div class="lft w25"><span>' + [n[d].ArrivalTime.replace(":", "").slice(0, 2), ":", n[d].ArrivalTime.replace(":", "").slice(2)].join("") + "&nbsp;" + n[d].ArrivalLocation + "</span><br />" + n[d].ArrivalCityName + "<br />" + n[d].Arrival_Date + "<br />" + n[d].ArrivalAirportName + " Airport, Terminal  " + n[d].ArrivalTerminal + '</div><div class="clear"></div>';
                a += "</div>"
            }
        }
        a += '</div><div class="clear"></div>'
    }
    return $("#" + t).html(a), $("#" + t).show(), !1
}

function displayBIn(e, t) {
    event.preventDefault();
    $(".selectbtn").removeClass("selectbtn"), $(t).addClass("selectbtn");
    var i = new ResHelper;
    $(".summary").hide(), $("#" + e).show();
    var a, r = e.split("_");
    if ("O" == $.trim(r[3])) {
        var s = JSLINQ(CommonResultArray[0]).Where(function (e) {
            return e.AirLineName == r[2].replace("-", " ")
        }).Select(function (e) {
            return e
        });
        a = JSLINQ(s.items).Where(function (e) {
            return e.LineNumber == r[1]
        }).Select(function (e) {
            return e
        })
    } else if ("R" == $.trim(r[3])) {
        s = JSLINQ(CommonResultArray[1]).Where(function (e) {
            return e.AirLineName == r[2].replace("-", " ")
        }).Select(function (e) {
            return e
        });
        a = JSLINQ(s.items).Where(function (e) {
            return e.LineNumber == r[1]
        }).Select(function (e) {
            return e
        }), tripsatus = "R"
    }
    var l = JSLINQ(a.items).Where(function (e) {
        return "1" == e.Flight
    }).Select(function (e) {
        return e
    }),
        n = JSLINQ(a.items).Where(function (e) {
            return "2" == e.Flight
        }).Select(function (e) {
            return e
        }),
        o = "<div>";
    if (l.items.length > 0) {
        o += '<div class=""><span style="font-size:20px;display:none; float:right; position:relative; top:-5px; right:-15px; cursor:pointer; height:1px;" onclick="Close(\'' + this.rel + '_\');" title="Click to close Details"><i class="fa fa-times-circle"></i></span><div></div>', o += '<table class="w100 f12"><tr><td  class="w50 f16 bld">Sector</td><td class="f16 bld">Baggage Quantity</td></tr>';
        for (var d = 0; d < l.items.length; d++) o += "<tr><td>" + l.items[d].DepartureCityName + "-" + l.items[d].ArrivalCityName + "(" + l.items[d].MarketingCarrier + " - " + l.items[d].FlightIdentification + ")</td>", o += "<td>" + i.BagInfo(l.items[d].BagInfo) + "</td></tr>";
        o += "</table></div>"
    }
    if (n.items.length > 0) {
        o += '<div class="depcity1">', o += '<table class="w100 f12">';
        for (var c = 0; c < n.items.length; c++) o += '<tr><td class="w50">' + n.items[c].DepartureCityName + "-" + n.items[c].ArrivalCityName + "(" + n.items[c].MarketingCarrier + " - " + n.items[c].FlightIdentification + ")</td>", o += "<td>" + i.BagInfo(n.items[c].BagInfo) + "</td></tr>";
        o += "</table></div>"
    }
    o += '<div class="padding1 f10 w95 mauto lh13">The information presented above is as obtained from the airline reservation system. RWT does not guarantee the accuracy of this information. The baggage allowance may vary according to stop-overs, connecting flights and changes in airline rules.</div>', o += '<div class="clear1"></div>', $("#" + e).html(o)
}

function SelectedSector(e, t) {
    var i, a, r;
    for (a = document.getElementsByClassName("tabcontent"), i = 0; i < a.length; i++) a[i].style.display = "none";
    for (r = document.getElementsByClassName("tablinks"), i = 0; i < r.length; i++) r[i].className = r[i].className.replace(" active", "");
    document.getElementById(t).style.display = "block", e.currentTarget.className += " active"
}

function MakeTabNrmAndSrf() {
    $("#RTFSAirMain").show(), $("#RTFSAir").show(), $("#splLoading").hide(), 0 == RTFFirstTime && ($("#RTFSAir").html('<div id="DivNormalfare" onclick="RTFResultShowHide(\'DivNormalfare\');" class="curve rtfpanel1 bld w15">Normal Fare</div>&nbsp;&nbsp;<div id="DivTabRTF" onclick="RTFResultShowHide(\'DivTabRTF\');" class="curve rtfpanel bld w15">SRF Fare</div>'), RTFFirstTime = 1)
}

function RTFResultShowHide(e) {
    BlockUI();
    try {
        $(".rtfpanel1").addClass("rtfpanel"), $(".curve").removeClass("rtfpanel1"), $("#" + $.trim(e)).addClass("rtfpanel1"), setTimeout(function () {
            "DivNormalfare" == $.trim(e) ? ($(".nrmResult").show(), $(".srtfResult").hide()) : ($(".nrmResult").hide(), $(".srtfResult").hide(), $("." + e).show()), $.unblockUI()
        }, 100)
    } catch (e) {
        $.unblockUI()
    }
}

function BlockUI() {
    $.blockUI({
        message: $("#waitMessage")
    })
}

function GetUniqueAirlineRtf(e, t, i) {
    $("#RTFSAirMain").show(), $("#RTFSAir").show(), $("#splLoading").hide(), 0 == RTFFirstTime && ($("#RTFSAir").html('<div id="DivNormalfare" onclick="RTFResultShowHide(\'DivNormalfare\');" class="curve rtfpanel1 bld w15">Normal Fare</div>'), RTFFirstTime = 1);
    try {
        var a = "<div  id='" + e.replace(/\s/g, "").split("_")[0] + "_" + i + "' class='curve rtfpanel'  onclick='RTFResultShowHide(\"" + e.replace(/\s/g, "").split("_")[0] + "_" + i + "\");' ><img src='" + getAirImagePath(e.replace(/\s/g, "").split("_")[0]) + "' title='" + e.split("_")[0] + "' class='w29 lft' alt='" + e.split("_")[0] + "' /></div>";
        $("#RTFSAir").append(a)
    } catch (e) { }
}

function getMinPriceRTF(e, t) {
    var i = JSLINQ(e).Where(function (e) {
        return $.trim(e.fltName).replace(/\s/g, "") == t.split("_")[1]
    }).Select(function (e) {
        return e.fltJson
    }),
        a = JSLINQ(i.items[0]).Select(function (e) {
            return parseFloat(parseFloat(e.AdtFare) * parseFloat(e.Adult) + parseFloat(e.ChdFare) * parseFloat(e.Child) + parseFloat(e.InfFare) * parseFloat(e.Infant))
        }),
        r = (new Array, a.items.slice(0).sort(function (e, t) {
            return e - t
        })),
        s = JSLINQ(i.items[0]).Where(function (e) {
            return parseFloat(parseFloat(e.AdtFare) * parseFloat(e.Adult) + parseFloat(e.ChdFare) * parseFloat(e.Child) + parseFloat(e.InfFare) * parseFloat(e.Infant)) == r[0]
        }).Select(function (e) {
            return e.LineNumber
        }),
        l = JSLINQ(i.items[0]).Where(function (e) {
            return parseFloat(parseFloat(e.AdtFare) * parseFloat(e.Adult) + parseFloat(e.ChdFare) * parseFloat(e.Child) + parseFloat(e.InfFare) * parseFloat(e.Infant)) == r[r.length - 1]
        }).Select(function (e) {
            return e.LineNumber
        });
    return r.length > 0 ? (airlineNameRTFAll.push({
        id: t.split("_")[1],
        html: "",
        minprice: parseFloat(r[0]),
        maxprice: parseFloat(r[r.length - 1]),
        minLn: s.items[0],
        maxLn: l.items[0]
    }), $("#RTFSAirMain").show(), r[0]) : "0"
}

function getAirImagePath(e) {
    var t;
    return $.trim(e).toLocaleLowerCase() == $.trim("GoAir").toLocaleLowerCase() ? t = "G8" : $.trim(e).toLocaleLowerCase() == $.trim("JetAirways").toLocaleLowerCase() ? t = "9W" : $.trim(e).toLocaleLowerCase() == $.trim("AirIndia").toLocaleLowerCase() ? t = "AI" : $.trim(e).toLocaleLowerCase() == $.trim("Vistara").toLocaleLowerCase() ? t = "UK" : $.trim(e).toLocaleLowerCase() == $.trim("Jetlite").toLocaleLowerCase() ? t = "S2" : $.trim(e).toLocaleLowerCase() == $.trim("SpiceJet").toLocaleLowerCase() ? t = "SG" : $.trim(e).toLocaleLowerCase() == $.trim("indigo").toLocaleLowerCase() && (t = "6E"), "../Airlogo/sm" + t + ".gif"
}

function SRFPriceItinReq(e) {
    var t = new Array;
    try {
        new Array(e.items);
        var i = JSON.stringify(e.items),
            a = UrlBase + "FLTSearch1.asmx/SRFPriceItinReq";
        $.ajax({
            url: a,
            type: "POST",
            data: JSON.stringify({
                AirArray: i,
                Trip: e.items[0].Trip
            }),
            dataType: "json",
            type: "POST",
            async: !1,
            contentType: "application/json; charset=utf-8",
            success: function (e) {
                t.push(e)
            },
            error: function (e, t, i) { }
        })
    } catch (e) { }
    return t
}

function ShowHideDiscount(e) {
    "show" == $.trim(e) ? ($(".spnDiscountShowHide").show(), $(".spnBtnHide").show(), $(".spnBtnShow").hide()) : ($(".spnDiscountShowHide").hide(), $(".spnBtnHide").hide(), $(".spnBtnShow").show())
}

function tabs() { }

function DisplayMultipleFares(e, t, i, a, r, s, l, n, o, d) {
    var c = "",
        p = "",
        m = "",
        u = (new Array(4), t.unique()),
        h = !1;
    1 == r ? c += "O" == n ? '<div class="">' : '<div class="" style="">' : (c += '<div class="" style="">', c += '<div class="col-md-12">', c += '<div class="row" >');
    let v = 1;
    for (var f = 0; f < u.length; f++) {
        var g = JSLINQ(e.items).Where(function (e) {
            return e.LineNumber == u[f]
        }).Select(function (e) {
            return e
        });
        if ($.trim(g.items[0].Sector).replace(":", "-"), 1 == r) {
            if ("O" == n) {
                c += '<div  class="disp_o0" style="display:block;">', c += '<div class="pro DG_0">', c += '<div class="rw">', c += '<div class="r"><input type="radio" name="O" class="' + o + '" title="' + o + '_R" value="' + g.items[0].LineNumber + '" /></div>', c += "</div>", c += '<a class="fd gridViewToolTip flt_details_open" title="View Flight Details"  data-target="#' + g.items[0].LineNumber + f + '_radio" id="' + g.items[0].LineNumber + '_O" ><i class="fa fa-info-circle" aria-hidden="true" style="font-size: 15px;margin-top: 6px !important;"></i></a>', c += '<div class="pw">', c += "</div>", c += '<div class="nw">', c += '<div class="p" data-container=".sc-result.air .a_sr_wrap" title="" >₹ ' + g.items[0].TotalFare + '&nbsp;&nbsp;<span class="a_nb spnDiscountShowHide" style="display:none;">Net Fare : ₹ ' + g.items[0].NetFare + ' |</span>  <span class="a_nb spnDiscountShowHide" style="display:none;">Incv. Fare : ₹ ' + g.items[0].TotDis + " </span></div>", c += '<div class="indi">';
                var y = "Not-Refundable";
                "SG" == g.items[0].ValiDatingCarrier && ($.trim(g.items[0].Searchvalue).search("AP7") >= 0 || $.trim(g.items[0].Searchvalue).search("AP14") >= 0) ? y = "Refundable" : (y = "Not-Refundable", "refundable" == $.trim(g.items[0].AdtFareType).toLowerCase() && (y = "Refundable")), c += '<div class="dg" title="Published Fare">' + g.items[0].DisplayFareType + "</div>", c += '<div class="a_rf" title="Click for Rules">' + y + "</div>", c += " </div>", c += "</div>", c += "</div>", c += "</div>", c += '<div class="AirlineFareType hide">' + g.items[0].AdtFar + "</div>"
            } else if ("R" == n) {
                c += '<div class="AirlineFareType hide">' + g.items[0].AdtFar + "</div>", c += '<div  class="disp_o0" style="display:block;">', c += '<div class="pro DG_0">', c += '<div class="rw">', c += '<div class="r"><input type="radio" name="R" class="' + o + '" title="' + o + '_R" value="' + g.items[0].LineNumber + '" /></div>', c += "</div>", c += '<a href="#" data-toggle="modal" data-target="#' + g.items[0].LineNumber + f + 'R_radio" id="' + g.items[0].LineNumber + '_R" class="fd gridViewToolTip" title="View Flight Details"><i class="fa fa-info-circle" aria-hidden="true" style="font-size: 15px;margin-top: 6px !important;"></i></a>', c += '<div class="pw">', c += '<div class="p" data-container=".sc-result.air .a_sr_wrap" title="" >₹ ' + g.items[0].TotalFare + "</div>", c += "</div>", c += '<div class="nw">', c += '<div class="dg" title="Published Fare">' + g.items[0].DisplayFareType + "</div>", c += '<div class="indi">';
                y = "Not-Refundable";
                "SG" == g.items[0].ValiDatingCarrier && ($.trim(g.items[0].Searchvalue).search("AP7") >= 0 || $.trim(g.items[0].Searchvalue).search("AP14") >= 0) ? y = "Refundable" : (y = "Not-Refundable", "refundable" == $.trim(g.items[0].AdtFareType).toLowerCase() && (y = "Refundable")), c += '<div class="a_rf" title="Click for Rules">' + y + "</div>", c += '<div class="a_nb spnDiscountShowHide" style="display:none;">Net-: ₹ ' + g.items[0].NetFare + "</div>", c += '<div class="a_nb spnDiscountShowHide" style="display:none;">Inv-:' + g.items[0].TotDis + "</div>", c += " </div>", c += "</div>", c += "</div>", c += "</div>"
            }
        } else {
            h = !0, 1 == v && (c += '<div class="col-md-12 chk-fare">'), c += '<div class="AirlineFareType hide">' + g.items[0].AdtFar + "</div>", c += '<div class="chk-fare2">', c += '<div class="row">', c += '<div class="col-md-9" style="width: 70%;">', c += '<label style="font-size: 16px; font-weight: bold !important; color: #282828;">', c += '<input type="radio" name="radioPrice" title="' + o + '" id="' + g.items[0].LineNumber + '_radio"  value="' + g.items[0].LineNumber + '">', c += " ₹ " + Math.ceil(g.items[0].TotalFare) + '&nbsp;&nbsp;<span class="spnDiscountShowHide" style="font-size: 11px; font-weight: 600 !important;color: #313131;display:none;"> | Net ₹ ' + g.items[0].NetFare + "  | Inv ₹" + g.items[0].TotDis + "</span>", c += "</label>", c += "</div>", c += '<div class="col-md-3 flt-det" style="width: 30%;margin-top: 13px;">', c += '<span class="flt_details_open" data-showsummary="' + g.items[0].LineNumber + '" data-target="#' + g.items[0].LineNumber + f + '_radio" id="' + g.items[0].LineNumber + '_ALL" class="gridViewToolTip">View Details</span>', c += "</div>", c += "</div>";
            y = "Not-Refundable";
            "SG" == g.items[0].ValiDatingCarrier && ($.trim(g.items[0].Searchvalue).search("AP7") >= 0 || $.trim(g.items[0].Searchvalue).search("AP14") >= 0) ? y = "Refundable" : (y = "Not-Refundable", "refundable" == $.trim(g.items[0].AdtFareType).toLowerCase() && (y = "Refundable")), c += '<div class="row">', c += '<div class="col-md-12" style="margin-top: -6px;">', c += '<div class="rfnd" style="background: #fbd4ffa3;color: #ff1da8;font-size: 10px;">' + g.items[0].DisplayFareType + "</div>", c += '<div class="rfnd">' + y + "</div>", c += "</div>", c += "</div>", c += "</div>"
        }
        v += 1
    }
    c += '</div><div class="col-md-3 book-btn" style="position: relative;display:none;">';
    for (f = 0; f < u.length; f++) {
        g = JSLINQ(e.items).Where(function (e) {
            return e.LineNumber == u[f]
        }).Select(function (e) {
            return e
        });
        if ($.trim(g.items[0].Sector).replace(":", "-"), 0 == r && (0 == f && (c += '<div class=""><input type="button"  value="Book" class="falsebookbutton ' + o + '" id="' + o + '_falsebookbutton" style=""/></div>'), c += '<div class=""><input type="button"  value="→"  class="buttonfltbk" style="display: none;" title="' + g.items[0].LineNumber + '"  id="' + g.items[0].LineNumber + '" style="font-size: 37px;"/></div>'), 1 == r)
            if ("O" == n) {
                p += '<div  value="' + o + '" class="fare-groups-wrap ' + o + '_faredetailmasterall" id="' + g.items[0].LineNumber + '_faredetailmaster" style="margin-bottom:40px;">', p += '<div class="gridViewToolTip1 lft"  title="' + g.items[0].LineNumber + '_O" ></div>', p += '<ul class="fare-groups nav navbar-nav" role="tablist" style="border-bottom:1px solid #CCC;">', p += '<li class="fgf sel nav-item active">', p += '<a href="#' + g.items[0].LineNumber + '_Fare" class="d div_cls gridViewToolTipSRF nav-link collapsible" data-toggle="tab" role="tab" style="padding:0px;" id="' + g.items[0].LineNumber + '_O" title="' + o + '" rel="' + o + '">Fare Details</a>', p += '<div class="p"></div>', p += "</li>", p += '<li class="fgf nav-item">', p += '<a href="#' + g.items[0].LineNumber + '_Obdfltdt" class="d div_cls fltDetailslinkR nav-link collapsible" data-toggle="tab" role="tab" id="' + g.items[0].LineNumber + '_O" title="' + o + '" rel="' + o + '_O" style="padding:0px;" >Flight Details</a>', p += '<div class="p"></div>', p += "</li>", p += '<li class="fgf nav-item">', p += '<a href="#' + g.items[0].LineNumber + '_Obdbag" class="d div_cls fltBagDetailsR nav-link collapsible" data-toggle="tab" role="tab" id="' + g.items[0].LineNumber + '_O"  title="' + o + '" rel="' + o + '_O"  style="padding:0px;">Baggage</a>', p += '<div class="p"></div>', p += "</li>", p += '<li class="fgf nav-item">', p += '<a href="#' + g.items[0].LineNumber + '_Obdcanc" class="d div_cls fareRuleToolTip cursorpointer nav-link collapsible" data-toggle="tab" role="tab" rel="FareRule_' + o + '_O" title="' + o + '" style="padding:0px;" >Cancellation</a><div class="fade" title="' + o + '_O" ></div>', p += '<div class="p"></div>', p += "</li>", p += '<div class="ui_block clearfix"></div>', p += "</ul>", p += "</div>", p += '<div class="tabs-stage tab-content">', p += '<div class="depcity tab-pane active" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_Fare" style="margin-top:-11px;"></div>', p += '<div class="depcity tab-pane" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_Obdfltdt" style="margin-top:-11px;"></div>', p += '<div class="depcity tab-pane" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_Obdbag" style="margin-top:-11px;"></div>', p += '<div class="depcity tab-pane" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_Obdcanc" style="margin-top:-11px;"></div>', p += "</div>", p += "</div>"
            } else {
                p += '<div class="tabs">', p += '<div  value="' + o + '" class="fare-groups-wrap ' + o + '_faredetailmasterall " id="' + g.items[0].LineNumber + '_faredetailmaster" style="margin-bottom:40px;">', p += '<div class="gridViewToolTip1 lft"  title="' + g.items[0].LineNumber + '_R" ></div>', p += '<ul class="fare-groups nav navbar-nav" role="tablist" style="border-bottom:1px solid #CCC;">', p += '<li class="fgf sel nav-item active">', p += '<a href="#' + g.items[0].LineNumber + '_Fare" class="d div_cls gridViewToolTipSRF nav-link collapsible" data-toggle="tab" role="tab" style="padding:0px;" id="' + g.items[0].LineNumber + '_R" title="' + o + '" rel="' + o + '">Fare Details</a>', p += '<div class="p"></div>', p += "</li>", p += '<li class="fgf nav-item">', p += '<a href="#' + g.items[0].LineNumber + '_RO" class="d div_cls fltDetailslinkR nav-link collapsible" data-toggle="tab" role="tab" id="' + g.items[0].LineNumber + '_R" title="' + o + '" rel="' + o + '_R" style="padding:0px;" >Flight Details</a>', p += '<div class="p"></div>', p += "</li>", p += '<li class="fgf nav-item">', p += '<a href="#' + g.items[0].LineNumber + '_bag" class="d div_cls fltBagDetailsR nav-link collapsible" data-toggle="tab " role="tab" id="' + g.items[0].LineNumber + '_R"  title="' + o + '" rel="' + o + '_R"  style="padding:0px;">Baggage</a>', p += '<div class="p"></div>', p += "</li>", p += '<li class="fgf nav-item">', p += '<a href="#' + g.items[0].LineNumber + '_canc" class="d div_cls fareRuleToolTip cursorpointer nav-link collapsible" data-toggle="tab" role="tab" rel="FareRule_' + o + '_R" title="' + o + '" style="padding:0px;" >Cancellation</a><div class="fade" title="' + o + '_R" ></div>', p += '<div class="p"></div>', p += "</li>", p += '<div class="ui_block clearfix"></div>', p += "</ul>", p += "</div>", p += '<div class="tabs-stage tab-content">', p += '<div class="depcity tab-pane active" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_Fare" style="margin-top:-11px;"></div>', p += '<div class="depcity tab-pane" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_RO" style="margin-top:-11px;"></div>', p += '<div class="depcity tab-pane" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_bag" style="margin-top:-11px;"></div>', p += '<div class="depcity tab-pane" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_canc" style="margin-top:-11px;"></div>', p += "</div>", p += "</div>"
            }
        else p += '<div class="tabs">', p += '<div  value="' + o + '" class="fare-groups-wrap ' + o + '_faredetailmasterall " id="' + g.items[0].LineNumber + '_faredetailmaster" style="margin-bottom:40px;">', p += '<div class="gridViewToolTip1 lft"  title="' + g.items[0].LineNumber + '_O" ></div>', p += '<ul class="fare-groups nav navbar-nav" role="tablist" style="border-bottom:1px solid #CCC;">', p += '<li class="fgf sel nav-item active"  id="' + g.items[0].LineNumber + '_Allll" >', p += '<a href="#' + g.items[0].LineNumber + '_Fare" data-toggle="tab" role="tab" class="div_cls d collapsible gridViewToolTip nav-link active"  style="padding:0px;" id="' + g.items[0].LineNumber + '_flt" title="' + o + '" rel="' + o + '">Fare Details</a>', p += '<div class="p"></div>', p += "</li>", p += '<li class="fgf nav-item">', p += '<a href="#' + g.items[0].LineNumber + '_fltdt" data-toggle="tab" role="tab"  class="div_cls d collapsible fltDetailslink nav-link"  id="' + g.items[0].LineNumber + 'Det" title="' + o + '" rel="' + o + '" style="padding:0px;" >Flight Details</a>', p += '<div class="p"></div>', p += "</li>", p += '<li class="fgf nav-item">', p += '<a href="#' + g.items[0].LineNumber + '_bag" data-toggle="tab" role="tab" class="div_cls d collapsible fltBagDetails nav-link"  id="' + g.items[0].LineNumber + 'BagDet"  title="' + o + '" rel="' + o + '" style="padding:0px;">Baggage</a>', p += '<div class="p"></div>', p += "</li>", p += '<li  class="fgf nav-item">', p += '<a href="#' + g.items[0].LineNumber + '_canc" data-toggle="tab" role="tab"  class="div_cls d collapsible fareRuleToolTip cursorpointer nav-link" rel="FareRule_' + o + '_O" title="' + o + '" style="padding:0px;" >Cancellation</a><div class="fade" title="' + o + '_O" ></div>', p += '<div class="p"></div>', p += "</li>", p += '<li  class="fgf nav-item" style="float:right;">', p += '<a class="div_cls d cursorpointer nav-link" style="padding:0px;font-size: 23px;" id="det_flt_cl"><i class="icofont-close"></i></a>', p += "</li>", p += '<div class="ui_block clearfix"></div>', p += "</ul>", p += "</div>", p += '<div class="tabs-stage tab-content">', p += '<div class="depcity tab-pane active" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_Fare" style="margin-top:-11px;"></div>', p += '<div class="depcity tab-pane" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_fltdt" style="margin-top:-11px;"></div>', p += '<div class="depcity tab-pane" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_bag" style="margin-top:-11px;"></div>', p += '<div class="depcity tab-pane" role="tabpanel" id="' + g.items[0].LineNumber.toString() + '_canc" style="margin-top:-11px;"></div>', p += "</div>", p += "</div>"
    }
    return c += "</div>", c += "</div>", 1 == h && (c += "</div>", c += "</div>"), c += '<div class="row flight-summary" id="showsummary_' + g.items[0].LineNumber + '" style="display:none;">', 0 == d ? c += p : m = p, [c += '<span class="scroll_' + g.items[0].LineNumber + '"></span> </div>', m]
}

function SelectFareAlert() {
    alert("Please select Fare !!")
}

function show_sidebar(e) {
    var t = e.id.split("_")[0];
    document.getElementById(t + "_sidebar").style.visibility = "visible"
}

function hide_sidebar(e) {
    var t = e.id.split("_")[0];
    document.getElementById(t + "_sidebar").style.visibility = "hidden"
}

function setfare(e, i) {
    event.preventDefault();
    var a, r = e.split("_"),
        s = i;
    a = "R" == r[1] ? JSLINQ(result[1]).Where(function (e) {
        return e.LineNumber == r[0]
    }).Select(function (e) {
        return e
    }) : JSLINQ(result[0]).Where(function (e) {
        return e.LineNumber == r[0]
    }).Select(function (e) {
        return e
    });
    var l = JSON.stringify(a.items),
        n = UrlBase + "FLTSearch1.asmx/FareBreakupGAL1";
    $.ajax({
        url: n,
        type: "POST",
        data: JSON.stringify({
            AirArray: l,
            Trip: a.items[0].Trip
        }),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (e) {
            $("#" + s + "_Fare").html(t.CreateFareBreakUp(e.d[0])), $("#SomeModal").on("show.bs.modal", function (e) { })
        },
        error: function (e, t, i) { }
    })
}
ResHelper.prototype.BindEvents = function () {
    var e = this;
    e.GetResult(e), e.eventFn(), e.NextPrevSearch(e)
}, ResHelper.prototype.GetCommomTimeForFilter = function (e) {
    var t = "",
        i = parseInt(e.substring(0, 2)),
        a = parseInt(e.substring(2, 4));
    return i >= 0 && i <= 6 ? t = 6 == i && a > 0 ? "6_12" : "0_6" : i >= 6 && i <= 12 && (t = 12 == i && a > 0 ? "12_18" : "6_12"), i >= 12 && a > 0 && i <= 18 && (t = 18 == i && a > 0 ? "18_0" : "12_18"), i >= 18 && (t = 18 == i && 0 == a ? "12_18" : "18_0"), t
}, ResHelper.prototype.NextPrevSearch = function (e) {
    e.RtfToNextDay.click(function (t) {
        t.preventDefault();
        var i, a = (new QSHelper).queryStr(),
            r = (a.txtDepDate.split("/"), a.txtRetDate.split("/")),
            s = new Date(parseFloat(r[2]), parseFloat(r[1]) - 1, parseFloat(r[0]) + 1);
        i = s.getDate() < 10 ? "0" + s.getDate() : s.getDate();
        var l, n = parseInt(s.getMonth() + 1);
        n = n < 10 ? "0" + n : n, e.txtRetDate.val(i + "/" + n + "/" + s.getFullYear());
        var o = e.hidtxtDepCity1.val().split(","),
            d = e.hidtxtArrCity1.val().split(",");
        l = "IN" == o[1] && "IN" == d[1] ? "D" : "I";
        var c = new Boolean;
        c = 1 == e.chkNonstop.is(":checked");
        var p = new Boolean;
        p = 1 == e.LCC_RTF.is(":checked");
        var m = new Boolean;
        m = 1 == e.GDS_RTF.is(":checked");
        var u = "TripType=" + $("input[name='TripType']:checked").val() + "&txtDepCity1=" + e.txtDepCity1.val() + "&txtArrCity1=" + e.txtArrCity1.val() + "&hidtxtDepCity1=" + e.hidtxtDepCity1.val() + "&hidtxtArrCity1=" + e.hidtxtArrCity1.val() + "&Adult=" + e.Adult.val();
        u += "&Child=" + e.Child.val() + "&Infant=" + e.Infant.val() + "&Cabin=" + e.Cabin.val() + "&txtAirline=" + e.txtAirline.val() + "&hidtxtAirline=" + e.hidtxtAirline.val() + "&txtDepDate=" + a.txtDepDate + "&txtRetDate=" + e.txtRetDate.val(), u += "&Nstop=" + c + "&RTF=" + p + "&Trip=" + l + "&GRTF=" + m, document.getElementById("__VIEWSTATE").name = "NOVIEWSTATE", "D" == l ? window.location.href = UrlBase + "Flightdom/FltResult.aspx?" + u : "I" == l && (window.location.href = UrlBase + "FlightInt/FltResIntl.aspx?" + u)
    }), e.RtfToPrevDay.click(function (t) {
        t.preventDefault();
        var i, a = (new QSHelper).queryStr(),
            r = (a.txtDepDate.split("/"), a.txtRetDate.split("/")),
            s = new Date(parseFloat(r[2]), parseFloat(r[1]) - 1, parseFloat(r[0]) - 1);
        i = s.getDate() < 10 ? "0" + s.getDate() : s.getDate();
        var l, n = parseInt(s.getMonth() + 1);
        n = n < 10 ? "0" + n : n, e.txtRetDate.val(i + "/" + n + "/" + s.getFullYear());
        var o = e.hidtxtDepCity1.val().split(","),
            d = e.hidtxtArrCity1.val().split(",");
        l = "IN" == o[1] && "IN" == d[1] ? "D" : "I";
        var c = new Boolean;
        c = 1 == e.chkNonstop.is(":checked");
        var p = new Boolean;
        p = 1 == e.LCC_RTF.is(":checked");
        var m = new Boolean;
        m = 1 == e.GDS_RTF.is(":checked");
        var u = "TripType=" + $("input[name='TripType']:checked").val() + "&txtDepCity1=" + e.txtDepCity1.val() + "&txtArrCity1=" + e.txtArrCity1.val() + "&hidtxtDepCity1=" + e.hidtxtDepCity1.val() + "&hidtxtArrCity1=" + e.hidtxtArrCity1.val() + "&Adult=" + e.Adult.val();
        u += "&Child=" + e.Child.val() + "&Infant=" + e.Infant.val() + "&Cabin=" + e.Cabin.val() + "&txtAirline=" + e.txtAirline.val() + "&hidtxtAirline=" + e.hidtxtAirline.val() + "&txtDepDate=" + a.txtDepDate + "&txtRetDate=" + e.txtRetDate.val(), u += "&Nstop=" + c + "&RTF=" + p + "&Trip=" + l + "&GRTF=" + m, document.getElementById("__VIEWSTATE").name = "NOVIEWSTATE", "D" == l ? window.location.href = UrlBase + "Flightdom/FltResult.aspx?" + u : "I" == l && (window.location.href = UrlBase + "FlightInt/FltResIntl.aspx?" + u)
    }), e.RtfFromNextDay.click(function (t) {
        t.preventDefault();
        var i, a = (new QSHelper).queryStr(),
            r = a.txtDepDate.split("/"),
            s = (a.txtRetDate.split("/"), new Date(parseFloat(r[2]), parseFloat(r[1]) - 1, parseFloat(r[0]) + 1));
        i = s.getDate() < 10 ? "0" + s.getDate() : s.getDate();
        var l, n = parseInt(s.getMonth() + 1);
        n = n < 10 ? "0" + n : n, e.txtDepDate.val(i + "/" + n + "/" + s.getFullYear());
        var o = e.hidtxtDepCity1.val().split(","),
            d = e.hidtxtArrCity1.val().split(",");
        l = "IN" == o[1] && "IN" == d[1] ? "D" : "I";
        var c = new Boolean;
        c = 1 == e.chkNonstop.is(":checked");
        var p = new Boolean;
        p = 1 == e.LCC_RTF.is(":checked");
        var m = new Boolean;
        m = 1 == e.GDS_RTF.is(":checked");
        var u = "TripType=" + $("input[name='TripType']:checked").val() + "&txtDepCity1=" + e.txtDepCity1.val() + "&txtArrCity1=" + e.txtArrCity1.val() + "&hidtxtDepCity1=" + e.hidtxtDepCity1.val() + "&hidtxtArrCity1=" + e.hidtxtArrCity1.val() + "&Adult=" + e.Adult.val();
        u += "&Child=" + e.Child.val() + "&Infant=" + e.Infant.val() + "&Cabin=" + e.Cabin.val() + "&txtAirline=" + e.txtAirline.val() + "&hidtxtAirline=" + e.hidtxtAirline.val() + "&txtDepDate=" + e.txtDepDate.val() + "&txtRetDate=" + a.txtRetDate, u += "&Nstop=" + c + "&RTF=" + p + "&Trip=" + l + "&GRTF=" + m, document.getElementById("__VIEWSTATE").name = "NOVIEWSTATE", "D" == l ? window.location.href = UrlBase + "domestic/flight-result?" + u : "I" == l && (window.location.href = UrlBase + "international/flight-result?" + u)
    }), e.RtfFromPrevDay.click(function (t) {
        t.preventDefault();
        var i, a = (new QSHelper).queryStr(),
            r = a.txtDepDate.split("/"),
            s = (a.txtRetDate.split("/"), new Date(parseFloat(r[2]), parseFloat(r[1]) - 1, parseFloat(r[0]) - 1));
        i = s.getDate() < 10 ? "0" + s.getDate() : s.getDate();
        var l, n = parseInt(s.getMonth() + 1);
        n = n < 10 ? "0" + n : n, e.txtDepDate.val(i + "/" + n + "/" + s.getFullYear());
        var o = e.hidtxtDepCity1.val().split(","),
            d = e.hidtxtArrCity1.val().split(",");
        l = "IN" == o[1] && "IN" == d[1] ? "D" : "I";
        var c = new Boolean;
        c = 1 == e.chkNonstop.is(":checked");
        var p = new Boolean;
        p = 1 == e.LCC_RTF.is(":checked");
        var m = new Boolean;
        m = 1 == e.GDS_RTF.is(":checked");
        var u = "TripType=" + $("input[name='TripType']:checked").val() + "&txtDepCity1=" + e.txtDepCity1.val() + "&txtArrCity1=" + e.txtArrCity1.val() + "&hidtxtDepCity1=" + e.hidtxtDepCity1.val() + "&hidtxtArrCity1=" + e.hidtxtArrCity1.val() + "&Adult=" + e.Adult.val();
        u += "&Child=" + e.Child.val() + "&Infant=" + e.Infant.val() + "&Cabin=" + e.Cabin.val() + "&txtAirline=" + e.txtAirline.val() + "&hidtxtAirline=" + e.hidtxtAirline.val() + "&txtDepDate=" + e.txtDepDate.val() + "&txtRetDate=" + a.txtRetDate, u += "&Nstop=" + c + "&RTF=" + p + "&Trip=" + l + "&GRTF=" + m, document.getElementById("__VIEWSTATE").name = "NOVIEWSTATE", "D" == l ? window.location.href = UrlBase + "domestic/flight-result?" + u : "I" == l && (window.location.href = UrlBase + "international/flight-result?" + u)
    }), e.PrevDaySrch.click(function (t) {
        t.preventDefault();
        var i, a, r = (new QSHelper).queryStr(),
            s = r.txtDepDate.split("/"),
            l = r.txtRetDate.split("/"),
            n = new Date(parseFloat(s[2]), parseFloat(s[1]) - 1, parseFloat(s[0]) - 1),
            o = new Date(parseFloat(l[2]), parseFloat(l[1]) - 1, parseFloat(l[0]) - 1);
        a = n.getDate() < 10 ? "0" + n.getDate() : n.getDate(), i = o.getDate() < 10 ? "0" + o.getDate() : o.getDate();
        var d, c = parseInt(n.getMonth() + 1),
            p = parseInt(o.getMonth() + 1);
        c = c < 10 ? "0" + c : c, p = p < 10 ? "0" + p : p, e.txtDepDate.val(a + "/" + c + "/" + n.getFullYear()), e.txtRetDate.val(i + "/" + p + "/" + o.getFullYear());
        var m = e.hidtxtDepCity1.val().split(","),
            u = e.hidtxtArrCity1.val().split(",");
        d = "IN" == m[1] && "IN" == u[1] ? "D" : "I";
        var h = new Boolean;
        h = 1 == e.chkNonstop.is(":checked");
        var v = new Boolean;
        v = 1 == e.LCC_RTF.is(":checked");
        var f = new Boolean;
        f = 1 == e.GDS_RTF.is(":checked");
        var g = "TripType=" + $("input[name='TripType']:checked").val() + "&txtDepCity1=" + e.txtDepCity1.val() + "&txtArrCity1=" + e.txtArrCity1.val() + "&hidtxtDepCity1=" + e.hidtxtDepCity1.val() + "&hidtxtArrCity1=" + e.hidtxtArrCity1.val() + "&Adult=" + e.Adult.val();
        if (g += "&Child=" + e.Child.val() + "&Infant=" + e.Infant.val() + "&Cabin=" + e.Cabin.val() + "&txtAirline=" + e.txtAirline.val() + "&hidtxtAirline=" + e.hidtxtAirline.val() + "&txtDepDate=" + e.txtDepDate.val() + "&txtRetDate=" + e.txtRetDate.val(), g += "&Nstop=" + h + "&RTF=" + v + "&Trip=" + d + "&GRTF=" + f, document.getElementById("__VIEWSTATE").name = "NOVIEWSTATE", "D" == d) try {
            $.ajax({
                url: UrlBase + "FltSearch1.asmx/GetMUForPage",
                data: "{ 'name': 'domestic/flight-result'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    var t = e.d;
                    window.location.href = null != t && "" != t ? UrlBase + t + "?" + u : UrlBase + "domestic/flight-result?" + g
                },
                error: function (e, t, i) {
                    window.location.href = UrlBase + "domestic/flight-result?" + g
                }
            })
        } catch (e) {
            window.location.href = UrlBase + "domestic/flight-result?" + g
        } else if ("I" == d) try {
            $.ajax({
                url: UrlBase + "FltSearch1.asmx/GetMUForPage",
                data: "{ 'name': 'international/flight-result'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    var t = e.d;
                    window.location.href = null != t && "" != t ? UrlBase + t + "?" + g : UrlBase + "international/flight-result?" + g
                },
                error: function (e, t, i) {
                    window.location.href = UrlBase + "international/flight-result?" + g
                }
            })
        } catch (e) {
            window.location.href = UrlBase + "international/flight-result?" + g
        }
    }), e.NextDaySrch.click(function (t) {
        t.preventDefault();
        var i, a, r = QSHandler.queryStr(),
            s = r.txtDepDate.split("/"),
            l = r.txtRetDate.split("/"),
            n = new Date(parseFloat(s[2]), parseFloat(s[1]) - 1, parseFloat(s[0]) + 1),
            o = new Date(parseFloat(l[2]), parseFloat(l[1]) - 1, parseFloat(l[0]) + 1);
        a = n.getDate() < 10 ? "0" + n.getDate() : n.getDate(), i = o.getDate() < 10 ? "0" + o.getDate() : o.getDate();
        var d, c = parseInt(n.getMonth() + 1),
            p = parseInt(o.getMonth() + 1);
        c = c < 10 ? "0" + c : c, p = p < 10 ? "0" + p : p, e.txtDepDate.val(a + "/" + c + "/" + n.getFullYear()), e.txtRetDate.val(i + "/" + p + "/" + o.getFullYear());
        var m = e.hidtxtDepCity1.val().split(","),
            u = e.hidtxtArrCity1.val().split(",");
        d = "IN" == m[1] && "IN" == u[1] ? "D" : "I";
        var h = new Boolean;
        h = 1 == e.chkNonstop.is(":checked");
        var v = new Boolean;
        v = 1 == e.LCC_RTF.is(":checked");
        var f = new Boolean;
        f = 1 == e.GDS_RTF.is(":checked");
        var g = "TripType=" + $("input[name='TripType']:checked").val() + "&txtDepCity1=" + e.txtDepCity1.val() + "&txtArrCity1=" + e.txtArrCity1.val() + "&hidtxtDepCity1=" + e.hidtxtDepCity1.val() + "&hidtxtArrCity1=" + e.hidtxtArrCity1.val() + "&Adult=" + e.Adult.val();
        if (g += "&Child=" + e.Child.val() + "&Infant=" + e.Infant.val() + "&Cabin=" + e.Cabin.val() + "&txtAirline=" + e.txtAirline.val() + "&hidtxtAirline=" + e.hidtxtAirline.val() + "&txtDepDate=" + e.txtDepDate.val() + "&txtRetDate=" + e.txtRetDate.val(), g += "&Nstop=" + h + "&RTF=" + v + "&Trip=" + d + "&GRTF=" + f, document.getElementById("__VIEWSTATE").name = "NOVIEWSTATE", "D" == d) try {
            $.ajax({
                url: UrlBase + "FltSearch1.asmx/GetMUForPage",
                data: "{ 'name': 'domestic/flight-result'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    var t = e.d;
                    window.location.href = null != t && "" != t ? UrlBase + t + "?" + u : UrlBase + "domestic/flight-result?" + g
                },
                error: function (e, t, i) {
                    window.location.href = UrlBase + "domestic/flight-result?" + g
                }
            })
        } catch (e) {
            window.location.href = UrlBase + "domestic/flight-result?" + g
        } else if ("I" == d) try {
            $.ajax({
                url: UrlBase + "FltSearch1.asmx/GetMUForPage",
                data: "{ 'name': 'international/flight-result'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    var t = e.d;
                    window.location.href = null != t && "" != t ? UrlBase + t + "?" + g : UrlBase + "international/flight-result?" + g
                },
                error: function (e, t, i) {
                    window.location.href = UrlBase + "FlightInt/FltResIntl.aspx?" + g
                }
            })
        } catch (e) {
            window.location.href = UrlBase + "international/flight-result?" + g
        }
    })
}, ResHelper.prototype.eventFn = function () {
    var e = this;
    e.flterTabO.click(function () {
        divclspmatrix = 0, e.flterR.hide(), e.flterO.show(), e.flterTabO.removeClass("spn"), $(".clspMatrix").switchClass("clspMatrix1", "clspMatrix"), e.flterTabO.removeClass("spn1"), e.flterTabO.addClass("spn1"), e.flterTabR.addClass("spn"), e.flterTabO.addClass(""), $(".list-itemR .fltbox").removeClass("bdrblue"), $(".list-itemR .fltboxnew").removeClass("bdrblue"), $(".list-item .fltbox").addClass("bdrblue"), $(".list-item .fltboxnew").addClass("bdrblue"), e.DivRefinetitle.html(e.txtDepCity1.val() + " to " + e.txtArrCity1.val()), e.DivMatrixRtfO.show(), e.DivMatrixRtfR.hide()
    }), e.flterTabR.click(function () {
        divclspmatrix = 1, e.flterO.hide(), e.flterR.show(), e.flterTabR.removeClass("spn"), e.flterTabR.removeClass("spn1"), $(".clspMatrix").switchClass("clspMatrix1", "clspMatrix"), $(".list-item .fltbox").removeClass("bdrblue"), $(".list-item .fltboxnew").removeClass("bdrblue"), $(".list-itemR .fltbox").addClass("bdrblue"), $(".list-itemR .fltboxnew").addClass("bdrblue"), e.flterTabR.addClass("spn1"), e.flterTabO.addClass("spn"), e.DivRefinetitle.html(e.txtArrCity1.val() + " to " + e.txtDepCity1.val()), e.DivMatrixRtfO.hide(), e.DivMatrixRtfR.show()
    }), e.clspMatrix.click(function () {
        if (0 == divclspmatrix) {
            try {
                e.DivMatrixRtfO.slideToggle(300)
            } catch (e) { }
            e.divMatrixO.slideToggle(300)
        } else e.DivMatrixRtfR.slideToggle(300);
        e.clspMatrix.toggleClass("clspMatrix1")
    }), e.rtfResultDiv.click(function () {
        e.hdnOnewayOrRound.val("RoundTrip"), e.flterR.show(), e.RoundTripH.show(), e.OnewayH.hide(), e.DivMatrixRtfO.show(), e.DivMatrixRtfR.show(), e.DivMatrix.hide(), null != rtfResult && e.RoundTripH.html(rtfResult)
    }), e.lccRTFDiv.click(function () {
        e.hdnOnewayOrRound.val("OneWay"), e.flterR.hide(), null == rtfResult && (rtfResult = e.RoundTripH.html()), e.RoundTripH.html(""), e.RoundTripH.hide(), e.DivMatrixRtfO.hide(), e.DivMatrixRtfR.hide(), null == lccRtfResult ? e.GetResultSplRTFTrip(e, "lcc") : ($.blockUI({
            message: $("#waitMessage")
        }), e.DivResult.html(lccRtfResult), e.Book(lccRtfResultArray), e.GetFltDetails(lccRtfResultArray), e.ShowFareBreakUp(lccRtfResultArray), e.FltrSort(lccRtfResultArray), e.GetFltDetailsR(lccRtfResultArray), e.MainSF.show(), FlightHandler = new FlightResult, FlightHandler.BindEvents(), $(document).ajaxStop($.unblockUI)), e.OnewayH.show(), e.DivMatrix.show()
    }), e.gdsRTFDiv.click(function () {
        e.hdnOnewayOrRound.val("OneWay"), e.flterR.hide(), null == rtfResult && (rtfResult = e.RoundTripH.html()), e.RoundTripH.html(""), e.RoundTripH.hide(), e.DivMatrixRtfO.hide(), e.DivMatrixRtfR.hide(), null == gdsRtfResult ? e.GetResultSplRTFTrip(e, "gds") : ($.blockUI({
            message: $("#waitMessage")
        }), e.DivResult.html(gdsRtfResult), e.Book(gdsRtfResultArray), e.GetFltDetails(gdsRtfResultArray), e.ShowFareBreakUp(gdsRtfResultArray), e.FltrSort(gdsRtfResultArray), e.GetFltDetailsR(gdsRtfResultArray), e.MainSF.show(), FlightHandler = new FlightResult, FlightHandler.BindEvents(), $(document).ajaxStop($.unblockUI)), e.OnewayH.show(), e.DivMatrix.show()
    })
}, ResHelper.prototype.Book = function (e) {
    this.bookO = $(".buttonfltbk"), $(document).on("click", ".buttonfltbk1", function (t) {
        ChangedFarePopupShow(0, 0, 0, "hide", "D"), $("#searchquery").hide();
        for (var i = $.trim($(this).attr("title")), a = $.trim(i.split("api")[0]), r = JSLINQ(e[0]).Where(function (e) {
            return e.LineNumber == i || e.LineNumber == a
        }).Select(function (e) {
            return e
        }), s = new Array(r.items), l = s[0][0].LineNumber, n = 0; n < s[0].length; n++) s[0][n].ProductDetailQualifier = s[0][n].LineNumber.split("ITZ")[1], s[0][n].LineNumber = s[0][n].LineNumber.split("api")[0];
        if ("I" == s[0][0].Trip) {
            var o = new URLSearchParams(location.search).get("NStop"),
                d = JSON.stringify(s),
                c = UrlBase + "FLTSearch1.asmx/Insert_International_FltDetails_LZCmp";
            $.ajax({
                url: c,
                type: "POST",
                data: JSON.stringify({
                    a: d
                }),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    "0" == e.d.ChangeFareO.TrackId ? alert("Fare Changed Please Try again") : parseFloat(e.d.ChangeFareO.CacheTotFare) != parseFloat(e.d.ChangeFareO.NewTotFare) ? ChangedFarePopupShow(e.d.ChangeFareO.CacheTotFare, e.d.ChangeFareO.NewTotFare, e.d.ChangeFareO.TrackId, "show", e.d.ChangeFareO.NewNetFare, "I") : window.location = "TRUE" == o ? UrlBase + "FlightInt/CustomerInfoFixDep.aspx?" + e.d.ChangeFareO.TrackId + ",I" : UrlBase + "International/PaxDetails.aspx?" + e.d.ChangeFareO.TrackId + ",I"
                },
                error: function (e, t, i) {
                    alert(t), window.location = UrlBase + "flight-search"
                }
            })
        } else {
            d = JSON.stringify(s), c = UrlBase + "FLTSearch1.asmx/Insert_Selected_FltDetails_LZCmp";
            $.ajax({
                url: c,
                type: "POST",
                data: JSON.stringify({
                    a: d
                }),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    for (var t = 0; t < s[0].length; t++) s[0][t].LineNumber = l;
                    "0" == e.d.ChangeFareO.TrackId ? (1 == confirm("Fare changed Please Try again.") && location.reload(), $("#ConfmingFlight").hide(), $(document).ajaxStop($.unblockUI)) : parseFloat(e.d.ChangeFareO.CacheTotFare) != parseFloat(e.d.ChangeFareO.NewTotFare) ? ChangedFarePopupShow(e.d.ChangeFareO.CacheTotFare, e.d.ChangeFareO.NewTotFare, e.d.ChangeFareO.TrackId, "show", e.d.ChangeFareO.NewNetFare, "D") : window.location = UrlBase + "domestic/passenger-summary?" + e.d.ChangeFareO.TrackId
                },
                error: function (e, t, i) {
                    for (var a = 0; a < s[0].length; a++) s[0][a].LineNumber = l;
                    alert(t), window.location = UrlBase + "flight-search"
                }
            })
        }
    }), this.bookO.click(function () {
        ChangedFarePopupShow(0, 0, 0, "hide", "D"), $("#searchquery").hide();
        for (var t = $.trim($(this).attr("title")), i = $.trim(t.split("api")[0]), a = JSLINQ(e[0]).Where(function (e) {
            return e.LineNumber == t || e.LineNumber == i
        }).Select(function (e) {
            return e
        }), r = new Array(a.items), s = r[0][0].LineNumber, l = 0; l < r[0].length; l++) r[0][l].ProductDetailQualifier = r[0][l].LineNumber.split("ITZ")[1], r[0][l].LineNumber = r[0][l].LineNumber.split("api")[0];
        if ("I" == r[0][0].Trip) {
            var n = new URLSearchParams(location.search).get("NStop"),
                o = JSON.stringify(r),
                d = UrlBase + "FLTSearch1.asmx/Insert_International_FltDetails_LZCmp";
            $.ajax({
                url: d,
                type: "POST",
                data: JSON.stringify({
                    a: o
                }),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    "0" == e.d.ChangeFareO.TrackId ? alert("Fare Changed Please Try again") : parseFloat(e.d.ChangeFareO.CacheTotFare) != parseFloat(e.d.ChangeFareO.NewTotFare) ? ChangedFarePopupShow(e.d.ChangeFareO.CacheTotFare, e.d.ChangeFareO.NewTotFare, e.d.ChangeFareO.TrackId, "show", e.d.ChangeFareO.NewNetFare, "I") : window.location = "TRUE" == n ? UrlBase + "FlightInt/CustomerInfoFixDep.aspx?" + e.d.ChangeFareO.TrackId + ",I" : UrlBase + "International/PaxDetails.aspx?" + e.d.ChangeFareO.TrackId + ",I"
                },
                error: function (e, t, i) {
                    alert(t), window.location = UrlBase + "flight-search"
                }
            })
        } else {
            o = JSON.stringify(r), d = UrlBase + "FLTSearch1.asmx/Insert_Selected_FltDetails_LZCmp";
            $.ajax({
                url: d,
                type: "POST",
                data: JSON.stringify({
                    a: o
                }),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    for (var t = 0; t < r[0].length; t++) r[0][t].LineNumber = s;
                    "0" == e.d.ChangeFareO.TrackId ? (1 == confirm("Fare changed Please Try again.") && location.reload(), $("#ConfmingFlight").hide(), $(document).ajaxStop($.unblockUI)) : parseFloat(e.d.ChangeFareO.CacheTotFare) != parseFloat(e.d.ChangeFareO.NewTotFare) ? ChangedFarePopupShow(e.d.ChangeFareO.CacheTotFare, e.d.ChangeFareO.NewTotFare, e.d.ChangeFareO.TrackId, "show", e.d.ChangeFareO.NewNetFare, "D") : window.location = UrlBase + "domestic/passenger-summary?" + e.d.ChangeFareO.TrackId
                },
                error: function (e, t, i) {
                    for (var a = 0; a < r[0].length; a++) r[0][a].LineNumber = s;
                    alert(t), window.location = UrlBase + "flight-search"
                }
            })
        }
    }), this.bookfalse = $(".falsebookbutton"), this.bookfalse.click(function () {
        alert("Please select Fare !!")
    })
}, ResHelper.prototype.ShowFareBreakUp = function (e) {
    var t = this;
    $(".gridViewToolTip").click(function (i) {
        i.preventDefault();
        var a, r = this.id.split("_"),
            s = r[0];
        "" == this.rel && (s = r[0]), a = "R" == r[1] ? JSLINQ(e[1]).Where(function (e) {
            return e.LineNumber == r[0]
        }).Select(function (e) {
            return e
        }) : JSLINQ(e[0]).Where(function (e) {
            return e.LineNumber == r[0]
        }).Select(function (e) {
            return e
        });
        var l = JSON.stringify(a.items),
            n = UrlBase + "FLTSearch1.asmx/FareBreakupGAL1";
        $.ajax({
            url: n,
            type: "POST",
            data: JSON.stringify({
                AirArray: l,
                Trip: a.items[0].Trip
            }),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (e) {
                $("#" + s + "_Fare").html(t.CreateFareBreakUp(e.d[0]))
            },
            error: function (e, t, i) { }
        })
    }), this.SRFTooltipFare = $(".gridViewToolTipSRF"), t.SRFTooltipFare.click(function (e) {
        if ("2" == (0 == $("input[name=RadioGroup1]").length ? "" : $("input[name=RadioGroup1]:checked").val())) {
            var i, a = Obook[0].SubKey + Rbook[0].SubKey,
                r = null,
                s = (new Array, "D");
            1 == SRFReprice && null != SRFResultAfterReprice && SRFResultAfterReprice.length > 0 && SRFResultAfterReprice[0].d.length > 0 && "6E" == SRFResultAfterReprice[0].d[0].ValiDatingCarrier.toUpperCase() ? (r = SRFResultAfterReprice[0].d, new Array(r), i = JSON.stringify(r), s = r[0].Trip) : (r = JSLINQ(CommanRTFArray).Where(function (e) {
                return e.MainKey == a
            }).Select(function (e) {
                return e
            }), new Array(r.items), i = JSON.stringify(r.items), s = r.items[0].Trip);
            var l = UrlBase + "FLTSearch1.asmx/FareBreakupGAL1";
            $.ajax({
                url: l,
                type: "POST",
                data: JSON.stringify({
                    AirArray: i,
                    Trip: s
                }),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    $("#FareBreakupHeder").addClass("show"), $("#FareBreakupHederId").html(""), $("#FareBreakupHederId").html("Loding Breakup ..."), $("#FareBreakupHederId").html(t.CreateFareBreakUp(e.d[0]))
                },
                error: function (e, t, i) { }
            })
        } else {
            var n = JSON.stringify(Obook);
            l = UrlBase + "FLTSearch1.asmx/FareBreakupGAL1";
            $.ajax({
                url: l,
                type: "POST",
                data: JSON.stringify({
                    AirArray: n,
                    Trip: Obook[0].Trip
                }),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (e) {
                    var i = JSON.stringify(Rbook),
                        a = t.CreateFareBreakUp(e.d[0]);
                    $.ajax({
                        url: l,
                        type: "POST",
                        data: JSON.stringify({
                            AirArray: i,
                            Trip: Rbook[0].Trip
                        }),
                        dataType: "json",
                        type: "POST",
                        async: !1,
                        contentType: "application/json; charset=utf-8",
                        success: function (e) {
                            var i = t.CreateFareBreakUp(e.d[0]),
                                r = '<div class="row">';
                            r += '<div class="col-md-12"><div><h4>Outbound</h4></div>' + a + "</div>", r += '<div class="col-md-12"><div><h4>Inbound</h4></div>' + i + "</div>", r += '<div class="clear"></div>', r += "</div>", $("#FareBreakupHeder").addClass("show"), $("#FareBreakupHederId").html(""), $("#FareBreakupHederId").html("Loding Breakup ..."), $("#FareBreakupHederId").html(r)
                        },
                        error: function (e, t, i) {
                            alert(t)
                        }
                    })
                },
                error: function (e, t, i) {
                    alert(t)
                }
            })
        }
    }), this.fareRuleToolTip = $(".fareRuleToolTip"), $(".fareRuleToolTip").click(function (t) {
        t.preventDefault();
        var i = $(this).next().attr("title").split("_");
        if ($("#" + i[0] + "_").html("<div align='center'><img alt='loading' width='50px' height='50px' src='../images/loadingAnim.gif'/></div>"), "</div>", null != i[0]) {
            var a;
            i[1], i[1], a = "R" == i[1] ? JSLINQ(e[1]).Where(function (e) {
                return e.LineNumber == i[0]
            }).Select(function (e) {
                return e
            }) : JSLINQ(e[0]).Where(function (e) {
                return e.LineNumber == i[0]
            }).Select(function (e) {
                return e
            });
            var r = new Array(a.items);
            if (a.items.length > 0) {
                var s = UrlBase + "FareRuleService.asmx/GetFareRule";
                $.ajax({
                    url: s,
                    type: "POST",
                    data: JSON.stringify({
                        JsonArr: r,
                        sno: a.items[0].sno,
                        Provider: a.items[0].Provider
                    }),
                    dataType: "json",
                    type: "POST",
                    async: !0,
                    contentType: "application/json; charset=utf-8",
                    success: function (e) {
                        var t = "<div>";
                        null == e.d.Response ? (t += '<div class=""><div></div>', t += "<div>fare rule not available.</div>", t += "</div>") : (t += '<div class=""><div></div>', t += "<div>" + e.d.Response.FareRules[0].FareRuleDetail + "</div>", t += "</div>"), "R" == i[1] ? $("#" + i[0] + "_RO").html(t) : $("#" + i[0] + "_canc").html(t)
                    },
                    error: function (e, t, a) {
                        alert("Please try after sometime!"), $("#" + i[0] + "_").hide()
                    }
                })
            } else $(this).next().html("fare rule not available."), $("#" + i[0] + "_").hide(), "R" == i[1] && $("#" + i[0] + "_R").hide()
        }
    }), $(".fltfareDetailsR").click(function (i) {
        if (null != $(this).attr("rel")) {
            var a = this;
            $(a).next().html("Loading.., Please wait."), $(a).next().fadeToggle(1e3);
            var r, s = $(this).attr("rel").split("_");
            $(this).attr("rel");
            $("#" + s[1] + "_" + s[2]).show(), "O" == $.trim(s[2]) ? r = JSLINQ(e[0]).Where(function (e) {
                return e.LineNumber == s[1]
            }).Select(function (e) {
                return e
            }) : "R" == $.trim(s[2]) && (r = JSLINQ(e[1]).Where(function (e) {
                return e.LineNumber == s[1]
            }).Select(function (e) {
                return e
            }));
            var l = JSLINQ(r.items).Where(function (e) {
                return "1" == e.Flight
            }).Select(function (e) {
                return e
            }),
                n = JSLINQ(r.items).Where(function (e) {
                    return "2" == e.Flight
                }).Select(function (e) {
                    return e
                });
            if (l.items.length > 0 || n.items.length > 0) {
                var o, d, c;
                l.items.length > 0 ? (o = new Array(l.items), d = l.items[0].sno, c = l.items[0].Provider) : n.items.length > 0 && (o = new Array(n.items), d = n.items[0].sno, c = n.items[0].Provider);
                var p = UrlBase + "FareRuleService.asmx/GetFareRule";
                $.ajax({
                    url: p,
                    type: "POST",
                    data: JSON.stringify({
                        JsonArr: o,
                        sno: d,
                        Provider: c
                    }),
                    dataType: "json",
                    type: "POST",
                    async: !0,
                    contentType: "application/json; charset=utf-8",
                    success: function (e) {
                        var t = "";
                        t += '<div class="depcity">', t += '<div style="width: 60%; margin:10% 0 0 20% !important; padding: 10px;background: #ffffff;box-shadow:0px 0px 4px #333;margin: auto;border: 6px solid #eee;left: 34%; top: 182px;">', t += '<div style="cursor:pointer; float:right; position:relative; top:-12px; right:3px;font-size:20px" onclick="DiplayMsearch(' + $.trim($(a).next()[0].id) + ');">X</div>', t += '<div class="large-12 medium-12 small-12 bld">Fare Rule</div>', null == e.d.Response ? t += "<div>fare rule not available.</div>" : t += '<div style="overflow-y:scroll;height:250px;">' + e.d.Response.FareRules[0].FareRuleDetail + "</div>", t += "</div>", t += "</div>", $(a).next().html(t)
                    },
                    error: function (e, t, i) {
                        alert("Please try after sometime!!")
                    }
                })
            } else $(a).next().html("fare rule not available."), t.fareRuleToolTip.tooltip({
                track: !0,
                delay: 0,
                showURL: !1,
                fade: 100,
                bodyHandler: function () {
                    return $(a).next().html()
                },
                showURL: !1
            })
        }
    }), $(".FareBreakupHederClose").click(function () {
        $("#FareBreakupHeder").removeClass("show"), $("#FareBreakupHederId").html("")
    })
}, ResHelper.prototype.CreateFareBreakUp = function (e) {
    var t;
    e.AdtTax, e.AdtFSur, e.AdtFare, e.ChdTax, e.ChdFSur, e.ChdFare, e.InfTax, e.InfFSur, e.InfFare, this.DisplayPromotionalFare(e);
    t = "Special Fare" == e.AdtFareType ? e.ADTAgentMrk * e.Adult + e.CHDAgentMrk * e.Child : e.TotMrkUp;
    e.TotCB;
    var i = e.TotTds;
    parseInt(t / (e.Adult + e.Child));
    return strResult = "<div><span onclick=\"Close('" + e.LineNumber + '_\');" title="Click to close Details"></span></div>', strResult += "<div class='new-fare-details ' id='FareBreak'>", strResult += "<div class='new-fare-d-1'>", strResult += "<div class='nw-b2b-fared'>", strResult += "<div class='nw-pad-b2'>", strResult = strResult + "<div class='nw-far-1'><span>" + e.Adult + " </span>x Adult</div>", strResult = strResult + "<div class='nw-far-2'>₹ <span>" + e.AdtBfare + "</span></div>", strResult += "</div>", strResult += "</div>", e.Child > 0 && (strResult += "<div class='nw-b2b-fared'>", strResult += "<div class='nw-pad-b2'>", strResult = strResult + "<div class='nw-far-1'><span>" + e.Child + " </span>x Child</div>", strResult = strResult + "<div class='nw-far-2'>₹ <span>" + e.ChdBFare + "</span></div>", strResult += "</div>", strResult += "</div>"), e.Infant > 0 && (strResult += "<div class='nw-b2b-fared'>", strResult += "<div class='nw-pad-b2'>", strResult = strResult + "<div class='nw-far-1'><span>" + e.Infant + " </span>x Child</div>", strResult = strResult + "<div class='nw-far-2'>₹ <span>" + e.InfBfare + "</span></div>", strResult += "</div>", strResult += "</div>"), 1 == e.IsCorp ? (strResult += "<div class='nw-b2b-fared'>", strResult += "<div class='nw-pad-b2'>", strResult += "<div class='nw-far-1'><span>Total Taxes & Fees</span></div>", strResult = strResult + "<div class='nw-far-2'>₹ <span>" + e.TotalFare + "</span></div>", strResult += "</div>", strResult += "</div>", strResult += "<div class='nw-b2b-fared'>", strResult += "<div class='nw-pad-b2'>", strResult += "<div class='nw-far-1'><span>Gross Total</span></div>", strResult = strResult + "<div class='nw-far-2'>₹ <span>" + e.TotalFare + "</span></div>", strResult += "</div>", strResult += "</div>", strResult += "<div class='nw-b2b-fared'>", strResult += "<div class='nw-pad-b2'>", strResult += "<div class='nw-far-1'><span>Net Fare</span></div>", strResult = strResult + "<div class='nw-far-2'>₹ <span>" + e.NetFare + "</span></div>", strResult += "</div>", strResult += "</div>") : (strResult += "<div class='nw-b2b-fared'>", strResult += "<div class='nw-pad-b2'>", strResult += "<div class='nw-far-1'><span>Total Taxes & Fees</span></div>", strResult = strResult + "<div class='nw-far-2'>₹ <span>" + e.TotalTax + "</span></div>", strResult += "</div>", strResult += "</div>", strResult += "<div class='nw-b2b-fared'>", strResult += "<div class='nw-pad-b2'>", strResult += "<div class='nw-far-1'><span>Gross Total</span></div>", strResult = strResult + "<div class='nw-far-2'>₹ <span>" + e.TotalFare + "</span></div>", strResult += "</div>", strResult += "</div>", strResult += "<div class='nw-b2b-fared'>", strResult += "<div class='nw-pad-b2'>", strResult += "<div class='nw-far-1'><span>Net Fare</span></div>", strResult = strResult + "<div class='nw-far-2'>₹ <span>" + e.NetFare + "</span></div>", strResult += "</div>", strResult += "</div>"), strResult += "</div>", strResult += "<div class='can-bnew-rg'>", strResult += "<div class='lef-b2b-cane'>", strResult += "<div class='b2b-ca-char'>Tax & Other Fees</div>", strResult += " <table rules='all' border='1' class='b2b-can-tabe' style='border:1px solid #ddd;'>", strResult += "<tbody>", strResult += "<tr>", strResult += " <td scope='row' width='50%'><span>SGST Airline</span></td>", strResult += "<td width='50%'>", strResult += "<span>₹ 0</span>", strResult += "</td>", strResult += "</tr>", strResult += "<tr>", strResult += " <td scope='row' width='50%'><span>CGST Airline</span></td>", strResult += "<td width='50%'>", strResult += "<span>₹ 0</span>", strResult += "</td>", strResult += "</tr>", strResult += "<tr>", strResult += " <td scope='row' width='50%'><span>IGST Airline</span></td>", strResult += "<td width='50%'>", strResult += "<span>₹ 0</span>", strResult += "</td>", strResult += "</tr>", strResult += "<tr>", strResult += " <td scope='row' width='50%'><span>Fuel Surcharge</span></td>", strResult += "<td width='50%'>", strResult = strResult + "<span>₹ " + e.TotalFuelSur + "</span>", strResult += "</td>", strResult += "</tr>", strResult += "<tr>", strResult += " <td scope='row' width='50%'><span>Transaction Charge</span></td>", strResult += "<td width='50%'>", strResult += "<span>₹ 0</span>", strResult += "</td>", strResult += "</tr>", strResult += "</tbody>", strResult += "</table>", strResult += "</div>", strResult += "<div class='rig-b2b-cane'>", strResult += "<div class='b2b-ca-char'>Commission</div>", strResult += "<table rules='all' border='1' class='b2b-can-tabe' style='border:1px solid #ddd;'>", strResult += "<tbody>", strResult += "<td scope='row' width='50%'><span>Commission</span></td>", strResult += " <td width='50%'>", strResult = strResult + "<span>₹ " + e.TotDis + "</span>", strResult += "</td>", strResult += "</tr>", strResult += "<td scope='row' width='50%'><span>TDS</span></td>", strResult += "<td width='50%'>", strResult = strResult + "<span>₹ " + i + "</span>", strResult += "</td>", strResult += "</tr>", strResult += "<tr>", strResult += "<td scope='row'>CGST Company</td>", strResult += "<td>", strResult += "<span>₹ 0</span>", strResult += "</td>", strResult += "</tr>", strResult += "<tr>", strResult += "<td scope='row'>SGST Company</td>", strResult += "<td>", strResult += "<span>₹ 0</span>", strResult += "</td>", strResult += "</tr>", strResult += "<tr>", strResult += "<td scope='row'>IGST Company</td>", strResult += "<td>", strResult += "<span>₹ 0</span>", strResult += "</td>", strResult += "</tr>", strResult += "<tr>", strResult += "<td scope='row'>Transaction Charge</td>", strResult += "<td>", strResult += "<span>₹ 0</span>", strResult += "</td>", strResult += "</tr>", strResult += "</tbody>", strResult += "</table>", strResult += "</div>", strResult += "<div class='tr-b2b-cancgr'>", strResult += "<div class='trm-had'>Terms &amp; Conditions</div>", strResult += "<div class='terms-b2b2-cancahe'>", strResult += "<ul>", strResult += "<li>Penalty is subject to 4 hours prior to departure and no changes are allowed after that.</li>", strResult += "<li>The charges will be on per passenger per sector</li>", strResult += "<li>Rescheduling Charges = Rescheduling/Change Penalty + Fare Difference (if applicable)</li>", strResult += "<li>Partial cancellation is not allowed on the flight tickets which are book under special discounted fares</li>", strResult += "<li>In case, the customer have not cancelled the ticket within the stipulated time or no show then only statutory taxes are refundable from the respective airlines</li>", strResult += "<li>For infants there is no baggage allowance</li>", strResult += "<li>In certain situations of restricted cases, no amendments and cancellation is allowed</li>", strResult += "<li>Penalty from airlines needs to be reconfirmed before any cancellation or amendments</li>", strResult += "<li>Penalty changes in airline are indicative and can be changes without any prior notice</li>", strResult += " </ul>", strResult += "</div>", strResult += "</div>", strResult += "</div>", strResult += "</div>", strResult += "<div class='clear'></div>", strResult += "<div class='clear'></div>", strResult
}, ResHelper.prototype.DisplayPromotionalFare = function (e) {
    var t = "";
    try {
        "A" == e.ProductClass && ("NRM" == e.AdtFar || "CRP" == e.AdtFar) && "6E" == e.ValiDatingCarrier && parseInt(e.Adult) + parseInt(e.Child) >= 4 ? t = "Friend and Family special fares (*T&C Apply.)" : "NRM" != e.AdtFar && "CRP" != e.AdtFar || "SG" != e.ValiDatingCarrier || "E" != e.RBD && "F" != e.RBD && "H" != e.RBD && "J" != e.RBD && "K" != e.RBD || !(parseInt(e.Adult) + parseInt(e.Child) >= 4) || (t = "Friend and Family special fares (*T&C Apply.) ")
    } catch (e) { }
    return t
}, ResHelper.prototype.MakeupTotDur = function (e) {
    var t, i = "";
    if ("" != $.trim(e))
        if ($.trim(e).search("hrs") > 0) t = (i = $.trim(e).replace(" hrs", ":").replace(" min", "")).split(":"), i = ($.trim(t[0]).length > 1 ? t[0] : "0" + t[0]) + ":" + ($.trim(t[1]).length > 1 ? $.trim(t[1]) : "0" + $.trim(t[1]));
        else if ($.trim(e).search(":") > 0) i = $.trim(e);
        else if (parseInt(e) < 60) i = "00:" + ($.trim(e).length > 1 ? $.trim(e) : "0" + $.trim(e));
        else {
            var a = $.trim(parseInt(parseInt(e) / 60).toString()),
                r = $.trim(parseInt(parseInt(e) % 60).toString());
            i = (a.length > 1 ? a : "0" + a) + ":" + ($.trim(r).length > 1 ? r : "0" + r)
        }
    return i
}, ResHelper.prototype.MakeupAdTime = function (e) {
    return e.length < 4 && (1 == e.length ? e = "000" + e : 2 == e.length ? e = "00" + e : 3 == e.length && (e = "0" + e)), $.trim(e).search(":") <= 0 ? $.trim(e).slice(0, 2) + ":" + $.trim(e).slice(2, 4) : e
}, ResHelper.prototype.GetMinMaxPrice = function (e) {
    var t = new Array,
        i = JSLINQ(e).Select(function (e) {
            return e.TotalFare
        });
    return t.push(Math.min.apply(Math, i.items)), t.push(Math.max.apply(Math, i.items)), t
}, ResHelper.prototype.GetMinMaxTime = function (e) {
    var t = this,
        i = new Array,
        a = JSLINQ(e).Select(function (e) {
            return t.MakeupAdTime(e.DepartureTime).replace(":", "")
        });
    return i.push(Math.min.apply(Math, a.items)), i.push(Math.max.apply(Math, a.items)), i
}, ResHelper.prototype.GetUniqueAirline = function (e, t, i) {
    var a = new Array,
        r = new Array,
        s = JSLINQ(e).OrderBy(function (e) {
            return e.AirLineName
        }).Select(function (e) {
            return e.AirLineName
        }),
        l = JSLINQ(e).OrderBy(function (e) {
            return e.AirLineName
        }).Select(function (e) {
            return e.ValiDatingCarrier
        });
    a = s.items.unique1(), r = l.items.unique1();
    for (var n = new Array, o = 0; o < e.length; o++) n.push(e[o].AirLineName);
    var d = {};
    $(n).each(function (e, t) {
        var i = t;
        d[i] = i in d ? d[i] + 1 : 1
    });
    var c = '<div class="jplist-group" data-control-type="Airlinefilter' + i + '" data-control-action="filter"  data-control-name="Airlinefilter' + i + '"';
    for (var p in c += ' data-path=".' + t + '" data-logic="or">', d)
        for (o = 0; o < a.length; o++) p == a[o] && (c += '<div class="al-multifare mt-10">', c += '<ul class="flight__airline__list">', c += '<li class="flight__airline__item mt-0"><span class="flight__airline__name"><img alt="" src="../Airlogo/sm' + r[o] + '.gif" style="width:17px;"></span><span class="airline_total-search">' + d[p] + '</span></span><span class="pull-right"><span class="airline__total-price">' + p + '</span><input type="checkbox" id="CheckboxA' + i + o + '1" value="' + a[o] + '" class="al-checkfield airline_filtercheck" style="height: 16px;"><label for="' + a[o] + '" class="al-label"></label></span></li>', c += "</ul>", c += "</div>");
    c += "</div>", "O" == i ? this.airlineFilter.html(c) : this.airlineFilterR.html(c)
}, ResHelper.prototype.GetStopFilter = function (e, t, i) {
    var a = new Array;
    a = JSLINQ(e).OrderBy(function (e) {
        return e.Stops
    }).Select(function (e) {
        return e.Stops
    }).items.unique1();
    var r = '<div class="jplist-group" data-control-type="Stopfilter' + i + '" data-control-action="filter"  data-control-name="Stopfilter' + i + '"';
    r += ' data-path=".' + t + '" data-logic="or">', r += '<div class="stopslist">';
    for (var s = 0; s < a.length; s++) r += '<li class="flt-stops-airline "><input value="' + a[s] + '"  id="CheckboxS' + i + s + '1"  type="checkbox"  style="display:none;"/><label for="CheckboxS' + i + s + '1" class="stop-flt">' + a[s] + "</label></li>";
    r += "</div>", r += "</div>", "O" == i ? this.stopFilter.html(r) : this.stopFilterR.html(r)
}, ResHelper.prototype.GetMatrix = function (e, t) {
    var i = new Array;
    i = JSLINQ(e).OrderBy(function (e) {
        return e.Stops
    }).Select(function (e) {
        return e.Stops
    }).items.unique();
    var a, r = new Array;
    r = JSLINQ(e).OrderBy(function (e) {
        return e.TotalFare
    }).Select(function (e) {
        return $.trim(e.MarketingCarrier) + "_" + $.trim(e.AirLineName)
    }).items.unique1();
    for (var s = 0; s < r.length; s++) $.trim(r[s]).search("null") >= 0 && r.splice(s, 1);
    a = "O" == t ? ".airstopO" : ".airstopR";
    var l = "";
    l = '<div class="jplist-group w100" style="overflow-x:scroll;"  data-control-type="button-text-filter-group' + t + '"  data-control-action="filter"  data-control-name="button-text-filter-group-' + t + '">';
    l += '<table class="matrix passenger brdblue" cellpadding="0" cellspacing="0" >';
    for (s = 0; s < i.length + 1; s++) {
        l += "<tr>";
        for (var n = 0; n < r.length + 1; n++)
            if (0 == s)
                if (0 == s && 0 == n) l += '<td class="f16 bld bgmn1" style="min-width:70px;" onclick="mtrxx()" id="' + s + '">', l += '<button type="button" class="jplist-reset-btn colorwht" data-control-type="reset" data-control-name="reset1" data-control-action="reset" style="border: none; background: none; cursor:pointer;">ALL</button>', l += "</td>";
                else {
                    var o = r[n - 1].split("_");
                    l += '<td id="' + n + '"><div data-path=".airlineImage" data-button="true"  data-text="' + o[1] + '" data-fltr="" > <img alt="" src="../Airlogo/sm' + o[0] + '.gif"  title="' + o[1] + '" /></div></td>'
                }
            else 0 != s && 0 == n ? l += '<td  class="colorwht bgmn1" id="' + n + '"><div data-path=".stops" data-button="true"  data-text="' + i[s - 1] + '" data-fltr="" > ' + i[s - 1] + "</div> </td>" : (l += '<td id="' + n + '" style="min-width:50px;">' + this.GetFareForMatrix(r[n - 1], i[s - 1], e, a), l += "</td>");
        l += "</tr>"
    }
    return l += "</table>", l += "</div>"
}, ResHelper.prototype.GetFareForMatrix = function (e, t, i, a) {
    var r = e.split("_"),
        s = JSLINQ(i).Where(function (e) {
            return $.trim(e.AirLineName).toLowerCase() == $.trim(r[1]).toLowerCase() && $.trim(e.Stops).toLowerCase() == $.trim(t).toLowerCase()
        }).Select(function (e) {
            return e.TotalFare
        }),
        l = Math.min.apply(Math, s.items),
        n = "";
    if (s.items.length > 0) {
        var o = $.trim(t).toString().toLowerCase() + "_" + $.trim(r[1]).toString().toLowerCase().replace(" ", "_");
        n += '<div  data-path="' + a + '" data-button="true"  data-text="' + o + '"  data-fltr="' + o + '" >', n += '<img src="' + UrlBase + 'Images/rs.png" style="" />&nbsp;' + l, n += "</div>"
    } else n = "-";
    return n
}, Array.prototype.unique = function () {
    for (var e = [], t = this.length, i = 0; i < t; i++) {
        for (var a = i + 1; a < t; a++) this[i] === this[a] && (a = ++i);
        e.push(this[i])
    }
    return e
}, Array.prototype.unique1 = function () {
    for (var e = [], t = this.length, i = 0; i < t; i++)
        if (0 == i) e.push(this[i]);
        else {
            for (var a = !1, r = 0; r < e.length; r++) $.trim(e[r]).toLowerCase() === $.trim(this[i]).toLowerCase() && (a = !0);
            0 == a && e.push(this[i])
        } return e
}, ResHelper.prototype.calFlightDur = function (e, t) {
    var i = parseInt(t.slice(0, 2), 10) - parseInt(e.slice(0, 2), 10);
    i < 0 && (i += 24);
    var a = parseInt(t.slice(2), 10) - parseInt(e.slice(2), 10);
    a < 0 && (a = 60 + a, i -= 1);
    var r = this.getFourDigitTime($.trim(i.toString())),
        s = this.getFourDigitTime($.trim(a.toString()));
    return [r.slice(2), ":", s.slice(2)].join("")
}, ResHelper.prototype.getFourDigitTime = function (e) {
    return 1 == e.toString().length ? "000" + e.toString() : 2 == e.toString().length ? "00" + e.toString() : 3 == e.toString().length ? "0" + e.toString() : e
}, ResHelper.prototype.GetFltDetails = function (e) {
    var t = this;
    $(".fltDetailslink").click(function (i) {
        if (i.preventDefault(), null != this.rel) {
            $("#" + this.rel + "_").slideUp();
            var a = this.rel,
                r = JSLINQ(e[0]).Where(function (e) {
                    return e.LineNumber == a
                }).Select(function (e) {
                    return e
                }),
                s = JSLINQ(r.items).Where(function (e) {
                    return "1" == e.Flight
                }).Select(function (e) {
                    return e
                }),
                l = JSLINQ(r.items),
                n = JSLINQ(r.items).Select(function (e) {
                    return e.Flight
                }),
                o = Math.max.apply(Math, n.items),
                d = '<div class="bdr-fltdet">';
            if (s.items.length > 0) {
                var c = "";
                c = "TB" == s.items[0].Provider ? t.GetTotalDuration(s.items) : t.MakeupTotDur(s.items[0].TotDur);
                for (var p = 0; p < s.items.length; p++) {
                    p >= 1 && s.items.length > 1 && p < s.items.length && (d += t.GetLayOver(s.items, p)), "6E" == s.items[p].MarketingCarrier && $.trim(s.items[p].sno).search("INDIGOCORP") >= 0 ? d += '<div class="col-md-2 col-xs-3"><img alt="" src="' + UrlBase + 'AirLogo/smITZ.gif" />' + s.items[p].FlightIdentification + "<br/>Class(" + s.items[p].AdtRbd + ")</div>" : d += '<div class="col-md-2 col-xs-3"><img alt="" src="' + UrlBase + "AirLogo/sm" + s.items[p].MarketingCarrier + '.gif" /><br />' + s.items[p].MarketingCarrier + " - " + s.items[p].FlightIdentification + "<br/>Class(" + s.items[p].AdtRbd + ")</div>";
                    var m = t.calFlightDur(s.items[p].DepartureTime.replace(":", ""), s.items[p].ArrivalTime.replace(":", "")) + "";
                    "I" == s.items[0].Trip && "1G" == s.items[0].Provider && (m = s.items[p].TripCnt + " HRS"), d += '<div class="col-md-3 col-xs-3">', d += '<div class="theme-search-results-item-flight-section-meta">', d += '<p class="theme-search-results-item-flight-section-meta-time">' + s.items[p].DepartureCityName + "<span>" + [s.items[p].DepartureTime.replace(":", "").slice(0, 2), ":", s.items[p].DepartureTime.replace(":", "").slice(2)].join("") + "</span></p>", d += '<p class="theme-search-results-item-flight-section-meta-date">' + s.items[p].Departure_Date + "</p>", d += '<p class="theme-search-results-item-flight-section-meta-city">Terminal - ' + s.items[p].DepartureTerminal + "</p>", d += "</div > ", d += "</div > ", d += '<div class="col-md-4 col-xs-2">', d += '<div class="fly">', d += '<div class="fly1">', d += '<span class="jtm">' + m + "</span>", d += "</div>", d += '<div class="fly2">', d += '<span class="fart fart3">' + s.items[p].AdtFareType + "</span>", d += "</div>", d += "</div>", d += "</div>", d += '<div class="col-md-3 col-xs-3" style="text-align: end;">', d += '<div class="theme-search-results-item-flight-section-meta">', d += '<p class="theme-search-results-item-flight-section-meta-time"><span>' + [s.items[p].ArrivalTime.replace(":", "").slice(0, 2), ":", s.items[p].ArrivalTime.replace(":", "").slice(2)].join("") + "</span> " + s.items[p].ArrivalCityName + "</p>", d += '<p class="theme-search-results-item-flight-section-meta-date">' + s.items[p].Arrival_Date + "</p>", d += '<p class="theme-search-results-item-flight-section-meta-city">Terminal - ' + s.items[p].ArrivalTerminal + "</p>", d += "</div > ", d += "</div > "
                }
            }
            for (var u = 2; u <= o; u++) {
                if ((l = JSLINQ(r.items).Where(function (e) {
                    return e.Flight == u.toString()
                }).Select(function (e) {
                    return e
                })).items.length > 0) {
                    c = "";
                    c = "TB" == l.items[0].Provider ? t.GetTotalDuration(l.items) : t.MakeupTotDur(l.items[0].TotDur), d += '</div><div class="" style="position:relative;top:12px;"><div class="large-12 medium-12 small-12 bld" style="position: relative;top: -10px;float: left;width: 100%;padding: 2px 0px 3px 3px;background: #eeeeee;border-bottom: 1px solid #a2a2a2;"><span>' + l.items[0].DepartureCityName + "-" + l.items[l.items.length - 1].ArrivalCityName + "</span>&nbsp;" + c + '</div><div class="clear"></div>';
                    for (var h = 0; h < l.items.length; h++) {
                        h >= 1 && l.items.length > 1 && h < l.items.length && (d += t.GetLayOver(l.items, h)), "6E" == l.items[h].MarketingCarrier && $.trim(l.items[h].sno).search("INDIGOCORP") >= 0 ? d += '<div class="col-md-2 col-xs-3"><img alt="" src="' + UrlBase + 'AirLogo/smITZ.gif" /><br />' + l.items[h].FlightIdentification + "<br/>Class(" + l.items[h].AdtRbd + ")</div>" : d += '<div class="col-md-2 col-xs-3"><img alt="" src="' + UrlBase + "AirLogo/sm" + l.items[h].MarketingCarrier + '.gif" /><br />' + l.items[h].MarketingCarrier + " - " + l.items[h].FlightIdentification + "<br/>Class(" + l.items[h].AdtRbd + ")</div>";
                        t.calFlightDur(l.items[h].DepartureTime.replace(":", ""), l.items[h].ArrivalTime.replace(":", ""));
                        "I" == l.items[0].Trip && "1G" == l.items[0].Provider && l.items[h].TripCnt + " HRS", d += '<div class="col-md-3 col-xs-3">', d += '<div class="theme-search-results-item-flight-section-meta">', d += '<p class="theme-search-results-item-flight-section-meta-time">' + l.items[h].DepartureLocation + "<span>" + [l.items[h].DepartureTime.replace(":", "").slice(0, 2), ":", l.items[h].DepartureTime.replace(":", "").slice(2)].join("") + "</span></p>", d += '<p class="theme-search-results-item-flight-section-meta-date">' + l.items[h].Departure_Date + "</p>", d += '<p class="theme-search-results-item-flight-section-meta-city">Terminal - ' + l.items[h].DepartureTerminal + "</p>", d += "</div > ", d += "</div > ", d += '<div class="col-md-4 col-xs-2">', d += '<div class="fly">', d += '<div class="fly1">', d += '<span class="jtm">' + m + "</span>", d += "</div>", d += '<div class="fly2">', d += '<span class="fart fart3">' + s.items[h].AdtFareType + "</span>", d += "</div>", d += "</div>", d += "</div>", d += '<div class="col-md-3 col-xs-3" style="text-align: end;">', d += '<div class="theme-search-results-item-flight-section-meta">', d += '<p class="theme-search-results-item-flight-section-meta-time"><span>' + [l.items[h].ArrivalTime.replace(":", "").slice(0, 2), ":", l.items[h].ArrivalTime.replace(":", "").slice(2)].join("") + "</span> " + l.items[h].ArrivalLocation + "</p>", d += '<p class="theme-search-results-item-flight-section-meta-date">' + l.items[h].Arrival_Date + "</p>", d += '<p class="theme-search-results-item-flight-section-meta-city">Terminal - ' + l.items[h].ArrivalTerminal + "</p>", d += "</div > ", d += "</div > "
                    }
                }
            }
            d += '<div class="clear"></div>', d += '</div></div><div class="clear"></div>', d += '<div class="clear"></div>', $("#" + this.rel + "_fltdt").html(d)
        }
    }), $(".fltBagDetails").click(function (i) {
        if (i.preventDefault(), null != this.rel) {
            $("#" + this.rel + "_").slideUp();
            var a = this.rel,
                r = JSLINQ(e[0]).Where(function (e) {
                    return e.LineNumber == a
                }).Select(function (e) {
                    return e
                }),
                s = JSLINQ(r.items).Where(function (e) {
                    return "1" == e.Flight
                }).Select(function (e) {
                    return e
                }),
                l = JSLINQ(r.items).Where(function (e) {
                    return "2" == e.Flight
                }).Select(function (e) {
                    return e
                }),
                n = "<div>";
            if (s.items.length > 0) {
                n += '<div class=""><div></div>', n += '<table class="rtable"><tr><td  class="w50 f16 bld">Sector</td><td class="f16 bld">Baggage Quantity</td></tr>';
                for (var o = 0; o < s.items.length; o++) n += "<tr><td>" + s.items[o].DepartureCityName + "-" + s.items[o].ArrivalCityName + "(" + s.items[o].MarketingCarrier + " - " + s.items[o].FlightIdentification + ")</td>", n += "<td>" + t.BagInfo(s.items[o].BagInfo) + "</td></tr>";
                n += "</table>", n += '<div class="padding1 f10 w95 mauto lh13">The information presented above is as obtained from the airline reservation system. RWT does not guarantee the accuracy of this information. The baggage allowance may vary according to stop-overs, connecting flights and changes in airline rules.</div>', n += "</div>"
            }
            if (l.items.length > 0) {
                n += '<div class="depcity1">', n += '<table class="w100 f12">';
                for (var d = 0; d < l.items.length; d++) n += '<tr><td class="w50">' + l.items[d].DepartureCityName + "-" + l.items[d].ArrivalCityName + "(" + l.items[d].MarketingCarrier + " - " + l.items[d].FlightIdentification + ")</td>", n += "<td>" + t.BagInfo(l.items[d].BagInfo) + "</td></tr>";
                n += "</table>", n += "</div>"
            }
            n += '<div class="clear1"></div>', $("#" + this.rel + "_bag").html(n)
        }
    })
}, ResHelper.prototype.GetLayOver = function (e, t) {
    var i = this.calFlightDur(e[t - 1].ArrivalTime.replace(":", ""), e[t].DepartureTime.replace(":", ""));
    return '<div class="stm"><span class="lay">Layover:' + e[t].DepartureCityName + " (" + e[t].DepartureLocation + ") - " + i + "</span></div>"
}, ResHelper.prototype.GetTotalDuration = function (e) {
    for (var t = 0, i = 0, a = 0; a < e.length; a++) {
        if (a > 0) {
            var r = this.calFlightDur(e[a - 1].ArrivalTime.replace(":", ""), e[a].DepartureTime.replace(":", ""));
            t += parseInt(r.split(":")[0]), i += parseInt(r.split(":")[1])
        }
        t += parseInt(e[a].TripCnt.split(":")[0]), i += parseInt(e[a].TripCnt.split(":")[1])
    }
    i > 60 && (t += parseInt(i / 60), i = i % 60);
    var s = this.getFourDigitTime($.trim(t.toString())),
        l = this.getFourDigitTime($.trim(i.toString()));
    return [s.slice(2), ":", l.slice(2)].join("")
}, ResHelper.prototype.FormatedDate = function (e) {
    var t = new Array(7);
    t[0] = "Sun", t[1] = "Mon", t[2] = "Tue", t[3] = "Wed", t[4] = "Thu", t[5] = "Fri", t[6] = "Sat";
    var i = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"),
        a = e.split("/");
    let r = parseInt(a[2]) + "-" + parseInt(a[1]) + "-" + parseInt(a[0]),
        s = t[new Date(r).getDay()],
        l = i[parseInt(a[1]) - 1];
    return s + ", " + a[0] + " " + l
}, ResHelper.prototype.GetSearchDisplay = function (e, t) {
    var i = this,
        a = "",
        r = "",
        s = "",
        l = "",
        n = "",
        o = "",
        d = "";
    let c = e.DepDate.split("/");
    var p = new Date(c[2] + "-" + c[1] + "-" + c[0]);
    p.setDate(p.getDate() - 1), String(p.getDate()).padStart(2, "0"), String(p.getMonth() + 1).padStart(2, "0"), p.getFullYear();
    var m = new Date(c[2] + "-" + c[1] + "-" + c[0]);
    m.setDate(m.getDate() + 1), String(m.getDate()).padStart(2, "0"), String(m.getMonth() + 1).padStart(2, "0"), m.getFullYear(), r += "<span>" + getTheDays(m) + "</span>", s += "<span>" + getTheDays(p) + "</span>";
    var u;
    u = parseInt(e.DepDate.split("/")[0]) + 1;
    var h = parseInt(e.DepDate.split("/")[0]) - 1 + "/" + e.DepDate.split("/")[1] + "/" + e.DepDate.split("/")[2],
        v = u + "/" + e.DepDate.split("/")[1] + "/" + e.DepDate.split("/")[2];
    l += '<span style="font-size:10px;">' + i.FormatedDate(v) + "</span>", n += '<span style="font-size:10px;">' + i.FormatedDate(h) + "</span>";
    var f;
    f = parseInt(e.DepDate.split("/")[0]) + 1;
    var g = parseInt(e.DepDate.split("/")[0]) - 1 + "/" + e.DepDate.split("/")[1] + "/" + e.DepDate.split("/")[2],
        y = f + "/" + e.DepDate.split("/")[1] + "/" + e.DepDate.split("/")[2];
    o += '<span style="font-size:10px;">' + i.FormatedDate(y) + "</span>", d += '<span style="font-size:10px;">' + i.FormatedDate(g) + "</span>", i.nextdate.html(r), i.prevdate.html(s), i.ondt.html(l), i.opdt.html(n), i.rndt.html(o), i.rpdt.html(d), "rdbOneWay" == t ? (a += "<div>", a += "<div>", a += '<div class="placeoneway-flights">', a += '<ul class="al-selist">', a += '<li class="whitecolor">' + i.hidtxtDepCity1.val().split(", ")[0], a += "<br>", a += '<div class="atls-holdbg"><span class="atls-hdcontent">Delhi, India</span><div class="altsmain">', a += '<div class="alts-content">', a += '<p class="cancellation-details atlscont">Delhi, India</p>', a += "</div>", a += "</div>", a += "</div>", a += "</li>", a += '<li><i class="fa fa-plane"></i></li>', a += '<li class="whitecolor">' + i.hidtxtArrCity1.val().split(", ")[0], a += "<br>", a += '<div class="atls-holdbg"><span class="atls-hdcontent">Mumbai, India</span><div class="altsmain">', a += '<div class="alts-content">', a += '<p class="cancellation-details atlscont">Mumbai, India</p>', a += "</div>", a += "</div>", a += "</div>", a += "</li>", a += "</ul>", a += "</div>", a += "</div>", a += "</div>", a += "<div>", a += "<div>", a += '<div class="departureoneway-flights">', a += '<div class="al-depdate">', a += "<p>Departure Date</p>", a += '<span class="whitecolor">' + i.FormatedDate(e.DepDate) + "</span></div>", a += "</div >", a += "</div>", a += "</div>", a += '<div class="paxclassoneway-flights">', a += '<div class="al-depdate">', a += "<p>Passengers &amp; Class</p>", a += '<div class="atls-holdbg"><span class="atls-hdcontent whitecolor"><span>Adult ' + e.Adult + "  Child " + e.Child + "  Infant " + e.Infant + " </span></span>", a += '<div class="altsmain">', a += '<div class="alts-content">', a += '<p class="cancellation-details atlscont"><span>Adult ' + e.Adult + "  Child " + e.Child + "  Infant " + e.Infant + "</span></p>", a += "</div>", a += "</div>", a += "</div>", a += '<div class="atls-holdec"><span class="atls-hdcontent whitecolor">&nbsp;| ECONOMY</span><div class="altsmain">', a += '<div class="alts-content">', a += '<p class="cancellation-details atlscont">ECONOMY</p>', a += "</div>", a += "</div>", a += "</div>", a += "</div>", a += "</div >", a += '<div class="preferedoneway-flights">', a += '<div class="al-depdate">', a += "<p>Preferred Airline</p>", a += '<span class="al-none">None</span></div>', a += "</div >", i.DisplaySearchinput.html(a)) : "rdbMultiCity" == t ? (a += ' <div class="lft plft10">', a += '  <div class="lft ">', a += i.hidtxtDepCity1.val().split(",")[0] + "<br />", a += ' <span class="f10 txtgray">' + i.FormatedDate(e.DepDate) + "</span>", a += " </div>", a += ' <div class="lft">', a += '  <i aria-hidden="true" style="color:#fff;">⟶</i></div>', a += '  <div class="lft plft10">', a += i.hidtxtArrCity1.val().split(",")[0] + "<br />", a += "  </div>", a += " </div>", a += '<div class="bdrdot lft">&nbsp;</div>', "" != e.DepartureCity2 && "" != e.ArrivalCity2 && "" != e.DepDate2 && (a += ' <div class="lft plft10">', a += '  <div class="lft ">', a += i.hidtxtDepCity2.val().split(",")[0] + "<br />", a += ' <span class="f10 txtgray">' + i.FormatedDate(e.DepDate2) + "</span>", a += " </div>", a += ' <div class="lft">', a += '  <i aria-hidden="true" style="color:#fff;">⟶</i></div>', a += '  <div class="lft plft10">', a += i.hidtxtArrCity2.val().split(",")[0] + "<br />", a += "  </div>", a += " </div>", a += '<div class="bdrdot lft">&nbsp;</div>'), "" != e.DepartureCity3 && "" != e.ArrivalCity3 && "" != e.DepDate3 && (a += ' <div class="lft plft10">', a += '  <div class="lft ">', a += i.hidtxtDepCity3.val().split(",")[0] + "<br />", a += ' <span class="f10 txtgray">' + i.FormatedDate(e.DepDate3) + "</span>", a += " </div>", a += ' <div class="lft">', a += '  <i aria-hidden="true" style="color:#fff;">⟶</i></div>', a += '  <div class="lft plft10">', a += i.hidtxtArrCity3.val().split(",")[0] + "<br />", a += "  </div>", a += " </div>", a += '<div class="bdrdot lft">&nbsp;</div>'), "" != e.DepartureCity4 && "" != e.ArrivalCity4 && "" != e.DepDate4 && (a += ' <div class="lft plft10">', a += '  <div class="lft ">', a += i.hidtxtDepCity4.val().split(",")[0] + "<br />", a += ' <span class="f10 txtgray">' + i.FormatedDate(e.DepDate4) + "</span>", a += " </div>", a += ' <div class="lft">', a += '  <i aria-hidden="true" style="color:#fff;">⟶</i></div>', a += '  <div class="lft plft10">', a += i.hidtxtArrCity3.val().split(",")[0] + "<br />", a += "  </div>", a += " </div>", a += '<div class="bdrdot lft">&nbsp;</div>'), "" != e.DepartureCity5 && "" != e.ArrivalCity5 && "" != e.DepDate5 && (a += ' <div class="lft plft10">', a += '  <div class="lft ">', a += i.hidtxtDepCity5.val().split(",")[0] + "<br />", a += ' <span class="f10 txtgray">' + i.FormatedDate(e.DepDate5) + "</span>", a += " </div>", a += ' <div class="lft">', a += '  <i aria-hidden="true" style="color:#fff;">⟶</i></div>', a += '  <div class="lft plft10">', a += i.hidtxtArrCity5.val().split(",")[0] + "<br />", a += "  </div>", a += " </div>", a += '<div class="bdrdot lft">&nbsp;</div>'), "" != e.DepartureCity6 && "" != e.ArrivalCity6 && "" != e.DepDate6 && (a += ' <div class="lft plft10">', a += '  <div class="lft ">', a += i.hidtxtDepCity6.val().split(",")[0] + "<br />", a += ' <span class="f10 txtgray">' + i.FormatedDate(e.DepDate6) + "</span>", a += " </div>", a += ' <div class="lft">', a += '  <i aria-hidden="true" style="color:#fff;">⟶</i></div>', a += '  <div class="lft plft10">', a += i.hidtxtArrCity5.val().split(",")[0] + "<br />", a += "  </div>", a += " </div>"), i.DisplaySearchinput.html(a)) : "rdbRoundTrip" != t && "rdbRoundTripF" != t || (a += '<div class="col-md-4 col-xs-3">', a += '<div class="WID theme-search-area-section first theme-search-area-section-curved theme-search-area-section-sm theme-search-area-section-fade-white theme-search-area-section-no-border">', a += '<i class="fa fa-plane p-n" style="background: #fff; color: #000; padding: 5px 9px; border-radius: 50% 50% 50% 50%; margin: 0.1em; font-size: 2em;"></i>', a += '<p class="theme-login-terms" style="color: white; position: relative; margin-top: -37px; margin-left: 28px; font-weight: 800;">', a += i.hidtxtDepCity1.val().split(", ")[0] + "", a += "<br>", a += '<a style="color: #fff; font-weight: initial;">' + i.FormatedDate(e.DepDate) + ' <span class="mob-pax" >| Adult ' + e.Adult + "  Child " + e.Child + "  Infant " + e.Infant + "</span></a>", a += "</p>", a += "</div>", a += "</div>", a += '<div class="col-md-2 col-xs-2"><div class="pln"><img class="r-plane" src="../Images/returnplane.png" /></div></div>', a += '<div class="col-md-4 col-xs-3">', a += '<div class="WID theme-search-area-section theme-search-area-section-curved theme-search-area-section-sm theme-search-area-section-fade-white theme-search-area-section-no-border">', a += '<i class="fa fa-plane p-n" style="background: #fff; color: #000; padding: 5px 9px; border-radius: 50% 50% 50% 50%; margin: 0.1em; transform: rotate(90deg); font-size: 2em;"></i>', a += '<p class="theme-login-terms" style="color: white; position: relative; margin-top: -37px; margin-left: 28px; font-weight: 800;">', a += i.hidtxtArrCity1.val().split(",")[0] + "", a += "<br>", a += '<a style="color: #fff; font-weight: initial;">' + i.FormatedDate(e.DepDate) + ' <span class="mob-pax" >| Adult ' + e.Adult + "  Child " + e.Child + "  Infant " + e.Infant + "</span></a>", a += "</p>", a += "</div>", a += "</div>", i.DisplaySearchinput.html(a))
}, ResHelper.prototype.BagInfo = function (e) {
    if (null != e && "" != e.toString()) {
        var t = "";
        t = $.trim(e).toUpperCase().search("PC") > 0 ? $.trim(e).replace("PC", " Piece(s) Baggage included.") : $.trim(e).toUpperCase().search("K") > 0 && $.trim(e).toString().length < 4 ? $.trim(e).replace("K", " Kg Baggage included.") : e
    } else t = "Baggage information not available.";
    return t
}, ResHelper.prototype.TerminalAirportInfo = function (e, t) {
    var i = "";
    return null != e && "" != e && null != t && "" != t ? i = t.toString().replace("Airport", "").replace("airport", "") + " Airport, Terminal  " + e : null == e || "" == e || null != t && "" != t ? null != e && "" != e || null == t || "" == t || (i = t.toString().replace("Airport", "").replace("airport", "") + " Airport") : i = "Terminal  " + e, i
}, ResHelper.prototype.GetFltDetailsR = function (e) {
    var t = this;
    $(".fltDetailslinkR").click(function (i) {
        var a;
        if (null != $(this).attr("rel")) {
            a = $(this).attr("rel").split("_"), lineNums = $(this).attr("id").split("_");
            var r;
            $(this).attr("rel");
            "O" == $.trim(lineNums[1]) ? r = JSLINQ(e[0]).Where(function (e) {
                return e.LineNumber == lineNums[0]
            }).Select(function (e) {
                return e
            }) : "R" == $.trim(lineNums[1]) && (r = JSLINQ(e[1]).Where(function (e) {
                return e.LineNumber == lineNums[0]
            }).Select(function (e) {
                return e
            }));
            var s = JSLINQ(r.items).Where(function (e) {
                return "1" == e.Flight
            }).Select(function (e) {
                return e
            }),
                l = JSLINQ(r.items).Where(function (e) {
                    return "2" == e.Flight
                }).Select(function (e) {
                    return e
                }),
                n = "";
            if (n += '<div class="">', s.items.length > 0) {
                try {
                    parseInt(s.items[0].AvailableSeats1) <= 5 && s.items[0].ValiDatingCarrier
                } catch (e) { }
                for (var o = 0; o < s.items.length; o++) {
                    if (0 == o) {
                        var d = "";
                        d = "TB" == s.items[0].Provider ? t.GetTotalDuration(s.items) : t.MakeupTotDur(s.items[0].TotDur)
                    } else n += '<div class="clear1"></div><hr /><div class="clear1"></div><div class="large-12 medium-12 small-12 bld">';
                    "6E" == s.items[o].MarketingCarrier && $.trim(s.items[o].sno).search("INDIGOCORP") >= 0 ? n += '<div class="col-md-2"><img alt="" src="' + UrlBase + 'AirLogo/smITZ.gif" /><br />' + s.items[o].FlightIdentification + "<br/>Class(" + s.items[o].AdtRbd + ")</div>" : n += '<div class="col-md-2"><img alt="" src="' + UrlBase + "AirLogo/sm" + s.items[o].MarketingCarrier + '.gif" /><br />' + s.items[o].MarketingCarrier + " - " + s.items[o].FlightIdentification + "<br/>Class(" + s.items[o].AdtRbd + ")</div>";
                    var c = t.calFlightDur(s.items[o].DepartureTime.replace(":", ""), s.items[o].ArrivalTime.replace(":", "")) + " HRS";
                    "I" == s.items[0].Trip && (c = s.items[o].TripCnt + " HRS"), n += '<div class="col-md-3 col-xs-3">', n += '<div class="theme-search-results-item-flight-section-meta">', n += '<p class="theme-search-results-item-flight-section-meta-time">' + s.items[o].DepartureLocation + "<span>" + [s.items[o].DepartureTime.replace(":", "").slice(0, 2), ":", s.items[o].DepartureTime.replace(":", "").slice(2)].join("") + "</span></p>", n += '<p class="theme-search-results-item-flight-section-meta-date">' + s.items[o].Departure_Date + "</p>", n += '<p class="theme-search-results-item-flight-section-meta-city">Terminal - ' + s.items[o].DepartureTerminal + "</p>", n += "</div > ", n += "</div > ", n += '<div class="col-md-4 col-xs-2">', n += '<div class="fly">', n += '<div class="fly1">', n += '<span class="jtm">' + c + "</span>", n += "</div>", n += '<div class="fly2">', n += '<span class="fart fart3">' + s.items[o].AdtFareType + "</span>", n += "</div>", n += "</div>", n += "</div>", n += '<div class="col-md-3 col-xs-3" style="text-align: end;">', n += '<div class="theme-search-results-item-flight-section-meta">', n += '<p class="theme-search-results-item-flight-section-meta-time"><span>' + [s.items[o].ArrivalTime.replace(":", "").slice(0, 2), ":", s.items[o].ArrivalTime.replace(":", "").slice(2)].join("") + "</span> " + s.items[o].ArrivalLocation + "</p>", n += '<p class="theme-search-results-item-flight-section-meta-date">' + s.items[o].Arrival_Date + "</p>", n += '<p class="theme-search-results-item-flight-section-meta-city">Terminal - ' + s.items[o].ArrivalTerminal + "</p>", n += "</div > ", n += "</div > ", n += "</br> ", o >= 1 && s.items.length > 1 && o < s.items.length - 1 && (n += t.GetLayOver(s.items, o))
                }
            }
            if (l.items.length > 0) {
                d = "";
                d = "TB" == l.items[0].Provider ? t.GetTotalDuration(l.items) : t.MakeupTotDur(l.items[0].TotDur), n += '</div><div class="depcity1"><span>' + l.items[0].DepartureCityName + "-" + l.items[l.items.length - 1].ArrivalCityName + "</span>&nbsp;" + d + '<div class="clear"></div>';
                for (var p = 0; p < l.items.length; p++) {
                    "6E" == l.items[p].MarketingCarrier && $.trim(l.items[p].sno).search("INDIGOCORP") >= 0 ? n += '<div class="large-2 medium-2 small-2 columns"><img alt="" src="' + UrlBase + 'AirLogo/smITZ.gif" /><br />' + l.items[p].FlightIdentification + "<br/>Class(" + l.items[p].AdtRbd + ")</div>" : n += '<div class="large-2 medium-2 small-2 columns"><img alt="" src="' + UrlBase + "AirLogo/sm" + l.items[p].MarketingCarrier + '.gif" /><br />' + l.items[p].MarketingCarrier + " - " + l.items[p].FlightIdentification + "<br/>Class(" + l.items[p].AdtRbd + ")</div>";
                    var m = t.calFlightDur(l.items[p].DepartureTime.replace(":", ""), l.items[p].ArrivalTime.replace(":", "")) + " HRS";
                    "I" == l.items[0].Trip && (m = l.items[p].TripCnt + " HRS"), n += '<div class="large-2 medium-2 small-2 columns bld">' + m + "</div>", n += '<div class="large-3 medium-3 small-3 columns"><span>' + l.items[p].DepartureLocation + "&nbsp;" + [l.items[p].DepartureTime.replace(":", "").slice(0, 2), ":", l.items[p].DepartureTime.replace(":", "").slice(2)].join("") + "</span><br />" + l.items[p].DepartureCityName + "<br />" + l.items[p].Departure_Date + "<br />" + t.TerminalAirportInfo(l.items[p].DepartureTerminal, l.items[p].DepartureAirportName) + "</div>", n += '<div class="large-3 medium-3 small-3 columns"><span>' + [l.items[p].ArrivalTime.replace(":", "").slice(0, 2), ":", l.items[p].ArrivalTime.replace(":", "").slice(2)].join("") + "&nbsp;" + l.items[p].ArrivalLocation + "</span><br />" + l.items[p].ArrivalCityName + "<br />" + l.items[p].Arrival_Date + "<br />" + t.TerminalAirportInfo(l.items[p].ArrivalTerminal, l.items[p].ArrivalAirportName) + '</div><div class="clear"></div>', p >= 1 && l.items.length > 1 && p < l.items.length - 1 && (n += t.GetLayOver(l.items, p))
                }
            }
            n += '<div class="clear"></div>', n += "</div></div>", n += '<div class="clear"></div>'
        }
        "O" == a[1] ? $("#" + lineNums[0] + "_Obdfltdt").html(n) : $("#" + lineNums[0] + "_RO").html(n)
    }), $(".fltBagDetailsR").click(function (i) {
        var a;
        if (null != $(this).attr("rel")) {
            a = $(this).attr("rel").split("_");
            var r, s = $(this).attr("id").split("_");
            $(this).attr("rel");
            "O" == $.trim(s[1]) ? r = JSLINQ(e[0]).Where(function (e) {
                return e.LineNumber == s[1]
            }).Select(function (e) {
                return e
            }) : "R" == $.trim(s[1]) && (r = JSLINQ(e[1]).Where(function (e) {
                return e.LineNumber == s[1]
            }).Select(function (e) {
                return e
            }));
            var l = JSLINQ(r.items).Where(function (e) {
                return "1" == e.Flight
            }).Select(function (e) {
                return e
            }),
                n = JSLINQ(r.items).Where(function (e) {
                    return "2" == e.Flight
                }).Select(function (e) {
                    return e
                }),
                o = "";
            if (o += "<div>", l.items.length > 0) {
                o += '<div class="depcity">', o += '<table class="w100 f12" style="padding: 10px; background: #ffffff; box-shadow:0px 0px 15px #333; border: 6px solid #eee;"><tr><td class="f16 bld w50">Sector</td><td class="f16 bld">Baggage Quantity</td></tr>';
                for (var d = 0; d < l.items.length; d++) o += "<tr><td>" + l.items[d].DepartureCityName + "-" + l.items[d].ArrivalCityName + "(" + l.items[d].MarketingCarrier + " - " + l.items[d].FlightIdentification + ")</td>", o += "<td>" + t.BagInfo(l.items[d].BagInfo) + "</td></tr>";
                o += "</table></div>"
            }
            if (n.items.length > 0) {
                o += '<div class="depcity1">', o += '<table class="w100 f12" style="padding: 10px; background: #ffffff; box-shadow:0px 0px 15px #333; border: 6px solid #eee;">';
                for (var c = 0; c < n.items.length; c++) o += '<tr><td class="w50">' + n.items[c].DepartureCityName + "-" + n.items[c].ArrivalCityName + "(" + n.items[c].MarketingCarrier + " - " + n.items[c].FlightIdentification + ")</td>", o += "<td>" + t.BagInfo(n.items[c].BagInfo) + "</td></tr>";
                o += "</table></div>"
            }
            o += "<p>The information presented above is as obtained from the airline reservation system. Faretripbox does not guarantee the accuracy of this information. The baggage allowance may vary according to stop-overs, connecting flights and changes in airline rules.</p>", o += '<div class="clear1"></div>', o += "</div></div>"
        }
        "O" == a[1] && $("#" + s[0] + "_Obdbag").html(o), "R" == a[1] && $("#" + s[0] + "_RO").html(o)
    })
}, ResHelper.prototype.GetResult = function (e) {
    applyfilterStatus = !1;
    var t, i = e;
    i.MainSF.hide();
    var a = $("input[name='TripType']:checked").val(),
        r = i.hidtxtDepCity1.val().split(","),
        s = i.hidtxtArrCity1.val().split(","),
        l = "";
    l = i.hidtxtDepCity2.val().split(",").length > 1 ? i.hidtxtDepCity2.val().split(",")[1].trim() : "IN";
    var n = "";
    n = i.hidtxtArrCity2.val().split(",").length > 1 ? i.hidtxtArrCity2.val().split(",")[1].trim() : "IN";
    var o = "";
    o = i.hidtxtDepCity3.val().split(",").length > 1 ? i.hidtxtDepCity3.val().split(",")[1].trim() : "IN";
    var d = "";
    d = i.hidtxtArrCity3.val().split(",").length > 1 ? i.hidtxtArrCity3.val().split(",")[1].trim() : "IN";
    var c = "";
    c = i.hidtxtDepCity4.val().split(",").length > 1 ? i.hidtxtDepCity4.val().split(",")[1].trim() : "IN";
    var p = "";
    p = i.hidtxtArrCity4.val().split(",").length > 1 ? i.hidtxtArrCity4.val().split(",")[1].trim() : "IN";
    var m = "";
    m = i.hidtxtDepCity5.val().split(",").length > 1 ? i.hidtxtDepCity5.val().split(",")[1].trim() : "IN";
    var u = "";
    u = i.hidtxtArrCity5.val().split(",").length > 1 ? i.hidtxtArrCity5.val().split(",")[1].trim() : "IN";
    var h = "";
    h = i.hidtxtDepCity6.val().split(",").length > 1 ? i.hidtxtDepCity6.val().split(",")[1].trim() : "IN";
    var v = "";
    v = i.hidtxtArrCity6.val().split(",").length > 1 ? i.hidtxtArrCity6.val().split(",")[1].trim() : "IN", t = "IN" == r[1] && "IN" == s[1] && "IN" == n && "IN" == d && "IN" == p && "IN" == u && "IN" == v && "IN" == l && "IN" == o && "IN" == c && "IN" == m && "IN" == h ? "D" : "I";
    var f = new Boolean;
    $(this), f = 1 == i.chkNonstop.is(":checked");
    var g = new Boolean;
    g = 1 == i.LCC_RTF.is(":checked");
    var y, R, b = new Boolean;
    b = 1 == i.GDS_RTF.is(":checked");
    var F = {
        Trip1: t,
        TripType1: a,
        DepartureCity: i.txtDepCity1.val(),
        ArrivalCity: i.txtArrCity1.val(),
        HidTxtDepCity: i.hidtxtDepCity1.val(),
        HidTxtArrCity: i.hidtxtArrCity1.val(),
        Adult: i.Adult.val(),
        Child: i.Child.val(),
        Infant: i.Infant.val(),
        Cabin: i.Cabin.val(),
        AirLine: i.txtAirline.val(),
        HidTxtAirLine: i.hidtxtAirline.val(),
        DepDate: i.txtDepDate.val(),
        RetDate: i.txtRetDate.val(),
        RTF: g,
        NStop: f,
        GDSRTF: b,
        DepartureCity2: i.txtDepCity2.val(),
        ArrivalCity2: i.txtArrCity2.val(),
        HidTxtDepCity2: i.hidtxtDepCity2.val(),
        HidTxtArrCity2: i.hidtxtArrCity2.val(),
        DepDate2: i.txtDepDate2.val(),
        DepartureCity3: i.txtDepCity3.val(),
        ArrivalCity3: i.txtArrCity3.val(),
        HidTxtDepCity3: i.hidtxtDepCity3.val(),
        HidTxtArrCity3: i.hidtxtArrCity3.val(),
        DepDate3: i.txtDepDate3.val(),
        DepartureCity4: i.txtDepCity4.val(),
        ArrivalCity4: i.txtArrCity4.val(),
        HidTxtDepCity4: i.hidtxtDepCity4.val(),
        HidTxtArrCity4: i.hidtxtArrCity4.val(),
        DepDate4: i.txtDepDate4.val(),
        DepartureCity5: i.txtDepCity5.val(),
        ArrivalCity5: i.txtArrCity5.val(),
        HidTxtDepCity5: i.hidtxtDepCity5.val(),
        HidTxtArrCity5: i.hidtxtArrCity5.val(),
        DepDate5: i.txtDepDate5.val(),
        DepartureCity6: i.txtDepCity6.val(),
        ArrivalCity6: i.txtArrCity6.val(),
        HidTxtDepCity6: i.hidtxtDepCity6.val(),
        HidTxtArrCity6: i.hidtxtArrCity6.val(),
        DepDate6: i.txtDepDate6.val()
    };
    if (i.GetSearchDisplay(F, a), "rdbOneWay" == a) i.prexnt.show(), y = F.DepartureCity.split("(")[1].replace(")", "") + " → " + F.ArrivalCity.split("(")[1].replace(")", "") + "<i><image url='https://www.iconfinder.com/data/icons/small-n-flat/24/calendar-512.png' alt=''/></i> " + F.DepDate, i.searchquery.html(y), i.DivRefinetitle.html(i.txtDepCity1.val() + " to " + i.txtArrCity1.val()), F.RetDate = F.DepDate;
    else if ("rdbMultiCity" == a) i.prexnt.show(), y = F.DepartureCity + " To " + F.ArrivalCity + " On  " + F.DepDate + "  <br />", "" != F.DepartureCity2 && "" != F.ArrivalCity2 && "" != F.DepDate2 && (y = y + F.DepartureCity2 + " To " + F.ArrivalCity2 + " On  " + F.DepDate2 + "  <br />"), "" != F.DepartureCity3 && "" != F.ArrivalCity3 && "" != F.DepDate3 && (y = y + F.DepartureCity3 + " To " + F.ArrivalCity3 + " On  " + F.DepDate3 + "  <br />"), "" != F.DepartureCity4 && "" != F.ArrivalCity4 && "" != F.DepDate4 && (y = y + F.DepartureCity4 + " To " + F.ArrivalCity4 + " On  " + F.DepDate4 + "  <br />"), "" != F.DepartureCity5 && "" != F.ArrivalCity5 && "" != F.DepDate5 && (y = y + F.DepartureCity5 + " To " + F.ArrivalCity5 + " On  " + F.DepDate5 + "  <br />"), "" != F.DepartureCity6 && "" != F.ArrivalCity6 && "" != F.DepDate6 && (y = y + F.DepartureCity6 + " To " + F.ArrivalCity6 + " On  " + F.DepDate6 + "  <br />"), i.searchquery.html(y), i.DivRefinetitle.html(i.txtDepCity1.val() + " to " + i.txtArrCity1.val()), F.RetDate = F.DepDate, e.TripType.click();
    else if ("rdbRoundTrip" == a) {
        "D" == t && i.prexnt.hide();
        var D = (y = F.DepartureCity.split("(")[1].replace(")", "") + " → " + F.ArrivalCity.split("(")[1].replace(")", "") + " On  " + F.DepDate) + "<br/>" + (R = F.ArrivalCity.split("(")[1].replace(")", "") + " → " + F.DepartureCity.split("(")[1].replace(")", "") + " On  " + F.RetDate);
        i.searchquery.html(D), y = y + " For " + F.Adult + " Adult " + F.Child + " Child " + F.Infant + " Infant", R = R + " For " + F.Adult + " Adult " + F.Child + " Child " + F.Infant + " Infant", i.SearchTextDiv1.html("One Way from " + i.txtDepCity1.val() + " to " + i.txtArrCity1.val() + " | return " + i.txtArrCity1.val() + " to " + i.txtDepCity1.val()), i.RTFTextFrom.html(i.hidtxtDepCity1.val().split(",")[0] + " → " + i.hidtxtArrCity1.val().split(",")[0] + " | " + i.txtDepDate.val()), i.RTFTextTo.html(i.hidtxtArrCity1.val().split(",")[0] + " → " + i.hidtxtDepCity1.val().split(",")[0] + " | " + i.txtRetDate.val()), i.DivRefinetitle.html(i.txtDepCity1.val() + " to " + i.txtArrCity1.val()), i.flterTabO.html(i.hidtxtDepCity1.val().split(",")[0] + "-" + i.hidtxtArrCity1.val().split(",")[0]), i.flterTabR.html(i.hidtxtArrCity1.val().split(",")[0] + "-" + i.hidtxtDepCity1.val().split(",")[0])
    }
    $.blockUI({
        message: $("#waitMessage")
    });
    var T = !1;
    "D" == t && 0 == g && 0 == b && "rdbRoundTrip" == a ? (T = !0, roundtripNrml1 = !0, CommonResultArray.push([]), CommonResultArray.push([])) : (CommonResultArray.push([]), roundtripNrml1 = !1);
    var C = "";
    "D" == t && 1 == g && "rdbRoundTrip" == a ? C = "LCC" : "D" == t && 1 == b && "rdbRoundTrip" == a && (C = "GDS");
    var x = "";
    "" != $.trim(F.HidTxtAirLine) && (x = F.HidTxtAirLine.split(",")[1]), "rdbMultiCity" == $("input[name='TripType']:checked").val() ? ($("#two").show(), $("#three").show(), $("#add").show(), $("#minus").hide()) : ($("#two").hide(), $("#three").hide(), $("#four").hide(), $("#five").hide(), $("#six").hide(), $("#add").hide());
    var A = "/GetActiveAirlineProviders";
    1 == f && (A = "/GetActiveAirlineProvidersF"), $.ajax({
        url: UrlBase + "FltSearch1.asmx" + A,
        type: "POST",
        data: JSON.stringify({
            org: F.HidTxtDepCity.split(",")[0],
            dest: F.HidTxtArrCity.split(",")[0],
            airline: x,
            rtfStatus: C,
            trip: F.Trip1,
            DepDate: F.DepDate,
            RetDate: F.RetDate
        }),
        async: !0,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function (e, t, i) {
            return !1
        },
        success: function (e) {
            var a = e.d;
            $.ajax({
                url: UrlBase + "FltSearch1.asmx/GetFlightChacheDataListNoth",
                type: "POST",
                data: JSON.stringify({
                    obj: F
                }),
                async: !0,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error: function (e, t, i) {
                    return !1
                },
                success: function (e) {
                    var r = e.d,
                        s = "";
                    if (null != r)
                        for (var l = 0; l < r.length; l++) s += "," + r[l].Airline;
                    s += ",", i.ParallelCalling(F, T, t, a, r, s, x, C)
                }
            })
        }
    })
}, ResHelper.prototype.ParallelCalling = function (e, t, i, a, r, s, l, n) {
    var o = this,
        d = $("input[name='TripType']:checked").val();
    async.parallel({
        One: function (l) {
            "I" != i && "D" != i || "rdbMultiCity" == d ? o.Displayflter("G8", e, t) : -1 == $.trim(s).search("G8,") ? o.postData(o, e.HidTxtAirLine + ":LCC", e, t, UrlBase + "AirHandler.ashx", !1, !1, !0, $.trim(a), "") : o.DisplayCacheData("G8", t, r)
        },
        Two: function (i) {
            1 == t && "rdbMultiCity" != d ? o.postData(o, e.HidTxtAirLine + ":LCC", e, !1, UrlBase + "AirHandler.ashx", !1, !0, !0, $.trim(a), "") : o.Displayflter("G8", e, t)
        },
        Three: function (r) {
            if ("I" == i || "rdbMultiCity" == d || "D" == i) {
                var s = $.trim(a).split(":")[1].split("-")[0].replace("-", "");
                o.postData(o, e.HidTxtAirLine + ":" + s, e, t, UrlBase + "AirHandler.ashx", !1, !1, !1, $.trim(a), "")
            } else o.Displayflter("1G", e, t)
        },
        Four: function (l) {
            if ($.trim(a).search("CPN") >= 0 && "rdbMultiCity" != d && "D" == i)
                if (-1 == $.trim(s).search("CPN,") || "I" == i) {
                    o.postData(o, e.HidTxtAirLine + ":SG", e, t, UrlBase + "AirHandler.ashx", !0, !1, !0, $.trim(a), "")
                } else o.DisplayCacheData("CPN", t, r);
            else o.Displayflter("CPN", e, t)
        },
        Five: function (r) {
            if (1 == t && "rdbMultiCity" != d && "I" != i) {
                o.postData(o, e.HidTxtAirLine + ":1G", e, !1, UrlBase + "AirHandler.ashx", !1, !0, !1, $.trim(a), "")
            } else o.Displayflter("1G", e, t)
        },
        Six: function (l) {
            "I" != i && "D" != i || "rdbMultiCity" == d ? o.Displayflter("G8", e, t) : -1 == $.trim(s).search("G8,") ? o.postData(o, e.HidTxtAirLine + ":LCC", e, t, UrlBase + "AirHandler.ashx", !1, !1, !0, $.trim(a), "6E") : o.DisplayCacheData("G8", t, r)
        },
        Seven: function (i) {
            1 == t && "rdbMultiCity" != d ? o.postData(o, e.HidTxtAirLine + ":LCC", e, !1, UrlBase + "AirHandler.ashx", !1, !0, !0, $.trim(a), "6E") : o.Displayflter("G8", e, t)
        },
        Eight: function (l) {
            if ($.trim(a).search("CPN") >= 0 && "rdbMultiCity" != d && "D" == i)
                if (-1 == $.trim(s).search("CPN,") || "I" == i) {
                    o.postData(o, e.HidTxtAirLine + ":SG", e, t, UrlBase + "AirHandler.ashx", !0, !1, !0, $.trim(a), "6E")
                } else o.DisplayCacheData("CPN", t, r);
            else o.Displayflter("CPN", e, t)
        }
    }, function (e, t) { })
}, ResHelper.prototype.Displayflter = function (e, t, i) {
    var a = this;
    rStatus += 1, a.ProgressBar(rStatus), 8 == rStatus && a.ApplyFilters(a, e, i)
}, ResHelper.prototype.DisplayCacheData = function (e, t, i) {
    var a = this;
    rStatus += 1, a.ProgressBar(rStatus);
    var r = JSLINQ(i).Where(function (t) {
        return $.trim(t.Airline).toLowerCase() == $.trim(e).toLowerCase()
    }).Select(function (e) {
        return e
    }),
        s = $.parseJSON(r.items[0].Json);
    if (1 == t) {
        var l = 0,
            n = 0;
        if (s[0].length > 0) {
            l = s[0][s[0].length - 1].LineNumber;
            for (var o = 0; o < s[0].length; o++) s[0][o].LineNumber = s[0][o].LineNumber + "api" + e + "ITZCACHE";
            CommonResultArray[0] = $.merge(CommonResultArray[0], s[0])
        }
        if (s.length > 1 && s[1].length > 0) {
            n = s[1][s[1].length - 1].LineNumber;
            for (var d = 0; d < s[1].length; d++) s[1][d].LineNumber = s[1][d].LineNumber + "api1" + e + "ITZCACHE";
            CommonResultArray[1] = $.merge(CommonResultArray[1], s[1])
        }
        a.GetResultR(s, l, n, e + "ITZCACHE"), a.OnewayH.hide(), a.RoundTripH.show(), 8 == rStatus && a.ApplyFilters(a, e, t)
    } else {
        var c = 0;
        if (s[0][0].length > 0) {
            c = s[0][0][s[0][0].length - 1].LineNumber;
            for (o = 0; o < s[0][0].length; o++) s[0][0][o].LineNumber = s[0][0][o].LineNumber + "api" + e + "ITZCACHE";
            CommonResultArray[0] = $.merge(CommonResultArray[0], s[0][0]), $(document).ajaxStop($.unblockUI)
        }
        a.hdnOnewayOrRound.val("OneWay"), a.flterR.hide(), a.OnewayH.show(), a.divFromResult.prepend(a.GetResultO(s[0], c, e + "ITZCACHE")), a.DivResult.show(), 8 == rStatus && a.ApplyFilters(a, e, t)
    }
}, ResHelper.prototype.ApplyFilters = function (e, t, i) {
    0 == applyfilterStatus && (1 == roundtripNrml1 ? CommonResultArray[0].length > 0 && CommonResultArray[0].length > 0 ? (e.OnewayH.hide(), e.RoundTripH.show(), e.FltrSortR(CommonResultArray), e.flterR.hide(), e.flterTab.show(), e.GetSelectedRoundtripFlight(CommonResultArray), e.RTFFinalBook(), e.ShowFareBreakUp(CommonResultArray), e.hdnOnewayOrRound.val("RoundTrip")) : (alert("Sorry, we could not find a match for the destination you have entered.Kindly modify your search."), $(document).ajaxStop($.unblockUI), window.location = UrlBase + "flight-search") : CommonResultArray[0].length > 0 ? (e.hdnOnewayOrRound.val("OneWay"), e.flterR.hide(), e.Book(CommonResultArray), e.GetFltDetails(CommonResultArray), e.ShowFareBreakUp(CommonResultArray), e.FltrSort(CommonResultArray), e.GetSelectedOneWayFlight(CommonResultArray)) : (alert("Sorry, we could not find a match for the destination you have entered.Kindly modify your search."), $(document).ajaxStop($.unblockUI), window.location = UrlBase + "Search.aspx"), e.GetFltDetailsR(CommonResultArray), e.MainSF.show(), e.fltrDiv.show(), e.DivColExpnd.show(), e.DivLoadP.hide(), FlightHandler = new FlightResult, FlightHandler.BindEvents(), $(document).ajaxStop($.unblockUI), applyfilterStatus = !0)
}, ResHelper.prototype.postData = function (e, t, i, a, r, s, l, n, o, d) {
    var c = t.split(":")[1],
        p = "";
    "OF" == t ? (c = "OF", p = i.HidTxtAirLine) : p = t.split(":")[0];
    var m = "?Trip1=" + i.Trip1 + "&TripType1=" + i.TripType1 + "&DepartureCity=" + i.DepartureCity + "&ArrivalCity=" + i.ArrivalCity + "&HidTxtDepCity=" + i.HidTxtDepCity + "&HidTxtArrCity=" + i.HidTxtArrCity + "&Adult=" + i.Adult + "&Child=" + i.Child + "&Infant=" + i.Infant + "&Cabin=" + i.Cabin + "&AirLine=" + p + "&HidTxtAirLine=" + p + "&DepDate=" + i.DepDate + "&RetDate=" + i.RetDate + "&RTF=" + l + "&NStop=" + i.NStop + "&GDSRTF=" + l + "&Provider=" + c + "&isCoupon=" + s + "&DepartureCity2=" + i.DepartureCity2 + "&ArrivalCity2=" + i.ArrivalCity2 + "&HidTxtDepCity2=" + i.HidTxtDepCity2 + "&HidTxtArrCity2=" + i.HidTxtArrCity2 + "&DepDate2=" + i.DepDate2 + "&DepartureCity3=" + i.DepartureCity3 + "&ArrivalCity3=" + i.ArrivalCity3 + "&HidTxtDepCity3=" + i.HidTxtDepCity3 + "&HidTxtArrCity3=" + i.HidTxtArrCity3 + "&DepDate3=" + i.DepDate3 + "&DepartureCity4=" + i.DepartureCity4 + "&ArrivalCity4=" + i.ArrivalCity4 + "&HidTxtDepCity4=" + i.HidTxtDepCity4 + "&HidTxtArrCity4=" + i.HidTxtArrCity4 + "&DepDate4=" + i.DepDate4 + "&DepartureCity5=" + i.DepartureCity5 + "&ArrivalCity5=" + i.ArrivalCity5 + "&HidTxtDepCity5=" + i.HidTxtDepCity5 + "&HidTxtArrCity5=" + i.HidTxtArrCity5 + "&DepDate5=" + i.DepDate5 + "&DepartureCity6=" + i.DepartureCity6 + "&ArrivalCity6=" + i.ArrivalCity6 + "&HidTxtDepCity6=" + i.HidTxtDepCity6 + "&HidTxtArrCity6=" + i.HidTxtArrCity6 + "&DepDate6=" + i.DepDate6 + "&isLCC=" + n + "&ListProvider=" + o + "&callApi=" + d;
    $.ajax({
        url: r + m,
        data: {},
        async: !0,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            rStatus += 1, e.ProgressBar(rStatus);
            for (var s = r.data, l = (d = atob(s)).split("").map(function (e) {
                return e.charCodeAt(0)
            }), n = new Uint16Array(l), o = pako.inflate(n), d = "", c = 0; c < o.length; c++) d += String.fromCharCode(o[c]);
            var p = new Array;
            p = JSON.parse(d);
            try {
                for (var m = 0; m < p.length; m++) {
                    var u = JSON.parse(p[m]),
                        h = u.result;
                    if (h.length > 1 && h[1].length > 0 && h[0].length > 0 && 1 == a || h.length > 0 && h[0].length, 1 == a) {
                        if (h.length > 1) {
                            var v = 0,
                                f = 0;
                            if (h[0].length > 0) {
                                v = h[0][h[0].length - 1].LineNumber;
                                for (c = 0; c < h[0].length; c++) h[0][c].LineNumber = h[0][c].LineNumber + "api" + u.Provider + "ITZNRML";
                                CommonResultArray[0] = $.merge(CommonResultArray[0], h[0])
                            }
                            if (h.length > 1 && h[1].length > 0) {
                                f = h[1][h[1].length - 1].LineNumber;
                                for (var g = 0; g < h[1].length; g++) h[1][g].LineNumber = h[1][g].LineNumber + "api1" + u.Provider + "ITZNRML";
                                CommonResultArray[1] = $.merge(CommonResultArray[1], h[1])
                            }
                            e.GetResultR(h, v, f, u.Provider + "ITZNRML"), e.OnewayH.hide(), e.RoundTripH.show()
                        }
                        1 == i.NStop ? (8 == rStatus || 6 == rStatus && m == parseInt(p.length) - 1) && (rStatus = 8, e.ApplyFilters(e, t, a)) : 8 == rStatus && m == parseInt(p.length) - 1 && e.ApplyFilters(e, t, a)
                    } else {
                        var y = 0;
                        if (h.length > 0 && h[0].length > 0)
                            if (y = h[0][h[0].length - 1].LineNumber, $.trim(h[0][0].Sector).length > 8 && "rdbMultiCity" != i.TripType1 && "D" == $.trim(h[0][0].Trip).toUpperCase()) {
                                var R = h[0];
                                for (c = 0; c < R.length; c++) R[c].LineNumber = R[c].LineNumber + "api" + u.Provider + "ITZNRML95SFM";
                                CommanRTFArray = $.merge(CommanRTFArray, R);
                                var b = JSLINQ(R).Where(function (e) {
                                    return "1" == e.Flight
                                }).Distinct(function (e) {
                                    return e.SubKey
                                }).Select(function (e) {
                                    return e
                                }).ToArray(),
                                    F = new Array;
                                for (c = 0; c < b.length; c++)
                                    for (var D = JSLINQ(R).Where(function (e) {
                                        return "1" == e.Flight & e.SubKey == b[c]
                                    }).Distinct(function (e) {
                                        return e.LineNumber
                                    }), T = JSLINQ(R).Where(function (e) {
                                        return "1" == e.Flight & e.LineNumber == D.items[0]
                                    }), C = 0; C < T.items.length; C++) F.push(T.items[C]);
                                var x = new Array;
                                for (g = 0; g < F.length; g++) {
                                    (L = jQuery.extend(!0, [], F[g])).LineNumber = L.LineNumber + "1", L.TotalFare = Math.ceil(parseFloat(L.TotalFare) / 2), x.push(L)
                                }
                                CommonResultArray[0] = $.merge(CommonResultArray[0], x);
                                var A = JSLINQ(R).Where(function (e) {
                                    return "2" == e.Flight
                                }).Distinct(function (e) {
                                    return e.SubKey
                                }).Select(function (e) {
                                    return e
                                }).ToArray(),
                                    S = new Array;
                                for (c = 0; c < A.length; c++) {
                                    var N = JSLINQ(R).Where(function (e) {
                                        return "2" == e.Flight & e.SubKey == A[c]
                                    }).Distinct(function (e) {
                                        return e.LineNumber
                                    }),
                                        w = JSLINQ(R).Where(function (e) {
                                            return "2" == e.Flight & e.LineNumber == N.items[0]
                                        });
                                    for (C = 0; C < w.items.length; C++) S.push(w.items[C])
                                }
                                var I = new Array;
                                for (c = 0; c < S.length; c++) {
                                    var L;
                                    (L = jQuery.extend(!0, [], S[c])).LineNumber = L.LineNumber.replace("api", "api1") + "2", L.TotalFare = Math.ceil(parseFloat(L.TotalFare) / 2), I.push(L)
                                }
                                var k = new Array;
                                k.push(x), CommonResultArray[1] = $.merge(CommonResultArray[1], I), k.push(I), e.OnewayH.hide(), e.RoundTripH.show();
                                try {
                                    h[0][0].AirLineName, h[0]
                                } catch (e) { }
                            } else {
                                for (c = 0; c < h[0].length; c++) h[0][c].LineNumber = h[0][c].LineNumber + "api" + u.Provider + "ITZNRML";
                                CommonResultArray[0] = $.merge(CommonResultArray[0], h[0]), e.hdnOnewayOrRound.val("OneWay"), e.flterR.hide(), e.OnewayH.show(), e.divFromResult.prepend(e.GetResultO(h, y, u.Provider + "ITZNRML")), e.DivResult.show()
                            } 1 == i.NStop ? (8 == rStatus || 6 == rStatus && m == parseInt(p.length) - 1) && (rStatus = 8, e.ApplyFilters(e, t, a)) : 8 == rStatus && m == parseInt(p.length) - 1 && e.ApplyFilters(e, t, a)
                    }
                }
            } catch (e) {
                $.unblockUI()
            }
            8 == rStatus && 0 == parseInt(p.length) && e.ApplyFilters(e, t, a), e.MainSF.show()
        },
        error: function (i, r, s) {
            rStatus += 1, e.ProgressBar(rStatus), 8 == rStatus && e.ApplyFilters(e, t, a)
        }
    })
}, ResHelper.prototype.ProgressBar = function (e) {
    var t;
    t = 14 * e, $("#DivLoadP").progressbar({
        value: t
    })
}, ResHelper.prototype.GetResultSplRTFTrip = function (e, t) {
    var i, r = e;
    r.MainSF.hide();
    var s = $("input[name='TripType']:checked").val(),
        l = r.hidtxtDepCity1.val().split(","),
        n = r.hidtxtArrCity1.val().split(",");
    i = "IN" == l[1] && "IN" == n[1] ? "D" : "I";
    var o = new Boolean;
    $(this), o = 1 == r.chkNonstop.is(":checked");
    var d = new Boolean;
    "lcc" == t ? (d = !0, a = !1) : "gds" == t && (d = !1, a = !0);
    var c, p, m = (new QSHelper).queryStr(),
        u = {
            Trip1: i,
            TripType1: s,
            DepartureCity: r.txtDepCity1.val(),
            ArrivalCity: r.txtArrCity1.val(),
            HidTxtDepCity: r.hidtxtDepCity1.val(),
            HidTxtArrCity: r.hidtxtArrCity1.val(),
            Adult: r.Adult.val(),
            Child: r.Child.val(),
            Infant: r.Infant.val(),
            Cabin: r.Cabin.val(),
            AirLine: r.txtAirline.val(),
            HidTxtAirLine: r.hidtxtAirline.val(),
            DepDate: m.txtDepDate,
            RetDate: m.txtRetDate,
            RTF: d,
            NStop: o,
            GDSRTF: a
        };
    if ("rdbOneWay" == s) r.prexnt.show(), c = u.DepartureCity + " To " + u.ArrivalCity + " On  " + u.DepDate, r.searchquery.html(c), r.DivRefinetitle.html(r.txtDepCity1.val() + " to " + r.txtArrCity1.val());
    else if ("rdbRoundTrip" == s) {
        r.prexnt.hide();
        var h = (c = u.DepartureCity + " To " + u.ArrivalCity + " On  " + u.DepDate) + "<br/>" + (p = u.ArrivalCity + " To " + u.DepartureCity + " On  " + u.RetDate);
        r.searchquery.html(h), c = c + " For " + u.Adult + " Adult " + u.Child + " Child " + u.Infant + " Infant", p = p + " For " + u.Adult + " Adult " + u.Child + " Child " + u.Infant + " Infant", r.SearchTextDiv1.html("One Way from " + r.txtDepCity1.val() + " to " + r.txtArrCity1.val() + " | return " + r.txtArrCity1.val() + " to " + r.txtDepCity1.val()), r.RTFTextFrom.html(r.hidtxtDepCity1.val().split(",")[0] + " - " + r.hidtxtArrCity1.val().split(",")[0] + " " + r.txtDepDate.val()), r.RTFTextTo.html(r.hidtxtArrCity1.val().split(",")[0] + " - " + r.hidtxtDepCity1.val().split(",")[0] + " " + r.txtRetDate.val()), r.DivRefinetitle.html(r.txtDepCity1.val() + " to " + r.txtArrCity1.val()), r.flterTabO.hide(), r.flterTabR.hide()
    }
    $.blockUI({
        message: $("#waitMessage")
    });
    var v = UrlBase + "FLTSearch1.asmx/Search_Flight";
    $.ajax({
        url: v,
        type: "POST",
        data: JSON.stringify({
            obj: u
        }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function (e, t, i) {
            return alert("Sorry, we could not find a match for the destination you have entered..Kindly mordify your search."), !1
        },
        success: function (e) {
            var i = e.d;
            "" == i || null == i ? (alert("Sorry, we could not find a match for the destination you have entered.Kindly mordify your search."), $(document).ajaxStop($.unblockUI)) : "" == i[0] || null == i[0] ? (alert("Sorry, we could not find a match for the destination you have entered.Kindly mordify your search."), $(document).ajaxStop($.unblockUI)) : (r.hdnOnewayOrRound.val("OneWay"), r.flterR.hide(), "lcc" == t ? (lccRtfResultArray = i, lccRtfResult = r.GetResultO(i), r.DivResult.html(lccRtfResult)) : "gds" == t && (gdsRtfResultArray = i, gdsRtfResult = r.GetResultO(i), r.DivResult.html(gdsRtfResult)), r.Book(i), r.GetFltDetails(i), r.ShowFareBreakUp(i), r.FltrSort(i), r.GetFltDetailsR(i), r.MainSF.show(), FlightHandler = new FlightResult, FlightHandler.BindEvents(), $(document).ajaxStop($.unblockUI))
        }
    })
}, ResHelper.prototype.GetResultR = function (e, t, i, a) {
    var r = 40,
        s = this,
        l = "";
    if (e[0].length > 0) {
        var n = 1,
            o = JSLINQ(e[0]).Select(function (e) {
                return e.SLineNumber
            }),
            d = Math.max.apply(Math, o.items);
        ! function t() {
            $.trim(a).search("SFM") > 0 ? a + "1" : a;
            for (var i = r; n <= d; n++) {
                i--;
                var o = JSLINQ(e[0]).Where(function (e) {
                    return e.SLineNumber == n
                }).Select(function (e) {
                    return e
                }),
                    c = o.items.sort(function (e, t) {
                        return e.AdtFare * e.Adult + e.ChdFare * e.Child + e.InfFare * e.Infant - (t.AdtFare * t.Adult + t.ChdFare * t.Child + t.InfFare * t.Infant)
                    }),
                    p = JSLINQ(c).Select(function (e) {
                        return e.LineNumber
                    }),
                    m = JSLINQ(o.items).Where(function (e) {
                        return e.LineNumber == p.items[0] && "1" == e.Flight
                    }).Select(function (e) {
                        return e
                    }),
                    u = 0;
                if (m.items.length > 0) {
                    if ($.trim(a).search("SFM") > 0) {
                        var h = m.items[0].AirLineName.replace(/\s/g, "") + "_" + m.items[0].AirLineName.replace(/\s/g, "");
                        l += '<div class="list-item resR srtfResult ' + h + '" style="display:none;">'
                    } else l += '<div class="list-item resR nrmResult">';
                    l += '<div id="main_' + n + "api" + a + '_O" class="fltbox mrgbtm bdrblue">', l += '<div class="">', l += '<div class="">';
                    for (var v = 0; v < 1; v++) {
                        if (l += '<div class="col-md-3 col-xs-3">', l += '<div class="row" id="ret">', 1 == s.CheckMultipleCarrier(m.items) ? (l += '<img alt="" class="air-img-r"   src="' + UrlBase + 'Airlogo/multiple.png"/>', l += "</div>", l += "<div>Multiple Carriers</div>", l += '<div class="airlineImage2 hide">' + m.items[v].AirLineName + "</div>") : "6E" == m.items[v].MarketingCarrier && $.trim(m.items[v].sno).search("INDIGOCORP") >= 0 ? (l += '<img alt="" class="air-img-r"  src="../Airlogo/smITZ.gif"/>', l += "</div>", l += '<div class="gray">' + m.items[v].FlightIdentification + "</div>", l += '<div class="airlineImage2">RWT Fare</div>') : (l += '<img alt="" class="air-img-r"  src="../Airlogo/sm' + m.items[v].MarketingCarrier + '.gif"/>', l += "</div>", l += ' <div class="airlineInfo-sctn row" ><span class="font12 inlineB append_bottom7 insertSep" style="font-size: 12px !important;">' + m.items[v].AirLineName + '</span></div> <div class="font10 prepend_left5 row" style="font-size: 12px !important;">' + m.items[v].MarketingCarrier + "-" + m.items[v].FlightIdentification + "</div>"), l += "</div>", l += '<div class="col-md-3 col-xs-4" style="text-align: left;">', l += '<div class="f16">' + s.MakeupAdTime(m.items[v].DepartureTime) + "</div> ", l += '<div class="">' + m.items[v].Departure_Date + "</div> ", l += '<div class="ter">' + m.items[v].DepartureTerminal + "</div> ", l += '<div class="dur">' + f + "</div>", l += "</div>", l += '<div class="col-md-3 col-xs-4">', 0 == u) {
                            var f = "";
                            f = "TB" == m.items[v].Provider ? s.GetTotalDuration(m.items) : s.MakeupTotDur(m.items[v].TotDur), l += '<div class="theme-search-results-item-flight-section-path stp-dur">', l += '<div class="theme-search-results-item-flight-section-path-fly-time"><p style="font-size:10px !important;">' + f + " | " + m.items[v].Stops + "</p></div>", l += '<div class="row path">', l += '<div class="theme-search-results-item-flight-section-path-line"></div>', l += '<div class="theme-search-results-item-flight-section-path-line-start"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + m.items[v].DepartureLocation + "</div></div>", l += '<div class="theme-search-results-item-flight-section-path-line-end"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + m.items[v].ArrivalLocation + "</div></div>", l += "</div>", l += "</div>", l += '<div class="airlineImage hide">' + m.items[v].AirLineName + "</div>", l += '<div class="stops hide">' + m.items[v].Stops + "</div>", l += '<div class="totdur hide">' + s.MakeupTotDur(m.items[v].TotDur).replace(":", "") + "</div>", l += '<div class="dtime2 hide">' + s.GetCommomTimeForFilter(s.MakeupAdTime(m.items[v].DepartureTime).replace(":", "")) + "</div>", $.trim(a).search("SFM") > 0 ? l += '<div class="srf hide">SRF</div>' : l += '<div class="srf hide">NRMLF</div>', l += '<div class="clear"></div>', l += "</div>", l += '<div class="col-md-3 col-xs-4" style="text-align: end;">', l += '<div class="f16"> ' + s.MakeupAdTime(m.items[m.items.length - 1].ArrivalTime), l += '<div class="arrtime hide">' + s.MakeupAdTime(m.items[m.items.length - 1].ArrivalTime).replace(":", "") + "</div>", l += "</div>", l += '<div class="">' + m.items[v].Arrival_Date + "</div> ", l += '<div class="ter">' + m.items[v].ArrivalTerminal + "</div>", l += '<div class="stp">' + m.items[v].Stops + "</div>", l += "</div>", l += "</div>", l += '<div class="row">', l += '<div class="large-12 medium-12 small-12 columns passenger">';
                            var g = "n";
                            "" == s.DisplayPromotionalFare(m.items[v]) || (l += '<div class="lft"><img src="' + UrlBase + 'images/icons/FamilyFriends.png" alt="F" title="Family and Friends" />&nbsp;</div>'), l += '<div class="gridViewToolTip1 hide" title="' + m.items[v].LineNumber + '_O" >ss</div>'
                        }
                        if (l += "</div>", l += "</div>", 0 == u) {
                            l += '<div class="">', $.trim(a).search("SFM") > 0 && ("SG" == m.items[v].MarketingCarrier || "6E" == m.items[v].MarketingCarrier || "G8" == m.items[v].MarketingCarrier || "UK888" == m.items[v].MarketingCarrier) && "D" == m.items[v].Trip ? l += '<div class="f20 rgt colorp" style="display:none;"><i class="fa fa-inr" aria-hidden="true"></i>' + m.items[v].NetFareSRTF + "</div>" : l += '<div class="f20 rgt colorp" style="display:none;"><i class="fa fa-inr" aria-hidden="true"></i>' + m.items[v].TotalFare + "</div>";
                            for (var y = 0; y < m.items.length; y++) l += '<div class="airstopO hide  gray">' + $.trim(m.items[y].Stops).toString().toLowerCase() + "_" + $.trim(m.items[y].AirLineName).toString().toLowerCase().replace(" ", "_") + "</div>";
                            if (l += '<div class="lft">', 0 == u) {
                                var R = DisplayMultipleFares(o, p.items, 0, m.items[v].AirLineName, !0, !1, n, "O", m.items[v].LineNumber);
                                "" != R[0] && "," != R[0] && (l += R[0]), "" != R[1] && "," != R[1] && (l += R[1]), l += '<div class="deptime hide passenger">' + s.MakeupAdTime(m.items[v].DepartureTime).replace(":", "") + "</div>", l += '<div class="dtime hide">' + s.GetCommomTimeForFilter(s.MakeupAdTime(m.items[v].DepartureTime).replace(":", "")) + "</div>", l += '<div class="rfnd hide passenger">' + g + "</div> ", l += '<div class="price hide passenger">' + m.items[v].TotalFare + "</div>"
                            }
                            if (l += "</div>", l += "</div>", l += "</div>", l += '<div class="clear"></div>', l += '<div class="shadborder"></div>', l += '<div class="row darkbg" style="display:none;">', l += '<div class="col-md-4">', 0 == u) {
                                var b = '<div class="text-center">Non-Refundable </div>';
                                "refundable" == $.trim(m.items[v].AdtFareType).toLowerCase() && (b = '<div class="text-center"> Refundable   </div>', g = "r"), $.trim(a).search("SFM") > 0 && (b = '<div class="text-center">  Special Return </div>'), l += '<div class="lft passenger" style="white-space:nowrap;">' + b + "</div>"
                            }
                            l += "</div>", l += '<div class="col-md-4"> Delhi</div>', l += '<div class="col-md-4">', l += '<div id="ABC" style="top: 0px;" onclick="return toggleFD(\'FD_' + m.items[v].LineNumber + "_" + m.items[v].AirLineName.replace(" ", "-") + '_O\');"> <a href="javascript:void(0);" class="bld1">Flight Details <i class="fa fa-chevron-down"></i></a>   </div>', l += "</div>", l += '<div class="clear"></div>', l += "</div>", l += '<div class="">', l += '<div class=""><div class="summary" id="FS_' + m.items[v].LineNumber + "_" + m.items[v].AirLineName.replace(" ", "-") + '_O"> </div>', l += "</div>", l += '<div class=""><div  class="summary" id="FDt_' + m.items[v].LineNumber + "_" + m.items[v].AirLineName.replace(" ", "-") + '_O"></div>', l += "</div>", l += '<div class=""><div  class="summary" id="BIn_' + m.items[v].LineNumber + "_" + m.items[v].AirLineName.replace(" ", "-") + '_O"></div>', l += "</div>"
                        }
                        u++;
                        break
                    }
                    l += "</div>", l += " </div>", l += " </div>"
                }
                if (n < d && 0 == i || n == d) {
                    l += " </div>", l += " </div>", n == d && n++, setTimeout(t, 10);
                    break
                }
            }
        }()
    }
    s.divFromR.prepend(l);
    var c = "";
    if (e.length > 1) {
        var p = 1;
        if (e[1].length > 0) {
            $.trim(a).search("SFM") > 0 ? a + "2" : a;
            var m = 0;
            o = JSLINQ(e[1]).Select(function (e) {
                return e.SLineNumber
            }), m = Math.max.apply(Math, o.items), p = Math.min.apply(Math, o.items),
                function t() {
                    for (var i = r; p <= m; p++) {
                        i--;
                        var o = JSLINQ(e[1]).Where(function (e) {
                            return e.SLineNumber == p
                        }).Select(function (e) {
                            return e
                        }),
                            d = o.items.sort(function (e, t) {
                                return e.AdtFare * e.Adult + e.ChdFare * e.Child + e.InfFare * e.Infant - (t.AdtFare * t.Adult + t.ChdFare * t.Child + t.InfFare * t.Infant)
                            }),
                            u = JSLINQ(d).Select(function (e) {
                                return e.LineNumber
                            }),
                            h = 0;
                        if (o.items.length > 0) {
                            if ($.trim(a).search("SFM") > 0) {
                                var v = o.items[0].AirLineName.replace(/\s/g, "") + "_" + o.items[0].AirLineName.replace(/\s/g, "");
                                c += '<div class="list-itemR srtfResult ' + v + '" style="display:none;">'
                            } else c += '<div class="list-itemR nrmResult">';
                            1 == s.CheckMultipleCarrier(o.items) || "6E" == o.items[0].MarketingCarrier && $.trim(o.items[0].sno).search("INDIGOCORP"), "SG" == o.items[0].ValiDatingCarrier && ($.trim(o.items[0].Searchvalue).search("AP7") >= 0 || $.trim(o.items[0].Searchvalue).search("AP14") >= 0) && (c += '<div class="clear"></div><div class="bld colorp italic">Non Refundable</div>'), c += '<div id="main_' + n + "api1" + a + '_R" class="fltbox mrgbtm">', c += '<div class="">';
                            for (var f = 0; f < 1; f++) {
                                if (c += '<div class="col-md-3 col-xs-3">', c += '<div class="row">', 1 == s.CheckMultipleCarrier(o.items) ? (c += '<img alt="" class="air-img-r" src="' + UrlBase + 'Airlogo/multiple.png"/>', c += "</div>", c += "<div>Multiple Carriers</div>") : "6E" == o.items[f].MarketingCarrier && $.trim(o.items[f].sno).search("INDIGOCORP") >= 0 ? (c += '<img alt="" class="air-img-r" src="../Airlogo/smITZ.gif"/>', c += "</div>", c += '<div class="gray">' + o.items[f].FlightIdentification + "</div>") : (c += '<img alt="" class="air-img-r" src="../Airlogo/sm' + o.items[f].MarketingCarrier + '.gif"/>', c += "</div>", c += ' <div class="airlineInfo-sctn row" ><span class="font12 inlineB append_bottom7 insertSep" style="font-size: 12px !important;">' + o.items[f].AirLineName + ' </div> <div class="row font10 prepend_left5" style="font-size: 12px !important;">' + o.items[f].MarketingCarrier + "-" + o.items[f].FlightIdentification + "</div>"), c += "</div>", c += '<div class="col-md-3 col-xs-4" style="text-align: left;">', c += '<div class="f16">' + s.MakeupAdTime(o.items[f].DepartureTime) + "</div>", c += "<div>" + o.items[f].Departure_Date + "</div>", c += '<div class="ter">' + o.items[f].DepartureTerminal + "</div>", c += '<div class="dur">' + g + "</div>", c += "</div>", c += '<div class="col-md-3 col-xs-4 ">', 0 == h) {
                                    var g = "";
                                    g = "TB" == o.items[f].Provider ? s.GetTotalDuration(o.items) : s.MakeupTotDur(o.items[f].TotDur), c += '<div class="theme-search-results-item-flight-section-path stp-dur">', c += '<div class="theme-search-results-item-flight-section-path-fly-time"><p style="font-size:10px !important;">' + g + " | " + o.items[f].Stops + "</p></div>", c += '<div class="row path">', c += '<div class="theme-search-results-item-flight-section-path-line"></div>', c += '<div class="theme-search-results-item-flight-section-path-line-start"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[f].DepartureLocation + "</div></div>", c += '<div class="theme-search-results-item-flight-section-path-line-end"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[f].ArrivalLocation + "</div></div>", c += "</div>", c += "</div>", c += '<div class="totdur hide">' + s.MakeupTotDur(o.items[f].TotDur).replace(":", "") + "</div>", c += '<div class="atime hide">' + s.GetCommomTimeForFilter(s.MakeupAdTime(o.items[f].DepartureTime).replace(":", "")) + "</div>", c += '<div class="airlineImage hide">' + o.items[f].AirLineName + "</div>", l += "</div>", l += '<div class="clear"></div>', c += "</div>", c += '<div class="col-md-3 col-xs-4" style="text-align: end;">', c += '<div class="f16"> ' + s.MakeupAdTime(o.items[o.items.length - 1].ArrivalTime) + "</div>", c += '<div class="arrtime hide">' + s.MakeupAdTime(o.items[o.items.length - 1].ArrivalTime).replace(":", "") + "</div>", c += "<div >" + o.items[f].Arrival_Date + "</div>", c += '<div class="ter">' + o.items[f].ArrivalTerminal + "</div>", c += '<div class="stp">' + o.items[f].Stops + "</div>", c += "</div>", l += '<div class="large-12 medium-12 small-12 columns passenger">';
                                    var y = "n";
                                    0 == h && ("refundable" == $.trim(o.items[f].AdtFareType).toLowerCase() && (y = "r"), $.trim(a).search("SFM"), $.trim(a).search("SFM") > 0 ? c += '<div class="srf hide passenger">SRF</div>' : c += '<div class="srf hide passenger">NRMLF</div>')
                                }
                                if (c += "</div>", 0 == h) {
                                    c += '<div class="">', $.trim(a).search("SFM") > 0 && ("SG" == o.items[f].MarketingCarrier || "6E" == o.items[f].MarketingCarrier || "G8" == o.items[f].MarketingCarrier || "UK888" == o.items[f].MarketingCarrier) && "D" == o.items[f].Trip ? c += '<div class="price f20 rgt colorp" style="display:none;"><i class="fa fa-inr" aria-hidden="true"></i>' + o.items[f].NetFareSRTF + "</div>" : c += '<div class="price f20 rgt colorp" style="display:none;"><i class="fa fa-inr" aria-hidden="true"></i>' + o.items[f].TotalFare + "</div>", c += '<div class="clear"></div>';
                                    for (var R = 0; R < o.items.length; R++) c += '<div class="airstopR hide  gray">' + $.trim(o.items[R].Stops).toString().toLowerCase() + "_" + $.trim(o.items[R].AirLineName).toString().toLowerCase().replace(" ", "_") + "</div>";
                                    if (c += '<div class="lft">', 0 == h) {
                                        var b = DisplayMultipleFares(o, u.items, 1, o.items[f].AirLineName, !0, !1, p, "R", o.items[f].LineNumber);
                                        "" != b[0] && "," != b[0] && (c += b[0]), "" != b[1] && "," != b[1] && (c += b[1]), c += '<div class="deptime hide">' + s.MakeupAdTime(o.items[f].DepartureTime).replace(":", "") + "</div>", c += '<div class="rfnd bld hide">' + y + "</div>"
                                    }
                                    c += "</div>", c += "</div>", c += '<div class="clear"></div>', c += '<div class="shadborder"></div>', c += '<div class="row darkbg" style="display:none;">', c += '<div class="col-md-4"> Delhi</div>', c += '<div class="col-md-4"> Delhi</div>', c += '<div class="col-md-4">', c += "</div>", c += "</div>", c += '<div class="clear"></div>', c += '<div class="">', c += '<div class=""><div class="summary" id="FS_' + o.items[f].LineNumber + "_" + o.items[f].AirLineName.replace(" ", "-") + '_R"> </div>', c += "</div>", c += '<div class=""><div  class="summary" id="FDt_' + o.items[f].LineNumber + "_" + o.items[f].AirLineName.replace(" ", "-") + '_R"></div>', c += "</div>", c += '<div class=""><div  class="summary" id="BIn_' + o.items[f].LineNumber + "_" + o.items[f].AirLineName.replace(" ", "-") + '_R"></div>', c += "</div>", c += "</div>"
                                }
                                h++;
                                break
                            }
                            c += "</div>", c += "</div>"
                        }
                        if (p < m && 0 == i || p == m) {
                            c += "</div>", c += "</div>", setTimeout(t, 10);
                            break
                        }
                    }
                }()
        }
    }
    s.divToR.prepend(c)
}, ResHelper.prototype.CheckMultipleCarrier = function (e) {
    for (var t = new Array, i = 0; i < e.length; i++) t.push(e[i].MarketingCarrier);
    return t.unique().length > 1
}, ResHelper.prototype.GetResultO = function (e, t, a) {
    var r = this,
        s = "";
    if (e[0].length > 0) {
        var l = JSLINQ(e[0]).Select(function (e) {
            return e.SLineNumber
        }),
            n = Math.max.apply(Math, l.items);
        i = Math.min.apply(Math, l.items);
        for (i = 1; i <= n; i++) {
            var o, d, c = JSLINQ(e[0]).Where(function (e) {
                return e.SLineNumber == i
            }).Select(function (e) {
                return e
            }),
                p = c.items.sort(function (e, t) {
                    return e.AdtFare * e.Adult + e.ChdFare * e.Child + e.InfFare * e.Infant - (t.AdtFare * t.Adult + t.ChdFare * t.Child + t.InfFare * t.Infant)
                }),
                m = JSLINQ(p).Select(function (e) {
                    return e.LineNumber
                }),
                u = JSLINQ(c.items).Where(function (e) {
                    return e.LineNumber == m.items[0]
                }).Select(function (e) {
                    return e
                }),
                h = 0;
            if (u.items.length > 0) {
                s += '<div class="list-item resO">', o = JSLINQ(u.items).Where(function (e) {
                    return "1" == e.Flight
                }).Select(function (e) {
                    return e
                });
                var v = JSLINQ(u.items).Select(function (e) {
                    return e.Flight
                });
                if (d = Math.max.apply(Math, v.items), h = (b = JSLINQ(u.items).Where(function (e) {
                    return "2" == e.Flight
                }).Select(function (e) {
                    return e
                })).items.length, s += '<div class="theme-search-results" >', s += '<div class="theme-search-results-item theme-search-results-item-">', s += '<div class="theme-search-results-item-preview">', s += '<div id="OneWay">', o.items.length > 0) {
                    s += '<div class="col-md-2 col-xs-5" >', s += '<div  class="logoimg col-xs-2">', 1 == r.CheckMultipleCarrier(o.items) ? (s += '<img class="air-img" alt="" src="' + UrlBase + 'Airlogo/multiple.png" />', s += "</div>", s += "<div>Multiple Carriers</div>", s += '<div class="airlineImage hide">' + o.items[0].AirLineName + "</div>") : "6E" == o.items[0].MarketingCarrier && $.trim(o.items[0].sno).search("INDIGOCORP") >= 0 ? (s += '<img class="air-img" alt="" src="../Airlogo/smITZ.gif"/>', s += "</div>", s += '<div class="gray"><span>' + o.items[0].FlightIdentification + "</div>", s += '<div class="airlineImage">RWT Fare</div>') : (s += '<img class="air-img" alt="" src="../Airlogo/sm' + o.items[0].MarketingCarrier + '.gif"/>', s += "</div>", s += '<div class="col-xs-6">', s += '<div class="row" style="font-size: 15px;">' + o.items[0].AirLineName + "</div>", s += '<div class="row"><span>' + o.items[0].MarketingCarrier + "</span> - " + o.items[0].FlightIdentification + "</div>", s += "</div>"), s += "</div>", s += '<div class="col-md-2 col-xs-3" style="width: 10%;">', s += '<div class="theme-search-results-item-flight-section-meta">', s += '<p class="theme-search-results-item-flight-section-meta-time">' + r.MakeupAdTime(o.items[0].DepartureTime) + "</p>", s += '<p class="theme-search-results-item-flight-section-meta-date">' + o.items[0].Departure_Date + "</p>", s += '<p class="theme-search-results-item-flight-section-meta-city ter">' + o.items[0].DepartureTerminal + "</p>", s += '<p class="theme-search-results-item-flight-section-meta-city stp">' + o.items[0].Stops + "</p>", s += '<div class="deptime hide">' + r.MakeupAdTime(o.items[0].DepartureTime).replace(":", "") + "</div>", s += '<div class="dtime hide">' + r.GetCommomTimeForFilter(r.MakeupAdTime(o.items[0].DepartureTime).replace(":", "")) + "</div>", s += "</div>", s += "</div>", s += '<div class="col-md-2 col-xs-3 tot-dur">';
                    var f = "";
                    s += '<div class="theme-search-results-item-flight-section-path">', s += '<div class="theme-search-results-item-flight-section-path-fly-time"><p>' + (f = "TB" == o.items[0].Provider ? r.GetTotalDuration(o.items) : r.MakeupTotDur(o.items[0].TotDur)) + " | " + o.items[0].Stops + "</p></div>", s += '<div class="row">', s += '<div class="theme-search-results-item-flight-section-path-line"></div>', "0-Stop" == o.items[0].Stops && (s += '<div class="theme-search-results-item-flight-section-path-line-start"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[0].DepartureLocation + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-end"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[o.items.length - 1].ArrivalLocation + "</div></div>"), "2-Stop" == o.items[0].Stops && (s += '<div class="theme-search-results-item-flight-section-path-line-start"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[0].DepartureLocation + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-middle-1"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[0].ArrAirportCode + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-middle-2"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[1].ArrivalLocation + "</div></div>", s += ' <div class="theme-search-results-item-flight-section-path-line-end"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[o.items.length - 1].ArrivalLocation + "</div></div>"), "1-Stop" == o.items[0].Stops && (s += '<div class="theme-search-results-item-flight-section-path-line-start"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[0].DepartureLocation + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-middle-1" style="left: 50% !important;"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[0].ArrAirportCode + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-end"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + o.items[o.items.length - 1].ArrivalLocation + "</div></div>"), s += "</div>";
                    s += '<div class="airlineImage hide">' + o.items[0].AirLineName + "</div>", s += '<div class="stops hide">' + o.items[0].Stops + "</div>", s += '<div class="totdur hide">' + r.MakeupTotDur(o.items[0].TotDur).replace(":", "") + "</div>";
                    for (var g = 0; g < o.items.length; g++) s += '<div class="airstopO hide  gray">' + $.trim(o.items[g].Stops).toString().toLowerCase() + "_" + $.trim(o.items[g].AirLineName).toString().toLowerCase().replace(" ", "_") + "</div>";
                    s += "</div>", s += "</div>", s += '<div class="col-md-2 col-xs-4">', s += '<div class="theme-search-results-item-flight-section-meta">', s += '<p class="theme-search-results-item-flight-section-meta-time">' + r.MakeupAdTime(o.items[o.items.length - 1].ArrivalTime) + "</p>", s += '<p class="theme-search-results-item-flight-section-meta-date">' + o.items[o.items.length - 1].Arrival_Date + "</p>", s += '<p class="theme-search-results-item-flight-section-meta-city ter">' + o.items[o.items.length - 1].ArrivalTerminal + "</p>", s += '<div class="arrtime hide">' + r.MakeupAdTime(o.items[o.items.length - 1].ArrivalTime).replace(":", "") + "</div>", s += '<p class="theme-search-results-item-flight-section-meta-city dur">' + f + "</p>", s += "</div>", s += "</div>", s += '<div class="gridViewToolTip1 hide"  title="' + u.items[0].LineNumber + '_O" >ss</div>', s += '<div class="col-md-4">', s += '<div class="">', $.trim(a).search("SFM") > 0 ? s += '<div class="price f20 lft colorp"> <i clawaitMessagess="fa fa-inr" aria-hidden="true"></i>' + Math.ceil(o.items[0].TotalFare / 2) + "</div>" : s += '<div class="price f20 lft colorp" style="display:none;"> <i class="fa fa-inr" aria-hidden="true"></i>' + o.items[0].TotalFare + "</div>", parseInt(u.items[0].AvailableSeats1) <= 5 && u.items[0].ValiDatingCarrier, s += '<div class="clear"></div>', s += '<div class="row" >';
                    var y = DisplayMultipleFares(c, m.items, 0, u.items[0].AirLineName, !1, u.items[0].RTF, i, "O", u.items[0].LineNumber, h);
                    "" != y[0] && "," != y[0] && (s += y[0]), "" != y[1] && "," != y[1] && (s += y[1]), s += "</div>", s += "</div>", s += "</div>", s += '<div class="clear"></div>'
                }
                s += '<div class="w100 bggray" style="margin-left:-10px;width:963px;" id="DIV_' + u.items[0].LineNumber.toString() + "_" + u.items[0].AirLineName.replace(" ", "-") + '">', s += '<div class="active in" id="' + u.items[0].LineNumber + '_O_flth"><div class="summary" id="FS_' + u.items[0].LineNumber.toString() + "_" + u.items[0].AirLineName.replace(" ", "-") + '" style="background:#fff;"> ', s += "</div>", s += "</div>", s += '<div class="active in" id="' + u.items[0].LineNumber + '_O_Det"><div  class="summary" id="FDt_' + u.items[0].LineNumber.toString() + "_" + u.items[0].AirLineName.replace(" ", "-") + '" style="background:#fff;"></div>', s += "</div>", s += '<div class="active in" id="' + u.items[0].LineNumber + '_O_BagD"><div  class="summary" id="BIn_' + u.items[0].LineNumber.toString() + "_" + u.items[0].AirLineName.replace(" ", "-") + '_O" style="background:#fff;"></div>', s += "</div>", s += "</div>", s += "</div>", s += "</div>";
                for (var R = 2; R <= d; R++) {
                    var b;
                    if (s += "<hr>", s += '<i><img alt="" src="/Custom_Design/img/round_trip-512.png" style="width: 30px;position: absolute;left: 50%;right: 50%;top: 101px;"/></i>', s += '<div class="theme-search-results-item-preview" style="/* padding: 0px 0px; */">', s += '<div id="Return" style="">', (b = JSLINQ(u.items).Where(function (e) {
                        return e.Flight == R.toString()
                    }).Select(function (e) {
                        return e
                    })).items.length > 0) {
                        s += '<hr class="w80 mauto" style="border:none; border-top:1px solid #eee; float:left; " /> ', s += '<div class="clear1"> </div> ', s += '<div class="col-md-2 col-xs-5" >', s += '<div  class="logoimg col-xs-2">', 1 == r.CheckMultipleCarrier(b.items) ? (s += '<img class="air-img" alt="" src="' + UrlBase + 'Airlogo/multiple.png" />', s += "</div>", s += "<div>Multiple Carriers</div>", s += '<div class="airlineImage hide">' + b.items[0].AirLineName + "</div>") : "6E" == b.items[0].MarketingCarrier && $.trim(b.items[0].sno).search("INDIGOCORP") >= 0 ? (s += '<img class="air-img" alt="" src="../AChangedFarePopupShowirlogo/smITZ.gif"/>', s += "</div>", s += '<div class="gray"><span>' + b.items[0].FlightIdentification + "</div>", s += '<div class="airlineImage">RWT Fare</div>') : (s += '<img class="air-img" alt="" src="../Airlogo/sm' + b.items[0].MarketingCarrier + '.gif"/>', s += "</div>", s += '<div class="col-xs-6">', s += '<div class="row"  style="font-size: 15px;">' + b.items[0].AirLineName + "</div>", s += '<div class="row"><span>' + b.items[0].MarketingCarrier + "</span> - " + b.items[0].FlightIdentification + "</div>", s += "</div>"), s += "</div>", s += '<div class="col-md-2 col-xs-3">', s += '<div class="theme-search-results-item-flight-section-meta">', s += '<p class="theme-search-results-item-flight-section-meta-time">' + r.MakeupAdTime(b.items[0].DepartureTime) + "</p>", s += '<p class="theme-search-results-item-flight-section-meta-date">' + b.items[0].Departure_Date + "</p>", s += '<p class="theme-search-results-item-flight-section-meta-city">' + b.items[0].DepartureTerminal + "</p>", s += '<div class="deptime hide">' + r.MakeupAdTime(b.items[0].DepartureTime).replace(":", "") + "</div>", s += '<div class="dtime hide">' + r.GetCommomTimeForFilter(r.MakeupAdTime(b.items[0].DepartureTime).replace(":", "")) + "</div>", s += "</div>", s += "</div>", s += '<div class="col-md-2">';
                        f = "";
                        s += '<div class="theme-search-results-item-flight-section-path">', s += '<div class="theme-search-results-item-flight-section-path-fly-time"><p>' + (f = "TB" == b.items[0].Provider ? r.GetTotalDuration(b.items) : r.MakeupTotDur(b.items[0].TotDur)) + " | " + b.items[0].Stops + "</p></div>", s += '<div class="row">', s += '<div class="theme-search-results-item-flight-section-path-line"></div>', "0-Stop" == b.items[0].Stops && (s += '<div class="theme-search-results-item-flight-section-path-line-start"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + b.items[0].DepartureLocation + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-end"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + b.items[b.items.length - 1].ArrivalLocation + "</div></div>"), "2-Stop" == b.items[0].Stops && (s += '<div class="theme-search-results-item-flight-section-path-line-start"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + b.items[0].DepartureLocation + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-middle-1"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + b.items[0].ArrAirportCode + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-middle-2"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + b.items[1].ArrivalLocation + "</div></div>", s += ' <div class="theme-search-results-item-flight-section-path-line-end"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + b.items[b.items.length - 1].ArrivalLocation + "</div></div>"), "1-Stop" == b.items[0].Stops && (s += '<div class="theme-search-results-item-flight-section-path-line-start"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + b.items[0].DepartureLocation + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-middle-1" style="left: 50% !important;"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + b.items[0].ArrAirportCode + "</div></div>", s += '<div class="theme-search-results-item-flight-section-path-line-end"><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">' + b.items[b.items.length - 1].ArrivalLocation + "</div></div>"), s += "</div>";
                        s += '<div class="stops hide">' + b.items[0].Stops + "</div>", s += '<div class="totdur hide">' + r.MakeupTotDur(b.items[0].TotDur).replace(":", "") + "</div>";
                        for (; g < b.items.length; g++) s += '<div class="airstopO hide  gray">' + $.trim(b.items[0].Stops).toString().toLowerCase() + "_" + $.trim(b.items[0].AirLineName).toString().toLowerCase().replace(" ", "_") + "</div>";
                        s += "</div>", s += "</div>", s += '<div class="col-md-2 col-xs-3">', s += '<div class="theme-search-results-item-flight-section-meta" style="text-align:right;">', s += '<p class="theme-search-results-item-flight-section-meta-time">' + r.MakeupAdTime(b.items[b.items.length - 1].ArrivalTime) + "</p>", s += '<p class="theme-search-results-item-flight-section-meta-date">' + b.items[b.items.length - 1].Arrival_Date + "</p>", s += '<p class="theme-search-results-item-flight-section-meta-city">' + b.items[b.items.length - 1].ArrivalTerminal + "</p>", s += '<div class="arrtime hide">' + r.MakeupAdTime(b.items[b.items.length - 1].ArrivalTime).replace(":", "") + "</div>", s += "</div>", s += "</div>", s += '<div class="clear1"></div>'
                    }
                    s += "</div>", s += "</div>"
                }
                s += '<div class="clear"></div>', s += "</div>", "I" == u.items[0].Trip && (s += '<div id="' + u.items[0].LineNumber.toString() + '__Fare" class="hide"></div>', s += '<div id="' + u.items[0].LineNumber.toString() + '__radio" class="hide"></div>'), s += '<div id="' + u.items[0].LineNumber.toString() + "_" + u.items[0].AirLineName.replace(" ", "-") + '" class="hide"></div>', s += "</div>", s += "</div>", s += "</div>", s += "</div>"
            }
        }
    }
    return s
}, ResHelper.prototype.GetSelectedRoundtripFlight = function (e) {
    var t = this;
    $("input:radio").click(function () {
        if ($("#msg1").html(""), t.RtfTotalPayDiv.html(""), $("#showfare").hide(), SRFReprice = !1, "O" == this.name || "RO" == this.name) {
            var i = this.value,
                a = JSLINQ(e[0]).Where(function (e) {
                    return e.LineNumber == i
                }).Select(function (e) {
                    return e
                });
            Obook = a.items, ORTFFare = a.items[0].TotalFare, ORTFLineNo = a.items[0].LineNumber, ORTFVC = a.items[0].ValiDatingCarrier, Obook[0].LineNumber.search("SFM") > 0 ? t.RtfFltSelectDivO.html(t.DisplaySelectedFlight(Obook, "SRF")) : t.RtfFltSelectDivO.html(t.DisplaySelectedFlight(Obook, "")), $(".list-item").each(function () {
                $(this).find(".rw").css("background", "#ECEFF1"), $(this).find(".mrgbtm").removeClass("fltbox01").addClass("fltbox"), $(this).find(".mrgbtmG").removeClass("fltbox02")
            }), $(this).attr("checked", "checked"), $(this).closest("div.r").closest("div.rw").css("background", "#e40e2b"), t.RtfFltSelectDiv.css("display", "block")
        } else if ("R" == this.name || "RR" == this.name) {
            i = this.value;
            var r = JSLINQ(e[1]).Where(function (e) {
                return e.LineNumber == i
            }).Select(function (e) {
                return e
            });
            Rbook = r.items, RRTFFare = r.items[0].TotalFare, RRTFLineNo = r.items[0].LineNumber, RRTFVC = r.items[0].ValiDatingCarrier, $("#hdnSRFPriceLineNoR").val(RRTFLineNo + RRTFVC), $("#hdnSRFAircodeR").val(RRTFVC), Rbook[0].LineNumber.search("SFM") > 0 ? t.RtfFltSelectDivR.html(t.DisplaySelectedFlight(Rbook, "SRF")) : t.RtfFltSelectDivR.html(t.DisplaySelectedFlight(Rbook, "")), $(".list-itemR").each(function () {
                $(this).find(".rw").css("background", "#ECEFF1"), $(this).find(".mrgbtm").removeClass("fltbox01").addClass("fltbox"), $(this).find(".mrgbtmG").removeClass("fltbox02")
            });
            i.split("api1")[0];
            $(this).attr("checked", "checked"), $(this).closest("div.r").closest("div.rw").css("background", "#e40e2b"), t.RtfFltSelectDiv.css("display", "block")
        }
        try {
            if (null != Obook && null != Rbook) {
                totprevFare = ORTFFare + RRTFFare;
                var s = $.trim(ORTFLineNo),
                    l = $.trim(RRTFLineNo),
                    n = !1,
                    o = $.trim(ORTFVC),
                    d = $.trim(RRTFVC);
                if (s.search("SFM") < 0 && l.search("SFM") < 0) {
                    a = JSLINQ(e[0]).Where(function (e) {
                        return e.LineNumber == $.trim(ORTFLineNo)
                    }).Select(function (e) {
                        return e
                    });
                    Obook = a.items;
                    r = JSLINQ(e[1]).Where(function (e) {
                        return e.LineNumber == $.trim(RRTFLineNo)
                    }).Select(function (e) {
                        return e
                    });
                    Rbook = r.items;
                    var c = Obook[0].SubKey + Rbook[0].SubKey;
                    if ((R = JSLINQ(CommanRTFArray).Where(function (e) {
                        return e.MainKey == c
                    }).Select(function (e) {
                        return e
                    })).items.length > 0) {
                        var p = JSLINQ(R.items).Where(function (e) {
                            return e.MainKey == c && "1" == e.Flight
                        }).Select(function (e) {
                            return e
                        }),
                            m = JSLINQ(R.items).Where(function (e) {
                                return e.MainKey == c && "2" == e.Flight
                            }).Select(function (e) {
                                return e
                            });
                        ObookSFM = new Array, RbookSFM = new Array;
                        for (var u = 0; u < p.items.length; u++) {
                            (C = jQuery.extend(!0, [], p.items[u])).TotalFare = C.TotalFare / 2, ObookSFM.push(C)
                        }
                        for (u = 0; u < m.items.length; u++) {
                            (C = jQuery.extend(!0, [], m.items[u])).TotalFare = C.TotalFare / 2, RbookSFM.push(C)
                        }
                        n = !0
                    }
                    Obook[0].AirLineName == Rbook[0].AirLineName && null != ObookSFM && null != RbookSFM && 0 != ObookSFM.length && 0 != RbookSFM.length && "NRM" == Obook[0].AdtFar && "NRM" == Rbook[0].AdtFar && 1 == n ? (SRFResultReprice = new Array, "6E" == ObookSFM[0].ValiDatingCarrier.toUpperCase() && "6E" == RbookSFM[0].ValiDatingCarrier.toUpperCase() ? (BlockUI(), SRFResultAfterReprice = SRFPriceItinReq(R), $.unblockUI(), SRFResultReprice = SRFResultAfterReprice, null != SRFResultAfterReprice && SRFResultAfterReprice.length > 0 && SRFResultAfterReprice[0].d.length > 0 && SRFResultAfterReprice[0].d[0].TotalFare > 0 ? (SRFReprice = !0, t.RtfTotalPayDiv.html('<label class="radio inline"><input type="radio" id="radio1" name="RadioGroup1" value="1" checked="true"/><span style="color:#fff">Selected Fare: ₹ ' + parseInt(Obook[0].TotalFare + Rbook[0].TotalFare) + '/-</span>  <span style="color:#fff;"><input type="radio" id="radio2" name="RadioGroup1" value="2" />SRF: ₹ ' + SRFResultAfterReprice[0].d[0].TotalFare + "/-</span>"), totcurrentFareSFM = parseInt(SRFResultAfterReprice[0].d[0].TotalFare)) : (t.RtfTotalPayDiv.html('<div><input type="radio" id="radio1" name="RadioGroup1" value="1" checked="true"/>Selected Fare: ₹ ' + parseInt(Obook[0].TotalFare + Rbook[0].TotalFare) + "/-</div>"), totcurrentFare = parseInt(Obook[0].TotalFare + Rbook[0].TotalFare))) : (t.RtfTotalPayDiv.html('<div><input type="radio" id="radio1" name="RadioGroup1" value="1" checked="true"/>Selected Fare: ₹ ' + parseInt(Obook[0].TotalFare + Rbook[0].TotalFare) + '/-</div>  <div><input type="radio" id="radio2" name="RadioGroup1" value="2" />SRF: ₹ ' + Math.ceil(ObookSFM[0].TotalFare + RbookSFM[0].TotalFare) + "/-</div>"), totcurrentFareSFM = parseInt(ObookSFM[0].TotalFare + RbookSFM[0].TotalFare))) : (t.RtfTotalPayDiv.html('<span style="color:#fff;">Selected Fare- ₹ ' + parseInt(Obook[0].TotalFare + Rbook[0].TotalFare) + "/-</span>"), totcurrentFare = parseInt(Obook[0].TotalFare + Rbook[0].TotalFare)), t.RtfTotalPayDiv.css("display", "block"), t.RtfBookBtn.css("display", "block"), $("#showfare").show()
                } else if (s.search("SFM") > 0 && l.search("SFM") > 0)
                    if (o.toLowerCase() != d.toLowerCase()) $("#msg1").html("Spacial return fare can be availed on same airline only."), t.RtfBookBtn.css("display", "none"), totcurrentFare = 0;
                    else {
                        c = Obook[0].SubKey + Rbook[0].SubKey;
                        if ((R = JSLINQ(CommanRTFArray).Where(function (e) {
                            return e.MainKey == c
                        }).Select(function (e) {
                            return e
                        })).items.length > 0) {
                            p = JSLINQ(R.items).Where(function (e) {
                                return e.MainKey == c && "1" == e.Flight
                            }).Select(function (e) {
                                return e
                            }), m = JSLINQ(R.items).Where(function (e) {
                                return e.MainKey == c && "2" == e.Flight
                            }).Select(function (e) {
                                return e
                            });
                            Obook = new Array, Rbook = new Array;
                            for (u = 0; u < p.items.length; u++) {
                                (C = jQuery.extend(!0, [], p.items[u])).TotalFare = Math.ceil(C.TotalFare / 2), Obook.push(C)
                            }
                            for (u = 0; u < m.items.length; u++) {
                                (C = jQuery.extend(!0, [], m.items[u])).TotalFare = Math.ceil(C.TotalFare / 2), Rbook.push(C)
                            }
                            if (t.RtfTotalPayDiv.html('<span style="color:#fff;">Current Fare- <i class="fa fa-inr" aria-hidden="true"></i> ' + parseInt(Obook[0].TotalFare + Rbook[0].TotalFare) + '/-</span><div class="clear1"></div>'), t.RtfTotalPayDiv.css("display", "block"), t.RtfBookBtn.css("display", "block"), isSRF = !0, totcurrentFare = parseInt(Obook[0].TotalFare + Rbook[0].TotalFare), "6E" != $("#hdnSRFAircodeO").val() && "SG" != $("#hdnSRFAircodeO").val() && "G8" != $("#hdnSRFAircodeO").val() && "6E" != $("#hdnSRFAircodeR").val() && "SG" != $("#hdnSRFAircodeR").val() && "G8" != $("#hdnSRFAircodeR").val()) {
                                var h = parseInt(totcurrentFare / 2),
                                    v = $("#hdnSRFPriceLineNoO").val(),
                                    f = $("#hdnSRFPriceLineNoR").val(),
                                    g = $("#Left_" + v).html(),
                                    y = $("#Right_" + f).html();
                                g == h ? $("#OLDSRFO_" + v).html("") : $("#OLDSRFO_" + v).html("&nbsp;" + g.strike()), y == h ? $("#OLDSRFR_" + f).html("") : $("#OLDSRFR_" + f).html("&nbsp;" + y.strike());
                                $("#Left_" + v).html(h), $("#Right_" + f).html(h)
                            }
                            $("#showfare").show()
                        } else $("#msg1").html("Fare changed Please Try again."), t.RtfBookBtn.css("display", "none"), totcurrentFare = 0
                    }
                else if (s.search("SFM") > 0 && l.search("SFM") < 0) {
                    c = Obook[0].SubKey + Rbook[0].SubKey;
                    var R = JSLINQ(CommanRTFArray).Where(function (e) {
                        return e.MainKey == c && e.ValiDatingCarrier == o
                    }).Select(function (e) {
                        return e
                    }),
                        b = 0,
                        F = Rbook[0].TotalFare;
                    R.items.length > 0 && (b = R.items[0].TotalFare);
                    var D = JSLINQ(e[0]).Where(function (e) {
                        return e.Subkey == Obook[0].SubKey && e.ValiDatingCarrier == o
                    }).Select(function (e) {
                        return e
                    });
                    if (D.items.length > 0 && (F += D.items[0].TotalFare), b >= 0 && F > 0)
                        if (F > b && b > 0 || F < b && D.items.length <= 0) {
                            p = JSLINQ(R.items).Where(function (e) {
                                return e.LineNumber == R.items[0].LineNumber && "1" == e.Flight
                            }).Select(function (e) {
                                return e
                            }), m = JSLINQ(R.items).Where(function (e) {
                                return e.LineNumber == R.items[0].LineNumber && "2" == e.Flight
                            }).Select(function (e) {
                                return e
                            });
                            Obook = new Array, Rbook = new Array;
                            for (u = 0; u < p.items.length; u++) {
                                (C = jQuery.extend(!0, [], p.items[u])).TotalFare = Math.ceil(C.TotalFare / 2), Obook.push(C)
                            }
                            for (u = 0; u < m.items.length; u++) {
                                (C = jQuery.extend(!0, [], m.items[u])).TotalFare = Math.ceil(C.TotalFare / 2), Rbook.push(C)
                            }
                            t.RtfTotalPayDiv.html('<span style="color:#fff;">Current Fare- <i class="fa fa-inr" aria-hidden="true"></i> ' + parseInt(Obook[0].TotalFare + Rbook[0].TotalFare) + '/-</span><div class="clear1"></div>'), t.RtfTotalPayDiv.css("display", "block"), t.RtfBookBtn.css("display", "block"), $("#showfare").show(), isSRF = !0, totcurrentFare = parseInt(Obook[0].TotalFare + Rbook[0].TotalFare)
                        } else {
                            if (Obook = new Array, D.items.length > 0)
                                for (u = 0; u < D.items.length; u++) {
                                    (C = jQuery.extend(!0, [], D.items[u])).TotalFare = Math.ceil(C.TotalFare / 2), Obook.push(C)
                                } else $("#msg1").html("Fare changed Please Try again."), t.RtfBookBtn.css("display", "none"), totcurrentFare = 0;
                            Obook.length > 0 && Rbook.length > 0 && (t.RtfTotalPayDiv.html('<span style="color:#fff;">Current Fare- <i class="fa fa-inr" aria-hidden="true"></i> ' + parseInt(Obook[0].TotalFare + Rbook[0].TotalFare) + '/-</span><div class="clear1"></div>'), t.RtfTotalPayDiv.css("display", "block"), t.RtfBookBtn.css("display", "block"), $("#showfare").show(), totcurrentFare = Obook[0].TotalFare + Rbook[0].TotalFare)
                        }
                } else if (s.search("SFM") < 0 && l.search("SFM") > 0) {
                    c = Obook[0].SubKey + Rbook[0].SubKey, R = JSLINQ(CommanRTFArray).Where(function (e) {
                        return e.MainKey == c && e.ValiDatingCarrier == o
                    }).Select(function (e) {
                        return e
                    }), b = 0, F = Obook[0].TotalFare;
                    R.items.length > 0 && (b = R.items[0].TotalFare);
                    var T = JSLINQ(e[1]).Where(function (e) {
                        return e.Subkey == Rbook[0].SubKey && e.ValiDatingCarrier == d
                    }).Select(function (e) {
                        return e
                    });
                    if (T.items.length > 0 && (F += T.items[0].TotalFare), b >= 0 && F > 0)
                        if (F > b && b > 0 || F < b && T.items.length <= 0) {
                            p = JSLINQ(R.items).Where(function (e) {
                                return e.LineNumber == R.items[0].LineNumber && "1" == e.Flight
                            }).Select(function (e) {
                                return e
                            }), m = JSLINQ(R.items).Where(function (e) {
                                return e.LineNumber == R.items[0].LineNumber && "2" == e.Flight
                            }).Select(function (e) {
                                return e
                            });
                            Obook = new Array, Rbook = new Array;
                            for (u = 0; u < p.items.length; u++) {
                                (C = jQuery.extend(!0, [], p.items[u])).TotalFare = Math.ceil(C.TotalFare / 2), Obook.push(C)
                            }
                            for (u = 0; u < m.items.length; u++) {
                                (C = jQuery.extend(!0, [], m.items[u])).TotalFare = Math.ceil(C.TotalFare / 2), Rbook.push(C)
                            }
                            t.RtfTotalPayDiv.html('<span style="color:#fff;">Current Fare- <i class="fa fa-inr" aria-hidden="true"></i> ' + parseInt(Obook[0].TotalFare + Rbook[0].TotalFare) + '/-</span><div class="clear1"></div>'), t.RtfTotalPayDiv.css("display", "block"), t.RtfBookBtn.css("display", "block"), isSRF = !0, totcurrentFare = parseInt(Obook[0].TotalFare + Rbook[0].TotalFare), $("#showfare").show()
                        } else {
                            if (Rbook = new Array, T.items.length > 0)
                                for (u = 0; u < T.items.length; u++) {
                                    var C;
                                    (C = jQuery.extend(!0, [], T.items[u])).TotalFare = Math.ceil(C.TotalFare / 2), Rbook.push(C)
                                } else $("#msg1").html("Fare changed Please Try again."), t.RtfBookBtn.css("display", "none"), totcurrentFare = 0;
                            Obook.length > 0 && Rbook.length > 0 && (t.RtfTotalPayDiv.html('<span style="color:#fff;">Current Fare- <i class="fa fa-inr" aria-hidden="true"></i> ' + parseInt(Obook[0].TotalFare + Rbook[0].TotalFare) + '/-</span><div class="clear1"></div>'), t.RtfTotalPayDiv.css("display", "block"), t.RtfBookBtn.css("display", "block"), totcurrentFare = Obook[0].TotalFare + Rbook[0].TotalFare, $("#showfare").show())
                        }
                }
                if ($("#prevfare").html(""), $("#FareDiff").html(""), totprevFare != totcurrentFare && totcurrentFare > 0 && totprevFare > 0) {
                    parseFloat(totcurrentFare) > parseFloat(totprevFare) ? '(+) <img src="../Images/drs.png"  /> ' + (parseFloat(totcurrentFare) - parseFloat(totprevFare)) + "/-</div> " : '(-) <img src="../Images/drs.png" /> ' + (parseFloat(totprevFare) - parseFloat(totcurrentFare)) + "/-</div> ", '<img src="../Images/prs.png" /> ' + totprevFare + "/-</div> "
                }
            }
        } catch (e) {
            $("#msg1").html("Fare changed Please Try again."), t.RtfBookBtn.css("display", "none"), totcurrentFare = 0
        }
    })
}, ResHelper.prototype.GetSelectedOneWayFlight = function (e) {
    var t = this;
    $("input:radio").click(function () {
        var i = this.value,
            a = JSLINQ(e[0]).Where(function (e) {
                return e.LineNumber == i
            }).Select(function (e) {
                return e
            });
        Obook = a.items, ORTFFare = a.items[0].TotalFare, ORTFLineNo = a.items[0].LineNumber, ORTFVC = a.items[0].ValiDatingCarrier, $("#hdvhgvhgvfh").html(t.DisplaySelectedFlightNew(Obook, "")), $(".one-way-select").removeClass("hide")
    })
}, ResHelper.prototype.DisplaySelectedFlightNew = function (e, t) {
    var i;
    if (e.length > 1) {
        for (var a = new Array, r = 0; r < e.length; r++) a.push(e[r].MarketingCarrier);
        var s = a.unique(),
            l = JSLINQ(e).Where(function (e) {
                return "1" == e.Flight
            }).Select(function (e) {
                return e
            }),
            n = JSLINQ(e).Where(function (e) {
                return "2" == e.Flight
            }).Select(function (e) {
                return e
            });
        if (0 != n.items.length) {
            i = "<div class='col-md-4'>", l.items.length > 1 && '<div large-2 medium-2 small-3 columns> <img src="../AirLogo/multiple.png"  /></div><div>Multiple Carrier</div>', i += '<div class="col-md-3 col-xs-3"><img src="../AirLogo/sm' + l.items[0].MarketingCarrier + '.gif" style="width:40px;"/><br>' + l.items[0].MarketingCarrier + "-" + l.items[0].FlightIdentification + "<br>" + l.items[0].AirLineName + "</div>", i += '<div class="col-md-3 col-xs-3">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + l.items[0].DepartureLocation + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date" style="font-size: 15px;">' + l.items[0].Departure_Date + "  " + l.items[0].DepartureTime + "</p>", i += "</div>", i += "</div>", i += '<div class="col-md-3 col-xs-2">', i += '<div class="fly">', i += '<div class="fly1">', i += '<span class="jtm">' + l.items[0].Departure_Date + "</span>", i += '</div><div class="fly2">', i += "</div>", i += "</div>", i += "</div>", i += '<div class="col-md-3 col-xs-3" style="text-align: end;">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + l.items[l.items.length - 1].ArrivalLocation + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date" style="font-size: 15px;">' + l.items[l.items.length - 1].Arrival_Date + "  " + l.items[l.items.length - 1].ArrivalTime + "</p>", i += "</div>", i += "</div>", i += "</div>", i += "</div>", i += "<div class='col-md-4'>";
            n.length > 1 && '<div large-2 medium-2 small-3 columns> <img src="../AirLogo/multiple.png"  /></div><div>Multiple Carrier</div>', i += '<div class="col-md-3 col-xs-3"><img src="../AirLogo/sm' + n.items[0].MarketingCarrier + '.gif" style="width:40px;"/>' + n.items[0].MarketingCarrier + "-" + n.items[0].FlightIdentification + "<br>" + FAirType + n.items[0].AirLineName + "</div>", i += '<div class="col-md-3 col-xs-3">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + n.items[0].DepartureLocation + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date" style="font-size: 15px;">' + n.items[0].Departure_Date + "  " + n.items[0].DepartureTime + "</p>", i += "</div>", i += "</div>", i += '<div class="col-md-3 col-xs-2">', i += '<div class="fly">', i += '<div class="fly1">', i += '<span class="jtm">' + n.items[0].Departure_Date + "</span>", i += '</div><div class="fly2">', i += "</div>", i += "</div>", i += "</div>", i += '<div class="col-md-3 col-xs-3" style="text-align: end;">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + n.items[n.items.length - 1].ArrivalLocation + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date" style="font-size: 15px;">' + n.items[n.items.length - 1].Arrival_Date + "  " + n.items[n.items.length - 1].ArrivalTime + "</p>", i += "</div>", i += "</div>", i += "</div>", i += "</div>", i += '<div class="col-md-3" style="border-right: 1px solid #000;border-left: 1px solid #000;"><label style="font-size: 20px; 400 !important; color: #df441b;">₹ ' + e[e.length - 1].TotalFare + '</label><span style="font-size: 20px; font-weight: 600 !important;color: #313131;"> ' + e[e.length - 1].DisplayFareType + "</span><br><p> Net Fare ₹ " + e[e.length - 1].NetFare + " | Incentive ₹ " + e[e.length - 1].TotDis + "</p></div>", i += '<div class="col-md-1"> <input type="button" class="buttonfltbk1 btn btn-danger" id = "' + e[e.length - 1].LineNumber + '" title = "' + e[e.length - 1].LineNumber + '" value="Book Now" id="FinalBook1" style="Float: left;" /> </div>'
        } else s.length > 1 && '<div large-2 medium-2 small-3 columns> <img src="../AirLogo/multiple.png"  /></div><div>Multiple Carrier</div>', i = '<div class="col-md-1 col-xs-3"><img src="../AirLogo/sm' + e[0].MarketingCarrier + '.gif" style="width:40px;"/><br>' + e[0].MarketingCarrier + "-" + e[0].FlightIdentification + "<br>" + e[0].AirLineName + "</div>", i += '<div class="col-md-2 col-xs-3">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + e[0].DepartureCityName + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date" style="font-size: 15px;">' + e[0].Departure_Date + "  " + e[0].DepartureTime + "</p>", i += "</div>", i += "</div>", i += '<div class="col-md-2 col-xs-2">', i += '<div class="fly">', i += '<div class="fly1">', i += '<span class="jtm">' + e[0].Departure_Date + "</span>", i += '</div><div class="fly2">', i += "</div>", i += "</div>", i += "</div>", i += '<div class="col-md-2 col-xs-3" style="text-align: end;">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + e[e.length - 1].ArrivalCityName + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date" style="font-size: 15px;">' + e[e.length - 1].Arrival_Date + "  " + e[e.length - 1].ArrivalTime + "</p>", i += "</div>", i += "</div>", i += "</div>", i += '<div class="col-md-3" style="border-right: 1px solid #000;border-left: 1px solid #000;"><label style="font-size: 20px; 400 !important; color: #e20000;">₹ ' + e[e.length - 1].TotalFare + '</label><span style="font-size: 20px; font-weight: 600 !important;color: #313131;"> ' + e[e.length - 1].DisplayFareType + "</span><br><p> Net Fare ₹ " + e[e.length - 1].NetFare + " | Incentive ₹ " + e[e.length - 1].TotDis + "</p></div>", i += '<div class="col-md-2"> <input type="button" class="buttonfltbk1 btn btn-danger" id = "' + e[e.length - 1].LineNumber + '" title = "' + e[e.length - 1].LineNumber + '" value="Book Now" id="FinalBook1" style="Float: left;" /> </div>'
    } else i = "6E" == e[0].MarketingCarrier && $.trim(e[0].sno).search("INDIGOCORP") >= 0 ? '<div class="large-2 medium-2 small-3 columns"><div> <img src="../AirLogo/smITZ.gif"  /></div> <div>' + e[0].FlightIdentification + '</br><span class="restext">' + e[0].DisplayFareType + "</span></div></div>" : '<div class="col-md-2 col-xs-3"><div class="logoimg col-xs-5"><img src="../AirLogo/sm' + e[0].MarketingCarrier + '.gif"  style="width:40px;"/></div><div class="col-xs-6"><div class="row">' + e[0].MarketingCarrier + "-" + e[0].FlightIdentification + '</div><div class="row">' + e[0].AirLineName + "</div></div></div>", i += '<div class="col-md-2 col-xs-3">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + e[0].DepartureCityName + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date" style="font-size: 15px;">' + e[0].Departure_Date + " " + e[0].DepartureTime + "</p>", i += "</div>", i += "</div>", i += '<div class="col-md-2 col-xs-2">', i += '<div class="fly">', i += '<div class="fly1">', i += '<span class="jtm">' + e[0].Departure_Date + "</span>", i += '</div><div class="fly2">', i += "</div>", i += "</div>", i += "</div>", i += '<div class="col-md-2 col-xs-3" style="text-align: end;">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + e[0].ArrivalCityName + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date" style="font-size: 15px;">' + e[e.length - 1].Arrmival_Date + " " + e[0].ArrivalTime + "</p>", i += "</div>", i += "</div>", i += '<div class="col-md-3" style="border-right: 1px solid #000;border-left: 1px solid #000;"><label style="font-size: 20px; 400 !important; color: #e20000;">₹ ' + e[0].TotalFare + '</label><span style="font-size: 20px; font-weight: 600 !important;color: #313131;"> ' + e[0].DisplayFareType + "</span><br><p> Net Fare ₹ " + e[0].NetFare + " | Incentive ₹ " + e[0].TotDis + "</p></div>", i += '<div class="col-md-1"> <input type="button" class="buttonfltbk1 btn btn-danger" id = "' + e[0].LineNumber + '" title = "' + e[0].LineNumber + '" value="Book Now" id="FinalBook1" style="Float: left;" /> </div>';
    return i
}, ResHelper.prototype.DisplaySelectedFlight = function (e, t) {
    var i;
    if (e.length > 1) {
        for (var a = new Array, r = 0; r < e.length; r++) a.push(e[r].MarketingCarrier);
        a.unique().length > 1 && '<div large-2 medium-2 small-3 columns> <img src="../AirLogo/multiple.png"  /></div><div>Multiple Carrier</div>', i = '<div class="col-md-3 col-xs-3"><img src="../AirLogo/sm' + e[0].MarketingCarrier + '.gif" /><br>' + e[0].MarketingCarrier + "-" + e[0].FlightIdentification + "<br></div>", i += '<div class="col-md-3 col-xs-3">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + e[0].DepartureCityName + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date">' + e[0].DepartureTime + "</p>", i += "</div>", i += "</div>", i += '<div class="col-md-3 col-xs-2">', i += '<div class="fly">', i += '<div class="fly1">', i += '<span class="jtm">' + e[0].Departure_Date + "</span>", i += '</div><div class="fly2">', i += "</div>", i += "</div>", i += "</div>", i += '<div class="col-md-3 col-xs-3" style="text-align: end;">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + e[e.length - 1].ArrivalCityName + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date">' + e[e.length - 1].ArrivalTime + "</p>", i += "</div>", i += "</div>", i += "</div>", i += "SRF" == t ? '<div class="large-3 medium-3 small-3 columns f16 blue bld hide">INR ' + Math.ceil(parseInt(e[0].TotalFare)) + "/-</div>" : '<div class="large-3 medium-3 small-3 columns f16 blue bld hide">INR ' + e[0].TotalFare + "/-</div>"
    } else i = "6E" == e[0].MarketingCarrier && $.trim(e[0].sno).search("INDIGOCORP") >= 0 ? '<div class="large-2 medium-2 small-3 columns"><div> <img src="../AirLogo/smITZ.gif"  /></div> <div>' + e[0].FlightIdentification + '</br><span class="restext">' + e[0].DisplayFareType + "</span></div></div>" : '<div class="col-md-3 col-xs-3"><img src="../AirLogo/sm' + e[0].MarketingCarrier + '.gif" /><br>' + e[0].MarketingCarrier + "-" + e[0].FlightIdentification + "<br></div>", i += '<div class="col-md-3 col-xs-3">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + e[0].DepartureLocation + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date">' + e[0].DepartureTime + "</p>", i += "</div>", i += "</div>", i += '<div class="col-md-3 col-xs-2">', i += '<div class="fly">', i += '<div class="fly1">', i += '<span class="jtm">' + e[0].Departure_Date + "</span>", i += '</div><div class="fly2">', i += "</div>", i += "</div>", i += "</div>", i += '<div class="col-md-3 col-xs-3" style="text-align: end;">', i += '<div class="theme-search-results-item-flight-section-meta">', i += '<p class="theme-search-results-item-flight-section-meta-time">' + e[0].ArrivalLocation + "</p>", i += '<p class="theme-search-results-item-flight-section-meta-date">' + e[0].ArrivalTime + "</p>", i += "</div>", i += "</div>", "SRF" == t ? (i += '<div class="large-3 medium-3 small-3 columns f16 blue bld hide">INR ' + Math.ceil(parseInt(e[0].TotalFare)) + "/-</div>", i += '<div class="large-3 medium-3 small-3 columns"><img src="' + UrlBase + 'images/srf.png" title="Special Return Fare" /></div>') : i += '<div class="large-3 medium-3 small-3 columns f16 blue bld hide">INR ' + e[0].TotalFare + "/-</div>";
    return i
}, ResHelper.prototype.RTFFinalBook = function () {
    this.RtfBookBtn.click(function () {
        if (null != Obook && null != Rbook) {
            $("#searchquery").hide();
            var e = Obook[0].LineNumber,
                t = Rbook[0].LineNumber;
            if (ChangedFarePopupShow(0, 0, 0, "hide", "D"), 0 == $("input[name=RadioGroup1]").length ? SFMfare = "" : SFMfare = $("input[name=RadioGroup1]:checked").val(), "2" == SFMfare) {
                var i = Obook[0].SubKey + Rbook[0].SubKey,
                    a = null,
                    r = new Array;
                1 == SRFReprice && null != SRFResultAfterReprice && SRFResultAfterReprice.length > 0 && SRFResultAfterReprice[0].d.length > 0 && "6E" == SRFResultAfterReprice[0].d[0].ValiDatingCarrier.toUpperCase() ? (a = SRFResultAfterReprice[0].d, r = new Array(a), a[0].Trip) : (a = JSLINQ(CommanRTFArray).Where(function (e) {
                    return e.MainKey == i
                }).Select(function (e) {
                    return e
                }), r = new Array(a.items));
                for (var s = r[0][0].LineNumber, l = 0; l < r[0].length; l++) r[0][l].ProductDetailQualifier = r[0][l].LineNumber.split("ITZ")[1], r[0][l].LineNumber = r[0][l].LineNumber.split("api")[0];
                var n = JSON.stringify(r),
                    o = UrlBase + "FLTSearch1.asmx/Insert_Selected_FltDetails_LZCmp";
                $.ajax({
                    url: o,
                    type: "POST",
                    data: JSON.stringify({
                        a: n
                    }),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (e) {
                        for (var t = 0; t < r[0].length; t++) r[0][t].LineNumber = s;
                        "0" == e.d.ChangeFareO.TrackId ? (1 == confirm("Fare changed Please Try again.") && location.reload(), $("#ConfmingFlight").hide(), $(document).ajaxStop($.unblockUI)) : parseFloat(e.d.ChangeFareO.CacheTotFare) != parseFloat(e.d.ChangeFareO.NewTotFare) ? ChangedFarePopupShow(e.d.ChangeFareO.CacheTotFare, e.d.ChangeFareO.NewTotFare, e.d.ChangeFareO.TrackId, "show", e.d.ChangeFareO.NewNetFare, "D") : window.location = UrlBase + "domestic/passenger-summary?" + e.d.ChangeFareO.TrackId
                    },
                    error: function (e, t, i) {
                        for (var a = 0; a < r[0].length; a++) r[0][a].LineNumber = s;
                        window.location = UrlBase + "flight-search"
                    }
                })
            } else {
                var d = new Array(Obook, Rbook);
                for (l = 0; l < d.length; l++)
                    for (var c = 0; c < d[l].length; c++) d[l][c].ProductDetailQualifier = d[l][c].LineNumber.split("ITZ")[1], d[l][c].LineNumber = d[l][c].LineNumber.split("api")[0];
                var p = JSON.stringify(d);
                o = UrlBase + "FLTSearch1.asmx/Insert_Selected_FltDetails_LZCmp";
                $.ajax({
                    url: o,
                    type: "POST",
                    data: JSON.stringify({
                        a: p
                    }),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (i) {
                        for (var a = 0; a < Obook.length; a++) Obook[a].LineNumber = e;
                        for (var r = 0; r < Rbook.length; r++) Rbook[r].LineNumber = t;
                        var s = i.d.ChangeFareO.TrackId + "," + i.d.ChangeFareR.TrackId;
                        if ("0" == i.d.ChangeFareO.TrackId) {
                            1 == confirm("Fare changed Please Try again.") && location.reload(), $("#ConfmingFlight").hide(), $(document).ajaxStop($.unblockUI)
                        } else {
                            var l, n, o = parseFloat(Obook[0].TotalFare) + parseFloat(Rbook[0].TotalFare);
                            l = parseFloat(i.d.ChangeFareO.NewTotFare) + parseFloat(i.d.ChangeFareR.NewTotFare), n = parseFloat(i.d.ChangeFareO.NewNetFare) + parseFloat(i.d.ChangeFareR.NewNetFare), o != l ? ChangedFarePopupShow(o, l, s, "show", n, "D") : "0" == i.d.ChangeFareO.TrackId ? (alert("Selected fare has been changed.Please select another flight."), window.location = UrlBase + "flight-search") : window.location = UrlBase + "domestic/passenger-summary?" + s
                        }
                    },
                    error: function (i, a, r) {
                        for (var s = 0; s < Obook.length; s++) Obook[s].LineNumber = e;
                        for (var l = 0; l < Rbook.length; l++) Rbook[l].LineNumber = t;
                        window.location = UrlBase + "flight-search"
                    }
                })
            }
        }
    })
}, ResHelper.prototype.FltrSortR = function (e) {
    $("#IdFareType").show(), this.GetStopFilter(e[0], "stops", "O"), this.GetUniqueAirline(e[0], "airlineImage", "O"), this.GetUniqueAirlineFareType(e[0], "AirlineFareType", "O");
    var t = this.GetMinMaxPrice(e[0]),
        i = this.GetMinMaxTime(e[0]),
        a = this;
    $(".closeopen").next().slideDown(), $(".closeopen").toggleClass("closeopen1"), a.MainSF.jplist({
        debug: !1,
        itemsBox: ".listO",
        itemPath: ".list-item",
        panelPath: ".jplist-panel",
        storage: "",
        controlTypes: {
            "range-slider": {
                className: "RangeSlider",
                options: {
                    ui_slider: function (e, i, a) {
                        e.slider({
                            min: t[0],
                            max: t[1],
                            range: !0,
                            values: [t[0], t[1]],
                            slide: function (e, t) {
                                i.text(t.values[0]), a.text(t.values[1])
                            }
                        })
                    },
                    set_values: function (e, t, i) {
                        t.text(e.slider("values", 0)), i.text(e.slider("values", 1))
                    }
                }
            },
            "range-slider-Time": {
                className: "RangeSlider",
                options: {
                    ui_slider: function (e, t, r) {
                        e.slider({
                            min: i[0],
                            max: i[1],
                            range: !0,
                            values: [i[0], i[1]],
                            slide: function (e, i) {
                                t.text(a.getFourDigitTime(i.values[0]) + " Hrs"), r.text(a.getFourDigitTime(i.values[1]) + " Hrs")
                            }
                        })
                    },
                    set_values: function (e, t, i) {
                        t.text(a.getFourDigitTime(e.slider("values", 0)) + " Hrs"), i.text(a.getFourDigitTime(e.slider("values", 1)) + " Hrs")
                    }
                }
            },
            AirlinefilterO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            StopfilterO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            RfndfilterO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            FareTypefilterO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            reset: {
                className: "Reset",
                options: {}
            },
            "button-text-filter-groupO": {
                className: "TextFilterGroup",
                options: {
                    ignore: "[~!@#$%^&*()+=`'\"/\\_]+"
                }
            },
            reset1: {
                className: "Reset",
                options: {}
            },
            sortCITZ1: {
                className: "DefaultSort1",
                options: {}
            },
            sortAirline: {
                className: "DefaultSort1",
                options: {}
            },
            sortDeptime: {
                className: "DefaultSort1",
                options: {}
            },
            sortArrtime: {
                className: "DefaultSort1",
                options: {}
            },
            sortTotdur: {
                className: "DefaultSort1",
                options: {}
            },
            DTimefilterO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            AirlineFareTypeO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            }
        }
    }), this.GetStopFilter(e[1], "stops", "R"), this.GetUniqueAirline(e[1], "airlineImage", "R"), this.GetUniqueAirlineFareType(e[1], "AirlineFareType", "R");
    t = this.GetMinMaxPrice(e[1]), i = this.GetMinMaxTime(e[1]);
    (a = this).MainSFR.jplist({
        debug: !1,
        itemsBox: ".listR",
        itemPath: ".list-itemR",
        panelPath: ".jplist-panel",
        storage: "",
        controlTypes: {
            "range-sliderR": {
                className: "RangeSlider",
                options: {
                    ui_slider: function (e, i, a) {
                        e.slider({
                            min: t[0],
                            max: t[1] + 1,
                            range: !0,
                            values: [t[0], t[1] + 1],
                            slide: function (e, t) {
                                i.text(t.values[0]), a.text(t.values[1])
                            }
                        })
                    },
                    set_values: function (e, t, i) {
                        t.text(e.slider("values", 0)), i.text(e.slider("values", 1))
                    }
                }
            },
            "range-slider-TimeR": {
                className: "RangeSlider",
                options: {
                    ui_slider: function (e, t, r) {
                        e.slider({
                            min: i[0],
                            max: i[1],
                            range: !0,
                            values: [i[0], i[1]],
                            slide: function (e, i) {
                                t.text(a.getFourDigitTime(i.values[0]) + " Hrs"), r.text(a.getFourDigitTime(i.values[1]) + " Hrs")
                            }
                        })
                    },
                    set_values: function (e, t, i) {
                        t.text(a.getFourDigitTime(e.slider("values", 0)) + " Hrs"), i.text(a.getFourDigitTime(e.slider("values", 1)) + " Hrs")
                    }
                }
            },
            AirlinefilterR: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            StopfilterR: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            RfndfilterR: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            FareTypefilterR: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            reset: {
                className: "Reset",
                options: {}
            },
            "button-text-filter-groupR": {
                className: "TextFilterGroup",
                options: {
                    ignore: "[~!@#$%^&*()+=`'\"/\\_]+"
                }
            },
            reset1: {
                className: "Reset",
                options: {}
            },
            sortCITZR: {
                className: "DefaultSort1",
                options: {}
            },
            sortAirlineR: {
                className: "DefaultSort1",
                options: {}
            },
            sortDeptimeR: {
                className: "DefaultSort1",
                options: {}
            },
            sortArrtimeR: {
                className: "DefaultSort1",
                options: {}
            },
            sortTotdurR: {
                className: "DefaultSort1",
                options: {}
            },
            DTimefilterR: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            AirlineFareTypeR: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            }
        }
    })
}, ResHelper.prototype.FltrSort = function (e) {
    $("#IdFareType").hide(), this.GetStopFilter(e[0], "stops", "O"), this.GetUniqueAirline(e[0], "airlineImage", "O"), this.GetUniqueAirlineFareType(e[0], "AirlineFareType", "O");
    var t = this.GetMinMaxPrice(e[0]),
        i = this.GetMinMaxTime(e[0]),
        a = this;
    $(".closeopen").next().slideDown(), $(".closeopen").toggleClass("closeopen1"), a.MainSF.jplist({
        debug: !1,
        itemsBox: ".list",
        itemPath: ".list-item",
        panelPath: ".jplist-panel",
        storage: "",
        controlTypes: {
            "range-slider": {
                className: "RangeSlider",
                options: {
                    ui_slider: function (e, i, a) {
                        e.slider({
                            min: t[0],
                            max: t[1],
                            range: !0,
                            values: [t[0], t[1]],
                            slide: function (e, t) {
                                i.text(t.values[0]), a.text(t.values[1])
                            }
                        })
                    },
                    set_values: function (e, t, i) {
                        t.text(e.slider("values", 0)), i.text(e.slider("values", 1))
                    }
                }
            },
            "default-sort": {
                className: "DefaultSort",
                options: {
                    isDefault: !1
                }
            },
            "range-slider-Time": {
                className: "RangeSlider",
                options: {
                    ui_slider: function (e, t, r) {
                        e.slider({
                            min: i[0],
                            max: i[1],
                            range: !0,
                            values: [i[0], i[1]],
                            slide: function (e, i) {
                                t.text(a.getFourDigitTime(i.values[0]) + " Hrs"), r.text(a.getFourDigitTime(i.values[1]) + " Hrs")
                            }
                        })
                    },
                    set_values: function (e, t, i) {
                        t.text(a.getFourDigitTime(e.slider("values", 0)) + " Hrs"), i.text(a.getFourDigitTime(e.slider("values", 1)) + " Hrs")
                    }
                }
            },
            AirlinefilterO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            StopfilterO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            RfndfilterO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            reset: {
                className: "Reset",
                options: {}
            },
            reset1: {
                className: "Reset",
                options: {}
            },
            sortCITZ: {
                className: "DefaultSort1",
                options: {}
            },
            sortAirline: {
                className: "DefaultSort1",
                options: {}
            },
            sortDeptime: {
                className: "DefaultSort1",
                options: {}
            },
            sortArrtime: {
                className: "DefaultSort1",
                options: {}
            },
            sortTotdur: {
                className: "DefaultSort1",
                options: {}
            },
            "button-text-filter-groupO": {
                className: "TextFilterGroup",
                options: {
                    ignore: "[~!@#$%^&*()+=`'\"/\\_]+"
                }
            },
            views: {
                className: "Views",
                options: {}
            },
            DTimefilterO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            },
            AirlineFareTypeO: {
                className: "CheckboxTextFilter",
                options: {
                    ignore: ""
                }
            }
        }
    })
}, ResHelper.prototype.GetUniqueAirlineFareType = function (e, t, i) {
    var a = new Array;
    a = JSLINQ(e).OrderBy(function (e) {
        return e.AdtFar
    }).Where(function (e) {
        return e.AdtFar.length < 5
    }).Select(function (e) {
        return e.AdtFar
    }).items.unique1();
    var r = '<div class="jplist-group" data-control-type="AirlineFareType' + i + '" data-control-action="filter"  data-control-name="AirlineFareType' + i + '"';
    r += ' data-path=".' + t + '" data-logic="or">';
    for (var s = 0; s < a.length; s++) {
        var l = "",
            n = "";
        "CPN" == a[s] || "Coupon Fare" == a[s] ? (n = "Coupon", l = "CPN") : "CRP" == a[s] ? (n = "RWT Fare", l = "CRP") : "HBAG" == a[s] || "HBG" == a[s] ? (n = "Hand Baggage", l = "HBAG") : "PKG" == a[s] ? (n = "Package Fare", l = "PKG") : "SME" == a[s] ? (n = "SME Fare", l = "SME") : "NRM" != a[s] && "" != a[s] ? (n = a[s], l = a[s]) : (n = "Published", l = "NRM"), "" != l && (r += '<label for="' + l + '" style="width:100%;"><input value="' + l + '"  id="CheckboxA' + i + s + '1"  type="checkbox"  />&nbsp;' + n + "</label>")
    }
    r += "</div>", "O" == i ? this.AirlineFareType.html(r) : this.AirlineFareTypeR.html(r)
}, ChangedFarePopupShow = function (e, t, i, a, r, s) {
    if ("show" == a) {
        var l = '<div class="f14 bld colorp">Oops! your selected flight fare has been changed';
        l += '</div><hr /> <div class="clear1"> </div><div class="w95 padding1 mauto bgpp f16">Updated fare is', l += ' <img src="../Images/rs.png" style="height: 12px; position:relative; top:1px;" /><span class="bld" id="spnupfare">' + t + '</span><span style="position:absolute; background:#f9f9f9; padding:5px; box-shadow:1px 2px 4px #888; margin-top:-5px;margin-left:10px;display:none;" id="divnetfare">NetFare: <img src="../Images/rs.png" style="height: 12px; position:relative; top:1px;" />' + r + "</span>", l += '</div><div class="clear1"></div><div class="clear1"></div><div class="bld w90 mauto">', l += ' <div class="w33 lft"> <div> Updated Fare</div> <div class="f14"> <img src="../Images/rs.png" style="height: 10px;" /> ' + t + "</div></div>", l += ' <div class="w33 lft"><div> Previous Fare</div><div class="f14"><img src="../Images/rs.png" style="height: 10px;" /> ' + e + "</div>", l += '</div><div class="w33 lft colorp"><div> Fare Difference</div> <div class="f14">', l += (parseFloat(t) > parseFloat(e) ? '(+) <img src="../Images/rsp.png" style="height: 10px;" /> ' + (parseFloat(t) - parseFloat(e)) + "</div> " : '(-) <img src="../Images/rsp.png" style="height: 10px;" /> ' + (parseFloat(e) - parseFloat(t)) + "</div> ") + '</div> </div></div> <div class="clear1">', l += '</div><div class="clear1"> </div><div class="w95 rgt"> <ul><span class="bld">Reasons:</span>', l += "   <li>Airfares are dynamic and subject to change. This change is beyond our control.</li>", l += " <li>The updated fare is the cheapest fare currently available on your selected flight.</li>", l += '</ul> </div> <div class="clear1"> </div><div class="textaligncenter">', l += '<input type="button" id="Cancelflt" name="Choose another flight" value="Choose another flight" class="button1" />', l += ' <input type="button" id="ContinueCflt" name="Continue" value="Continue" class="button1" style="margin-right:2px;" />', l += ' </div> <div class="clear1"></div>', $("#divLoadcf").html(""), $("#divLoadcf").html(l), $("#divLoadcf").show(), $("#Cancelflt").click(function () {
            $("#ConfmingFlight").hide()
        }), $("#ContinueCflt").click(function () {
            "" == i ? $("#ConfmingFlight").hide() : "I" == $.trim(s).toUpperCase() ? window.location = UrlBase + "International/PaxDetails.aspx?" + i + ",I" : window.location = UrlBase + "domestic/passenger-summary?" + i
        }), $("#spnupfare").mouseover(function () {
            $("#divnetfare").show()
        }), $("#spnupfare").mouseout(function () {
            $("#divnetfare").hide()
        })
    } else if ("hide" == a) {
        '<svg xmlns="http://www.w3.org/2000/svg" version="1.1">',
            "        <defs>",
            '            <filter id="gooey">',
            '                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur"></feGaussianBlur>',
            '                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo"></feColorMatrix>',
            '                <feBlend in="SourceGraphic" in2="goo"></feBlend>',
            "            </filter>",
            "        </defs>",
            "</svg>",
            '<div class="loading" >',
            '   <p style="position: absolute;left: 0;bottom: 220px;text-align: center;width: 100%;color:#fff;">Please wait</p>',
            "   <span><i></i><i></i></span>",
            "</div>",
            $("#divLoadcf").html(""),
            $("#divLoadcf").html('<svg xmlns="http://www.w3.org/2000/svg" version="1.1">        <defs>            <filter id="gooey">                <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur"></feGaussianBlur>                <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo"></feColorMatrix>                <feBlend in="SourceGraphic" in2="goo"></feBlend>            </filter>        </defs></svg><div class="loading" >   <p style="position: absolute;left: 0;bottom: 220px;text-align: center;width: 100%;color:#fff;">Please wait</p>   <span><i></i><i></i></span></div>'),
            $("#divLoadcf").show()
    }
    $("#ConfmingFlight").show()
}, $(".abdd div").click(function (e) {
    e.preventDefault(), $(this).next().toggle("fast"), $(this).children("i:last-child").toggleClass("fa-caret-down fa-caret-up")
});