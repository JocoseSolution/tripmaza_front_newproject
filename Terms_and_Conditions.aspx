﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="Terms_and_Conditions.aspx.vb" Inherits="privacy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="theme-hero-area theme-hero-area-half">
        <div class="theme-hero-area-bg-wrap">

            <div class="theme-hero-area-bg" style="background-image: url(https://boatrentalusa.com/wp-content/uploads/2021/06/terms.jpg);"></div>
            <%--<div class="theme-hero-area-mask theme-hero-area-mask-half"></div>--%>
            <div class="theme-hero-area-inner-shadow"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
                        <div class="theme-page-header theme-page-header-lg">
                            <h1 class="theme-page-header-title">Terms and condition</h1>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">

            <p style="text-align: justify">
                Trip Hojaye Private Limited requests you to consult your local regional authorities and evaluate travel prohibited , warning, restrictions and advisories
issued by them before booking travel to certain international/Domestic destinations. By offering for sale travel to particular international/Domestic destinations,
Trip Hojaye Private Limited does not represent or warrant that travel to such a point is advisable or without risk.
Trip Hojaye Private Limited does not accept liability for damages, losses, or delays that may result from improper documents for entry, exit, length of stay,
or from travel to such destinations.
            </p>

            <p style="text-align: justify">
                Trip Hojaye Private Limited reserves its exclusive right in its sole discretion to alter, limit or discontinue the Site or any material posted herein,
in any respect.Trip Hojaye Private Limited shall have no obligation to take the needs of any User into consideration in connection therewith.
Trip Hojaye Private Limited reserves its right to deny in its sole discretion any user access to this Site or any portion thereof without notice.
No waiver by Trip Hojaye Private Limited of any provision of these Terms and Conditions shall be binding except as set forth in writing and
signed by its duly authorized representative. If any dispute arises between you and Trip Hojaye Private Limited during your use of the Site or thereafter,
in connection with and arising from your use or attempt to use this Site, the dispute shall be referred to arbitration. The place of arbitration shall (Address mention).
The arbitration proceedings shall be in the English language. The said arbitration proceedings shall be governed and construed in accordance with the Arbitration.
            </p>

            <p style="text-align: justify">
                Any advance amount paid by the agent will be non-interest bearing and should be utilized within 360 days from the date of payment.
If such advance is not utilized within this period of 360 days, the unutilized portion shall be held in trust, for the benefit of the agent,
by Travelboutiqueonline for a period of 2 years from the date of payment of such advance after which the same shall be deemed as forfeited.
            </p>


            <h3>Payments</h3>
            <p style="text-align: justify">
                CASH TRANSACTION NOT ALLOWED FOR AMOUNT MORE THAN OR EQUAL TO 2 LAKH. Income Tax Act restricts any person to receive an amount of two lakh rupees or more in cash,
from a person in a day, in respect of a single transaction or in respect of transactions relating to one event or occasion from a person, under Section 269ST if you
deposit cash Charges will be apply(Penalty and it is an RBI Regulatory breach)..kindly make Online payment/RTGS/NEFT Or by Cheque..(Refere RBI circular for your reference)
            </p>

            <p style="text-align: justify">
                Trip Hojaye Private Limited shall have the right to charge transaction fees based on certain completed transactions using the Services.
These charges/fees may also be altered by Trip Hojaye Private Limited without any notice. You shall be completely responsible for all charges, fees, duties, taxes,
and assessments arising out of the use of the Services.
            </p>

            <p style="text-align: justify">
                In order to process the online payments, Trip Hojaye Private Limited may require details of your bank account, credit card number etc.
Please check the Privacy Policy.located at <<trip hojaye.com>>.on how Trip Hojaye Private Limited uses such confidential information provided by you.
            </p>

            <p style="text-align: justify">
                You hereby understand that agree that your reservation/booking is contingent upon Trip Hojaye Private Limited receiving the applicable fees/consideration/fares in
its account and unless such moneys have been credited into Trip Hojaye's account, it shall be under no obligation to issue you with the relevant tickets,
reservation confirmation, passenger name record (PNR) or such other confirmations in connection with the Services.
            </p>

            <h3>Ownership</h3>

            <p style="text-align: justify">
                All materials on this Site, including but not limited to audio, images, software, text, icons and such like (the "Content"), are protected by copyright under
international conventions and copyright laws. You cannot use the Content, except as specified herein. You agree to follow all instructions on this Site limiting
the way you may use the Content.There are a number of proprietary logos, service marks and trademarks found on this Site whether owned/used by trip hojaye.com
or otherwise. By displaying them on this Site, Trip Hojaye Private Limited is not granting you any license to utilize those proprietary logos, service marks, or trademarks.
            </p>

            <p style="text-align: justify">Any unauthorized use of the Content may violate copyright laws, trademark laws, the laws of privacy and publicity, and civil and criminal statutes.</p>

            <p style="text-align: justify">You may download such copy/copies of the Content to be used only by you for your personal use at home unless the subsite you are accessing states that you may not.</p>

            <p style="text-align: justify">If you download any Content from this Site, you shall not remove any copyright or trademark notices or other notices that go with it.</p>


            <h3>Disclaimer</h3>

            <ul>
                <li style="text-align: justify">The material in this Site could include technical inaccuracies or typographical errors. trip hojaye.com may make changes or improvements at any time.</li>

                <li style="text-align: justify">It is the responsibility of the hotel chain and/or the individual property to ensure the accuracy of the photos displayed. 'trip .com' is not
responsible for any inaccuracies in the photos.</li>

                <li style="text-align: justify">Trip Hojaye Private Limited will not be liable to you or to any other person for any direct, indirect, incidental, punitive or consequential loss, damage,
cost or expense of any kind whatsoever and howsoever caused from out of your usage of this Site.</li>

                <li style="text-align: justify">Terms and Conditions of Use
Trip Hojaye Private Limited may add to, change or remove any part of these Terms and Conditions of Use at any time, without notice.
Any changes to these Terms and Conditions of Use or any terms posted on this Site apply as soon as they are posted. By continuing to use this
Site after any changes are posted, you are indicating your acceptance of those changes.</li>

               
            </ul>
        </div>
    </div>

</asp:Content>

