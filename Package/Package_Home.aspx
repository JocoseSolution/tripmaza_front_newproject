﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForPack.master" AutoEventWireup="true" CodeFile="Package_Home.aspx.cs" Inherits="Package_Package_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="theme-hero-area" style="margin-top: -5px;">
        <div class="theme-hero-area-bg-wrap">
            <div class="theme-hero-area-bg theme-hero-area-bg-blur" style="background-image: url(../Advance_CSS/img/index/paul-gilmore-km74CLco7qs-unsplash.jpg);"></div>
            <div class="theme-hero-area-mask theme-hero-area-mask-strong"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="_pb-100 _pt-150 _pv-mob-60">
                    <div class="theme-search-area theme-search-area-stacked">
                        <div class="theme-search-area-header _c-w">
                            <h1 class="theme-search-area-title theme-search-area-title-lg" style="color:#fff;">Search for
                 
                                <br />
                                Travel Expericences
                </h1>
                            <p class="theme-search-area-subtitle">Explore 560,000+ travel expericences from all over the world</p>
                        </div>
                        <div class="theme-search-area-form" id="hero-search-form">
                            <div class="row">
                                <div class="col-md-4 ">
                                    <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon lin lin-location-pin" styyle="font-size: 22px;margin-top: 8px;"></i>
<%--                                            <input class="theme-search-area-section-input typeahead" type="text" placeholder="Destination" data-provide="typeahead" />--%>
                                            <asp:TextBox ID="TxtDestination" CssClass="theme-search-area-section-input typeahead" placeholder="Destination" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                               
                                        <div class="col-md-4 ">
                                            <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon lin lin-calendar" styyle="font-size: 22px;margin-top: 8px;"></i>
                                                    <input class="theme-search-area-section-input datePickerStart _mob-h" value="Wed 06/27" type="text" placeholder="Check-in" />
                                                    <input class="theme-search-area-section-input _desk-h mobile-picker" value="2018-06-27" type="date" />
                                                </div>
                                            </div>
                                        </div>
                                  
                                <div class="col-md-1 ">
<%--                                    <button class="theme-search-area-submit _mt-0 theme-search-area-submit-glow theme-search-area-submit-curved theme-search-area-submit-no-border">
                                        <i class="theme-search-area-submit-icon fa fa-angle-right"></i>
                                        <span class="_desk-h">Search</span>
                                    </button>--%>
                                    <asp:Button ID="Search" OnClick="Search_Click" runat="server" Text="Search" class="theme-search-area-submit _mt-0 theme-search-area-submit-glow theme-search-area-submit-curved theme-search-area-submit-no-border" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="_pt-60">
                        <div class="row row-col-mob-gap" data-gutter="60" style="    background: #00000070;color: #fff;padding: 25px;border-radius: 10px;">
                            <div class="col-md-3 ">
                                <div class="feature">
                                    <i class="feature-icon _mr-20 feature-icon-white feature-icon-xl fa fa-trophy"></i>
                                    <div class="feature-caption _c-w">
                                        <h5 class="feature-title _mb-10" style="color:#fff;">Gifts & Rewards</h5>
                                        <p class="feature-subtitle _op-06" style="color:#eee;">Get even more from our service. Spend less and travel more</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="feature">
                                    <i class="feature-icon _mr-20 feature-icon-white feature-icon-xl fa fa-credit-card"></i>
                                    <div class="feature-caption _c-w">
                                        <h5 class="feature-title _mb-10" style="color:#fff;">Best prices</h5>
                                        <p class="feature-subtitle _op-06" style="color:#eee;">We are comparing hundreds travel websites to find best price for you</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="feature">
                                    <i class="feature-icon _mr-20 feature-icon-white feature-icon-xl fa fa-users"></i>
                                    <div class="feature-caption _c-w">
                                        <h5 class="feature-title _mb-10" style="color:#fff;">3.5m Travelers</h5>
                                        <p class="feature-subtitle _op-06" style="color:#eee;">Enjoyed our service from all around the world</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="feature">
                                    <i class="feature-icon _mr-20 feature-icon-white feature-icon-xl fa fa-calendar"></i>
                                    <div class="feature-caption _c-w">
                                        <h5 class="feature-title _mb-10" style="color:#fff;">Trip Manager</h5>
                                        <p class="feature-subtitle _op-06" style="color:#eee;">Be in control with your trips by using our free and award winning manager</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">
            <div class="theme-page-section-header _ta-l">
                <h5 class="theme-page-section-title">Explore Domestic Expericences</h5>
                <p class="theme-page-section-subtitle"> <a href="Package_List.aspx?PkgType=Domestic">View All Domestic Package</a></p>

            </div>
            <div class="row row-col-gap" data-gutter="10">
                <asp:Repeater ID="DomesticPkg" runat="server">
                    <ItemTemplate>
                        <div class="col-md-3 ">
                            <div class="banner _br-4 banner-sqr banner-animate banner-animate-zoom-in banner-animate-very-slow banner-animate-mask-out">
                             <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>
                                <a class="banner-link" href="Package_Details.aspx?Pkg_Id=<%# Eval("pkg_Id") %> & counter=<%# Eval("counter") %>"></a>
                                <div class="banner-caption _pt-60 banner-caption-bottom banner-caption-grad">
                                    <h4 class="banner-title _tt-uc _fs-sm _fw-n" style="color:white !important;"><%# Eval("pkg_Title") %></h4>
                                    <p class="banner-subtitle _fw-n"><%# Eval("pkg_theme") %></p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

            </div>
        </div>
    </div>

        <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">
            <div class="theme-page-section-header _ta-l">
                <h5 class="theme-page-section-title">Explore International Expericences</h5>
                <p class="theme-page-section-subtitle"> <a href="Package_List.aspx?PkgType=International">View All International Package</a></p>
               
            </div>
            <div class="row row-col-gap" data-gutter="10">
                <asp:Repeater ID="InetrnationalPkg" runat="server">
                    <ItemTemplate>
                        <div class="col-md-3 ">
                            <div class="banner _br-4 banner-sqr banner-animate banner-animate-zoom-in banner-animate-very-slow banner-animate-mask-out">
                                            <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>
                                <a class="banner-link" href="Package_Details.aspx?Pkg_Id=<%# Eval("pkg_Id") %> & counter=<%# Eval("counter") %>"></a>
                                <div class="banner-caption _pt-60 banner-caption-bottom banner-caption-grad">
                                    <h4 class="banner-title _tt-uc _fs-sm _fw-b" style="color:white !important;"><%# Eval("pkg_Title") %></h4>
                                    <p class="banner-subtitle _fw-n"><%# Eval("pkg_theme") %></p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

            </div>
        </div>
    </div>
    <div class="container">
         <div class="theme-page-section-header _ta-l">
                <h5 class="theme-page-section-title">Explore All Expericences</h5>
                <p class="theme-page-section-subtitle"> <a href="Package_List.aspx?PkgType=All">View All  Package</a></p>
               
            </div>
    </div>


    <div class="theme-page-section theme-page-section-xxl">
      <div class="container">
        <div class="theme-page-section-header _ta-l">
          <h5 class="theme-page-section-title">Paris Photos</h5>
          <p class="theme-page-section-subtitle">Feel the taste of the capital of love</p>
        </div>
        <div class="row magnific-gallery row-col-gap" data-gutter="10">
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
          <div class="col-xs-2 ">
            <div class="banner _br-2 banner-sqr banner-">
              <div class="banner-bg" style="background-image:url(./img/800x600.png);"></div>
              <a class="banner-link" href="./img/800x600.png"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    
</asp:Content>

