﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

public partial class Package_Package_List : System.Web.UI.Page
{
    private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        string type = Request.QueryString["PkgType"];
        string SearchQuery = Request.QueryString["SearchQuery"];

        if ((type != "Nothing") && (type == "Domestic"))
        {


            GetAllDomesticPackage();
        }
        else if ((type != "Nothing") && (type == "International"))
        {
            GetAllInternationalPackage();
        }
        else if ((type != "Nothing") && (type == "All"))
        {
            BindAllPackage();
        }
        else if ((type != "Nothing") && (type == "Search"))
        {
            SearchAllPackage(SearchQuery);
        }
    }
    protected void BindAllPackage()
    {
        try
        {
            con.Open();
            string sp = "SELECT * FROM pkg_Details WHERE Pkg_status='1'";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            Package.DataSource = dt;
            Package.DataBind();
            int numberOfRecords = dt.Rows.Count;
            Total.Text = Convert.ToString(numberOfRecords);
            // TypePkg.Text = "All "
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }
    protected void SearchAllPackage(string SearchQuery)
    {
        try
        {
            con.Open();
            string sp = "SELECT * FROM pkg_Details WHERE pkg_destinetion_city LIKE '"+ SearchQuery + "'";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(sp, con);
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            Package.DataSource = dt;
            Package.DataBind();
            int numberOfRecords = dt.Rows.Count;
            Total.Text = Convert.ToString(numberOfRecords);
            // TypePkg.Text = "All "
            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }

    public void GetAllDomesticPackage()
    {
        // Dim Connection As New SqlConnection(conn)
        try
        {
            con.Open();
            string SP = "SELECT TOP 4 * FROM Pkg_Details Where Pkg_Type='Domestic'";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(SP, con);
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            Package.DataSource = dt;
            Package.DataBind();
            int numberOfRecords = dt.Rows.Count;
            Total.Text = Convert.ToString(numberOfRecords);
            // TypePkg.Text = "All "
            con.Close();
        }
        catch (Exception ex)
        {
        }
    }
    public void GetAllInternationalPackage()
    {
        // Dim Connection As New SqlConnection(conn)
        try
        {
            con.Open();
            string SP = "SELECT TOP 4 * FROM Pkg_Details Where Pkg_Type='International'";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(SP, con);
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            Package.DataSource = dt;
            Package.DataBind();
            int numberOfRecords = dt.Rows.Count;
            Total.Text = Convert.ToString(numberOfRecords);
            // TypePkg.Text = "All "
            con.Close();
        }
        catch (Exception ex)
        {
        }
    }
}