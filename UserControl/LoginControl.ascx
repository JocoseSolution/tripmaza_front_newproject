﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginControl.ascx.vb" Inherits="UserControl_LoginControl" %>

<style type="text/css">
    .mb-20 {
        margin-bottom: 20px;
    }

    .single-choose i {
        color: #fff;
        font-size: 22px;
        text-align: center;
        transition: all 0.3s ease 0s;
        float: left;
        margin-right: 25px;
        display: inline-block;
        position: relative;
        margin-top: 5px;
        margin-left: 0;
        border: 1px solid #096baf;
        background-color: #096baf;
        width: 55px;
        height: 55px;
        line-height: 55px;
        border-radius: 50%;
        box-shadow: 0px 5px 11px rgb(0 0 0 / 10%);
    }

    .choose-content {
        overflow: hidden;
    }

    .single-choose h4 {
        font-size: 18px;
        margin-top: 0px;
        text-transform: capitalize;
    }

    .bg-grays {
        position: relative;
    }

    .ws-section-spacing {
        padding: 80px 0 60px;
    }

    .center-title3 {
        text-align: center;
        padding-bottom: 42px;
    }

        .center-title3 .title3 {
            text-transform: capitalize;
            padding-bottom: 2rem;
            color: #404040;
            font-size: 40px;
        }

        .center-title3 .sub-title3 {
            font-family: 'Quicksand', sans-serif;
            font-weight: 500;
            font-size: 16px;
            line-height: 28px;
            max-width: 750px;
            margin: 0 auto;
            color: #404040;
        }

    .servicebox-one {
        position: relative;
        margin-bottom: 30px;
        padding: 30px 20px;
        box-shadow: 0px 0px 40px rgb(0 0 0 / 20%);
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
        background-color: #fff;
    }

        .servicebox-one .service-box-icon {
            font-size: 36px;
            line-height: 1;
            margin-bottom: 12px;
            color: #fc5b62;
        }

        .servicebox-one .service-box-title {
            margin-bottom: 15px;
            text-transform: capitalize;
            color: #404040;
            font-size: 28px;
        }

        .servicebox-one .service-box-desc {
            margin-bottom: 15px;
            color: #212121;
        }
</style>

<style type="text/css">
    .container-fluid {
        padding-right: 90px;
        padding-left: 90px;
        margin-right: auto;
        margin-left: auto;
        margin-top: 20px;
    }

    .service__icon--heading {
        font-size: 25px;
        text-align: center;
        line-height: 1.3;
        padding: 25px 0;
        color: #808080;
        font-weight: 200;
    }

    .float-md-left {
        float: left !important;
    }

    .service__icon--list {
        display: inline-block;
        width: 49%;
        text-align: center;
    }

    .service__icon--label {
        font-size: 14px;
        color: #324253;
        padding-top: 10px;
    }

    .round-ico {
        border: 2px solid #e67817;
        border-radius: 50%;
        padding: 7px;
        color: #e25b18;
    }

    .semiround {
        background: #24547d;
        padding: 10px;
        border-radius: 10px;
        color: #fff;
    }

    .business__image {
        height: 300px;
    }

    .pt-5, .py-5 {
        padding-top: 3rem !important;
    }

    .main__heading {
        font-size: 32px;
        color: #333;
    }

    .mb-3, .my-3 {
        margin-bottom: 1rem !important;
    }

    @media (min-width: 768px) {
        .col-md-6 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
    }

    .business__points--text {
        font-size: 14px;
        line-height: 1.5;
        color: #666;
        padding: 10px 0;
    }

    .business__content {
        display: flex;
        align-items: center;
        padding: 10px 0;
    }

    .pl-2, .px-2 {
        padding-left: .5rem !important;
    }

    .business__details--heading {
        font-size: 20px;
        color: #324253;
        font-weight: 500;
        margin-bottom: 0px;
    }

    .business__details--text {
        font-size: 12px;
        color: #999999;
        font-weight: 300;
        padding-top: 5px;
    }

    .business {
        font-size: 14px;
        line-height: 1.5;
        color: #666;
        padding: 10px 0;
    }

    .loginas__tab {
        padding: 25px 0 20px;
    }

    .loginas__tab--list.active, .loginas__tab--list:hover {
        color: #ff1228;
        border-bottom: 1px solid #ff1228;
    }

    .loginas__tab--list {
        display: inline-block;
        margin-right: 15px;
        font-size: 12px;
        font-family: "TJ_font-700";
        font-weight: 500;
        cursor: pointer;
        padding-bottom: 8px;
        color: #999;
        font-family: 'Quicksand', sans-serif !important;
    }

    .form__field--text {
        font-size: 14px;
        padding: 10px 0;
        color: #000;
        text-align: center;
    }

    .login__form--reset--link {
        color: #ff1228;
    }

    .trav-b2b {
        font-size: 20px;
        color: #000000;
        width: 800px;
        margin: 0 auto;
        text-align: center;
        font-weight: 600;
        padding: 0px 0 60px 0;
    }

    .b2b-product-tab {
        display: flex;
        justify-content: center;
        align-items: center;
        padding-bottom: 150px;
        margin: 0 auto;
        margin-right: 50%;
        margin-left: 55%;
    }

    .product-type {
        height: 100px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-end;
        padding-right: 93px;
    }

    .b2b-static-cont .b2b-product-tab .product-type img {
        display: block;
    }

    .product-type span {
        display: block;
        font-size: 22px;
        color: #ee3157;
        padding-top: 17px;
        font-weight: 600;
    }

    .easier-management-types-cont {
        background: #fafafa;
        margin-bottom: 90px;
    }

        .easier-management-types-cont .perfect-heading {
            padding-top: 100px;
            padding-bottom: 60px;
        }

    .aertrip-color {
        color: #ffbe07;
    }

    .easier-management-types {
        display: flex;
        justify-content: center;
        text-align: center;
    }

        .easier-management-types .management-lists {
            margin-right: 50px;
            width: 162px;
            color: #000;
            font-size: 16px;
            font-weight: 600;
        }

            .easier-management-types .management-lists .image {
                padding: 0;
                width: 93px;
                margin: 0 auto;
            }

            .easier-management-types .management-lists .name {
                padding-top: 16px;
                color: #000;
            }

    element.style {
    }

    .easier-management-para {
        font-size: 24px;
        padding: 85px 0 100px 0;
        width: 916px;
        margin: 0 auto;
        text-align: center;
        color: #000000;
    }

    .perfect-heading {
        font-size: 55px;
        text-align: center;
        margin: 0;
        padding: 0 0 50px 0;
        font-weight: bold;
        color: #2c3a4e;
    }

    @media only screen and (max-width: 500px) {
        .banner {
            display: none;
        }

        .banner, .business, .deal, .theme-copyright {
            display: none;
        }

        .navbar-theme.navbar-theme-abs {
            display: none !important;
        }

        .theme-login-header {
            text-align: center;
        }
    }

    input[type="radio"] {
        visibility: hidden;
        height: 0;
        width: 0;
    }

    label {
        display: flex;
        vertical-align: middle;
        align-items: center;
        justify-content: center;
        text-align: center;
        cursor: pointer;
        background-color: var(--color-gray);
        color: #828282;
        padding: 5px 10px;
        border-radius: 6px;
        transition: color --transition-fast ease-out, background-color --transition-fast ease-in;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        margin-right: 8px;
    }

        label:last-of-type {
            margin-right: 0;
        }

    input[type="radio"]:checked + label {
        color: #e14e18;
        border-bottom: 2px solid #0672b7;
    }

    input[type="radio"]:hover:not(:checked) + label {
        background-color: none;
        color: #000;
    }

    .InputGroup {
        display: flex;
    }
    .theme-search-area-section-input {
        border: 1px solid #0f64a7;
    }
</style>




<div class="theme-hero-area" style="margin-top: 50px;">

    <section class="theme-page-section theme-page-section-xl theme-page-section-gray" style="position: relative;">
        <div class="container">
            <div class="row text-left align-items-center">

                <div class="col-md-12 pl-30">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="why-choose-img">
                                <img src="../Advance_CSS/Images/why-choose-us.jpg" alt="Big image" style="margin-top: -54px;">
                            </div>
                        </div>
                        <div class="col-md-4" data-aos="fade-up">
                            <div class="theme-login-box">
                                <div class="theme-login-box-inner">
                                    <div class="theme-login-header">
                                        <h4 class="theme-login-title">Every time we provide best service</h4>
                                        <p style="font-size: 14px; color: #999;">Login here to your account as</p>
                                        <asp:Label ID="lblerror" Visible="false" Font-Size="10px" runat="server" Style="font-weight: 500; font-size: 13px; color: rgb(229 0 0); background-color: rgb(238 207 207); padding: 8px; margin-bottom: 6px; border-radius: 6px; display: block;"></asp:Label>
                                        <div class="loginas__tab">
                                            <div class="InputGroup">
                                                <asp:RadioButton ID="rdoAgent" runat="server" Text="AGENT" GroupName="LoginType" Checked="true" />
                                                <asp:RadioButton ID="rdoSupplier" runat="server" Text="SUPPLIER" GroupName="LoginType" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="theme-login-form">
                                        <div class="form-group theme-login-form-group">
                                            <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr" style="margin-right: 0;">
                                                <div class="">
                                                    <i class="theme-search-area-section-icon icofont-user-alt-7" style="line-height: 44px !important; color: #0c67ab;"></i>
                                                    <asp:TextBox runat="server" class="theme-search-area-section-input" placeholder="User Id" ID="txtAgencyUserName" Style="height: 45px !important; border-radius: 25px;"></asp:TextBox>
                                                   <%-- <asp:RequiredFieldValidator ID="AgencyUserNameRequired" runat="server" ControlToValidate="txtAgencyUserName"
                                                        ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
                                                <br />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group theme-login-form-group">
                                            <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr" style="margin-right: 0;">
                                                <div class="">
                                                    <i class="theme-search-area-section-icon icofont-key" style="line-height: 44px !important; color: #0c67ab;"></i>
                                                    <asp:TextBox runat="server" class="theme-search-area-section-input" TextMode="Password" placeholder="Password" ID="txtAgencyPassword" Style="height: 45px !important; border-radius: 25px;"></asp:TextBox>
                                                   <%-- <asp:RequiredFieldValidator ID="AgencyPasswordRequired" runat="server" ControlToValidate="txtAgencyPassword"
                                                        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
                                                <br />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:Button runat="server" ID="btnAgencyLogin" OnClick="btnAgencyLogin_Click" OnClientClick="return CheckUserDetails()" Text="Sign In" class="btn btn-uc btn-dark btn-block btn-lg-custom" Style="height: 50px; border-radius: 25px;" />
                                            </div>
                                        </div>
                                        <div>
                                            <p class="form__field--text" style="margin-bottom: -5px;">
                                                Forgot your password? <a href="/forget-password" class="login__form--reset--link">Reset Here </a>
                                            </p>
                                            <p class="form__field--text">
                                                Don’t have any account? <a href="/agency-registration" class="login__form--reset--link">Sign up here</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END COL -->
                    </div>
                    <!-- END ROW -->

                    <br />
                    <br />
                    <div class="row">
                        <div class="col-lg-3 col-xs-12">
                            <div class="single-choose mb-20">
                                <i class="icofont-card"></i>
                                <div class="choose-content">
                                    <h4>Easy Booking</h4>
                                    <p>We offer easy and convenient flight bookings with attractive offers.</p>
                                </div>
                            </div>
                        </div>
                        <!-- END COL -->

                        <div class="col-lg-3 col-xs-12 pr-0">
                            <div class="single-choose mb-20">
                                <i class="icofont-air-ticket"></i>
                                <div class="choose-content">
                                    <h4>Lowest Price</h4>
                                    <p>We ensure low rates on hotel reservation, holiday packages and on flight tickets.</p>
                                </div>
                            </div>
                        </div>
                        <!-- END COL -->

                        <div class="col-lg-3 col-xs-12">
                            <div class="single-choose">
                                <i class="icofont-price"></i>
                                <div class="choose-content">
                                    <h4>Exciting Deals</h4>
                                    <p>Enjoy exciting deals on flights, hotels, buses, car rental and tour packages.</p>
                                </div>
                            </div>
                        </div>
                        <!-- END COL -->

                        <div class="col-lg-3 col-xs-12 pr-0">
                            <div class="single-choose">
                                <i class="icofont-live-support"></i>
                                <div class="choose-content">
                                    <h4>24/7 Support</h4>
                                    <p>Get assistance 24/7 on any kind of travel related query. We are happy to assist you.</p>
                                </div>
                            </div>
                        </div>
                        <!-- END COL -->
                    </div>
                </div>
                <!-- END COL -->

            </div>
            <!-- END ROW -->
        </div>
        <!-- END CONTAINER -->
    </section>


    <div class="theme-hero-area">
        <div class="theme-hero-area-bg-wrap">
            <div class="theme-hero-area-bg-pattern theme-hero-area-bg-pattern-ultra-light" style="background-image: url(../Advance_CSS/img/patterns/travel/2.png);"></div>
            <div class="theme-hero-area-grad-mask"></div>
            <div class="theme-hero-area-inner-shadow theme-hero-area-inner-shadow-light"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="theme-page-section theme-page-section-xxl">
                <div class="container">
                    <div class="theme-page-section-header theme-page-section-header-white">
                        <h5 class="theme-page-section-title">Cities to Travel</h5>
                        <p class="theme-page-section-subtitle">The most searched cities in March</p>
                    </div>
                    <div class="theme-inline-slider row" data-gutter="10">
                        <div class="owl-carousel owl-carousel-nav-white" data-items="5" data-loop="true" data-nav="true">
                            <asp:Repeater ID="Package" runat="server">
                                <ItemTemplate>
                                    <div class="theme-inline-slider-item">
                                        <div class="banner _h-40vh _br-3 _bsh-xs banner-animate banner-animate-mask-in banner-animate-slow">
                                            <%--                    <div class="banner-bg" style="background-image:url(./img/400x500.png);"></div>--%>
                                 <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>


                                            <div class="banner-mask"></div>
                                            <a class="banner-link" href="#"></a>
                                            <div class="banner-caption _p-20 _bg-w banner-caption-bottom banner-caption-dark">
                                                <h5 class="banner-title _fs _fw-b"><%# Eval("pkg_Title") %></h5>
                                                <p class="banner-subtitle _fw-n _mt-5"><%# Eval("pkg_theme") %></p>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <%-- <div class="theme-inline-slider-item">
                  <div class="banner _h-40vh _br-3 _bsh-xs banner-animate banner-animate-mask-in banner-animate-slow">
                    <div class="banner-bg" style="background-image:url(./img/400x500.png);"></div>
                    <div class="banner-mask"></div>
                    <a class="banner-link" href="#"></a>
                    <div class="banner-caption _p-20 _bg-w banner-caption-bottom banner-caption-dark">
                      <h5 class="banner-title _fs _fw-b">San Francisco</h5>
                      <p class="banner-subtitle _fw-n _mt-5">Vehicula volutpat porta</p>
                    </div>
                  </div>
                </div>
                <div class="theme-inline-slider-item">
                  <div class="banner _h-40vh _br-3 _bsh-xs banner-animate banner-animate-mask-in banner-animate-slow">
                    <div class="banner-bg" style="background-image:url(./img/400x500.png);"></div>
                    <div class="banner-mask"></div>
                    <a class="banner-link" href="#"></a>
                    <div class="banner-caption _p-20 _bg-w banner-caption-bottom banner-caption-dark">
                      <h5 class="banner-title _fs _fw-b">Paris</h5>
                      <p class="banner-subtitle _fw-n _mt-5">Commodo mattis id</p>
                    </div>
                  </div>
                </div>
                <div class="theme-inline-slider-item">
                  <div class="banner _h-40vh _br-3 _bsh-xs banner-animate banner-animate-mask-in banner-animate-slow">
                    <div class="banner-bg" style="background-image:url(./img/400x500.png);"></div>
                    <div class="banner-mask"></div>
                    <a class="banner-link" href="#"></a>
                    <div class="banner-caption _p-20 _bg-w banner-caption-bottom banner-caption-dark">
                      <h5 class="banner-title _fs _fw-b">London</h5>
                      <p class="banner-subtitle _fw-n _mt-5">Adipiscing metus quis</p>
                    </div>
                  </div>
                </div>
                <div class="theme-inline-slider-item">
                  <div class="banner _h-40vh _br-3 _bsh-xs banner-animate banner-animate-mask-in banner-animate-slow">
                    <div class="banner-bg" style="background-image:url(./img/400x500.png);"></div>
                    <div class="banner-mask"></div>
                    <a class="banner-link" href="#"></a>
                    <div class="banner-caption _p-20 _bg-w banner-caption-bottom banner-caption-dark">
                      <h5 class="banner-title _fs _fw-b">New York</h5>
                      <p class="banner-subtitle _fw-n _mt-5">Donec nam placerat</p>
                    </div>
                  </div>
                </div>
                <div class="theme-inline-slider-item">
                  <div class="banner _h-40vh _br-3 _bsh-xs banner-animate banner-animate-mask-in banner-animate-slow">
                    <div class="banner-bg" style="background-image:url(./img/400x500.png);"></div>
                    <div class="banner-mask"></div>
                    <a class="banner-link" href="#"></a>
                    <div class="banner-caption _p-20 _bg-w banner-caption-bottom banner-caption-dark">
                      <h5 class="banner-title _fs _fw-b">Dubai</h5>
                      <p class="banner-subtitle _fw-n _mt-5">Curabitur habitasse porttitor</p>
                    </div>
                  </div>
                </div>
                <div class="theme-inline-slider-item">
                  <div class="banner _h-40vh _br-3 _bsh-xs banner-animate banner-animate-mask-in banner-animate-slow">
                    <div class="banner-bg" style="background-image:url(./img/400x500.png);"></div>
                    <div class="banner-mask"></div>
                    <a class="banner-link" href="#"></a>
                    <div class="banner-caption _p-20 _bg-w banner-caption-bottom banner-caption-dark">
                      <h5 class="banner-title _fs _fw-b">Tokyo</h5>
                      <p class="banner-subtitle _fw-n _mt-5">Feugiat lobortis tortor</p>
                    </div>
                  </div>
                </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="theme-page-section theme-page-section-xxl">
        <div class="container">
            <div class="theme-page-section-header">
                <h5 class="theme-page-section-title">Top Destinations</h5>
                <p class="theme-page-section-subtitle">The most searched countries in March</p>
            </div>
            <div class="row row-col-gap" data-gutter="10">

                <asp:Repeater ID="Top1" runat="server">
                    <ItemTemplate>
                        <div class="col-md-4 ">

                            <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
     <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>

                                <div class="banner-mask"></div>
                                <a class="banner-link" href="#"></a>
                                <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                    <h5 class="banner-title"><%# Eval("pkg_Title") %></h5>
                                    <p class="banner-subtitle"><%# Eval("pkg_theme") %></p>
                                </div>
                            </div>
                           
                        </div>
                    </ItemTemplate>
                </asp:Repeater>



                <asp:Repeater ID="Top2" runat="server">
                    <ItemTemplate>
                        <div class="col-md-8 ">
                            <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                           <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>
                                
                                <div class="banner-mask"></div>
                                <a class="banner-link" href="#"></a>
                                <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                    <h5 class="banner-title"><%# Eval("pkg_Title") %></h5>
                                    <p class="banner-subtitle"><%# Eval("pkg_theme") %></p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>


                </asp:Repeater>
                <asp:Repeater ID="Top3" runat="server">
                    <ItemTemplate>
                        <div class="col-md-3 ">
                            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
                                <%--                                <div class="banner-bg" style="background-image: url(./img/800x800.png);"></div>--%>
                           <div class="banner-bg" style='background-image: url(http://admin.amlintechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>

                               
                                <div class="banner-mask"></div>
                                <a class="banner-link" href="#"></a>
                                <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                                    <h5 class="banner-title"><%# Eval("pkg_Title") %></h5>
                                    <p class="banner-subtitle"><%# Eval("pkg_theme") %></p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <%-- <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url(./img/800x800.png);"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
              <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                <h5 class="banner-title">Canada</h5>
                <p class="banner-subtitle">Keep exploring</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url(./img/400x360.png);"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
              <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                <h5 class="banner-title">Portugal</h5>
                <p class="banner-subtitle">Europe's west coast</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url(./img/400x360.png);"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
              <div class="banner-caption _pt-100 banner-caption-bottom banner-caption-grad">
                <h5 class="banner-title">Malasia</h5>
                <p class="banner-subtitle">Truly asia</p>
              </div>
            </div>
          </div>--%>
            </div>
        </div>
    </div>

       <div class="theme-hero-area">
      <div class="theme-hero-area-bg-wrap">
        <div class="theme-hero-area-bg" style="background-image:url(Advance_CSS/img/index/AdobeStock_217638701-scaled.jpeg);"></div>
        <div class="theme-hero-area-mask theme-hero-area-mask-half"></div>
      </div>
      <div class="theme-hero-area-body">
        <div class="container">
          <div class="theme-page-section _p-0">
            <div class="row">
              <div class="col-md-12">
                <div class="theme-mobile-app">
                  <div class="row">
                      <div class="col-md-6">
                      <div class="theme-mobile-app-section">
                        <div class="theme-mobile-app-body">
                          <div class="theme-mobile-app-header">
                            <h2 class="theme-mobile-app-title" style="color:black">Download our app</h2>
                            <p class="theme-mobile-app-subtitle" style="color:gray">Book and manage your trips on the go. Be notified of our hot deals and offers.</p>
                          </div>
                          <ul class="theme-mobile-app-btn-list">
                            <li>
                              <a class="btn btn-dark theme-mobile-app-btn" href="#">
                                <i class="theme-mobile-app-logo">
                                  <img src="Advance_CSS/img/brands/apple.png" alt="Image Alternative text" title="Image Title">
                                </i>
                                <span>Download on
                                  <br>
                                  <span>App Store</span>
                                </span>
                              </a>
                            </li>
                            <li>
                              <a class="btn btn-dark theme-mobile-app-btn" href="#">
                                <i class="theme-mobile-app-logo">
                                  <img src="Advance_CSS/img/brands/play-market.png" alt="Image Alternative text" title="Image Title">
                                </i>
                                <span>Download on
                                  <br>
                                  <span>Play Market</span>
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="theme-mobile-app-section"></div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel3" aria-hidden="true" id="login-pop">
    <div class="modal-dialog modal-sm" style="width: 30%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="icofont-close-line-circled"></i></button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer hidden">
                <button type="button" class="btn btn-danger" id="pax-confirm" style="float: left; background: #ff0000">Cancel</button>


            </div>
        </div>
    </div>
</div>

<div class="large-12 medium-12 small-12" style="display: none;">
    <div class="col-md-12  userway "><i class="fa fa-user-circle" aria-hidden="true"></i></div>
    <div class="lft f16" style="display: none;">
        Login Here As
                <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                </asp:DropDownList>
    </div>
    <div class="clear1">
    </div>
    <div class="form-group has-success has-feedback">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
        </div>
    </div>
    <div class="form-group has-success has-feedback">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="font-size: 23px;"></i></span>
        </div>
    </div>
    <div class="large-4 medium-4 small-12 columns">
        <div class="clear1">
        </div>
        <br />
        <a href="/forget-password" style="color: #ff0000">Forgot Password</a>
    </div>
    <div class="clear">
    </div>
    <div class="clear">
    </div>
</div>

<script>
    function CheckUserDetails() {
        if ($("#<%=txtAgencyUserName.ClientID%>").val().trim() == "") {
            alert("Please enter user name");
            $("#<%=txtAgencyUserName.ClientID%>").css("border", "1px solid red").focus();
            return false;
        }
        else {
            $("#<%=txtAgencyUserName.ClientID%>").css("border", "1px solid #0f64a7");
        }
        if ($("#<%=txtAgencyPassword.ClientID%>").val().trim() == "") {
            alert("Please enter password");
            $("#<%=txtAgencyPassword.ClientID%>").css("border", "1px solid red").focus();
            return false;
        }
        else {
            $("#<%=txtAgencyPassword.ClientID%>").css("border", "1px solid #0f64a7");
        }
        $("#<%=btnAgencyLogin.ClientID%>").val("Processing...");
    }
</script>
