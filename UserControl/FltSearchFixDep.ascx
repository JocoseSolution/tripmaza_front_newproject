﻿<%@ Control Language="VB" AutoEventWireup="true" CodeFile="FltSearchFixDep.ascx.vb" Inherits="UserControl_FltSearchFixDep" %>

<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="uc1" TagName="HotelSearch" %>
<%@ Register Src="~/UserControl/HotelDashboard.ascx" TagPrefix="uc1" TagName="HotelDashboard" %>

<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />

<link href="../Advance_CSS/css/textbox.css" rel="stylesheet" />
<%--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>--%>
<%-- <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.css" rel="stylesheet" />--%>
<style type="text/css">

    #boxF {
        display: block;
    background: white;
    position: absolute;
    width: 464px;
    right: 0px;
    top: 70px;
    border-radius: 5px;
    -webkit-box-shadow: 0 8px 20px rgb(0 0 0 / 8%), 0 4px 6px rgb(0 0 0 / 12%);
    box-shadow: 0 8px 20px rgb(0 0 0 / 8%), 0 4px 6px rgb(0 0 0 / 12%);
    padding: 10px;
    }



    .chosen-container-single .chosen-single div b {
 display:none !important;
}
    .chosen-container-single .chosen-single span {
    display: block;
    overflow: hidden;
    margin-right: 52px;
    text-overflow: ellipsis;
    white-space: nowrap;
    margin: 0px 0px 0px 33px;
    
}

    .chosen-container-active.chosen-with-drop .chosen-single {
    border: 1px solid #aaa;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
    background-image: none;
     background-image: none; 
     -webkit-box-shadow: none; 
     box-shadow: none; 
}

    .chosen-container-active .chosen-single {
    border: none;
     -webkit-box-shadow: none; 
     box-shadow: none; 
}
.chosen-container-single .chosen-single {
    position: relative;
    display: block;
    overflow: hidden;
    padding: 16px 10px 0px 8px;
    height: 61px;
    width: 417px;
    border: 1px solid #fff;
    border-radius: 5px;
     background-color: none; 
     background: none; 
     background: none; 
     background-clip: padding-box; 
     -webkit-box-shadow: none; 
     box-shadow: none; 
    color: #444;
     text-decoration: none; 
     white-space: nowrap; 
    line-height: 24px;
    -webkit-box-shadow: 0 8px 20px rgb(0 0 0 / 8%), 0 4px 6px rgb(0 0 0 / 12%);
    box-shadow: 0 8px 20px rgb(0 0 0 / 8%), 0 4px 6px rgb(0 0 0 / 12%);
}

.chosen-container-single .chosen-drop {
    margin-top: -1px;
    border-radius: 0 0 4px 4px;
    background-clip: padding-box;
    width: 417px;
}

.chosen-container .chosen-results li.active-result {
    display: list-item;
    cursor: pointer;
    padding: 10px;
}


    label {
    font-weight: 600 !important;
    font-size: 12px;
    color: #474747;
}

    .btn.dropdown-toggle.selectpicker.btn-default {
        border: none;
    margin: 15px 10px 0px -20px;
    }

    option {
        margin-bottom: 5px;
        border-bottom: 1px solid #000;
    }

    .btn .dropdown-toggle .selectpicker .btn-default {
        border-bottom: 1px solid #000;
    border-top: none;
    border-left: none;
    border-right: none;
    }


    .theme-search-area-section-input {
    display: block;
    width: 100%;
    /*border: solid 1px #eee !important;*/
    outline: none !important;
    height: 60px !important;
    padding-left: 42.30769230769231px;
    font-size: 14px;
    background: #fff;
    /*-webkit-box-shadow: 0 8px 20px rgb(0 0 0 / 8%), 0 4px 6px rgb(0 0 0 / 12%);
    box-shadow: 0 8px 20px rgb(0 0 0 / 8%), 0 4px 6px rgb(0 0 0 / 12%);*/
}

</style>











<style type="text/css">
    .inp {
        width: 30px;
        text-align: center;
        color: #000;
        background: none;
        border: none;
    }

    .main_dv {
        width: 100%;
        float: left;
        margin-bottom: 13px;
    }

    .ttl_col {
        width: 35%;
        float: left;
    }

        .ttl_col span {
            font-size: 10px;
            color: #a3a2a2;
            display: block;
        }

        .ttl_col p {
            font-size: 13px;
            color: #000;
            display: block;
        }

    .dn_btn {
        cursor: pointer;
        background: #ff0000;
        float: right;
        text-align: center;
        padding: 4px 12px;
        display: block;
        color: #fff;
        font-size: 11px;
        border-radius: 3px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
    }

    .innr_pnl {
        width: 200px;
        position: relative;
        padding: 0px;
    }

    .dropdown-content-n2 {
        /*display:none;*/
        position: absolute;
        background-color: #fff;
        width: 200px;
        padding: 10px;
        box-shadow: 0 0 20px 0 rgba(0,0,0,0.45);
        z-index: 1;
        /*top: 114px;*/
        box-sizing: content-box;
        -webkit-box-sizing: content-box;
        right: 349px
    }

    .innr_pnl::before {
        content: '';
        position: absolute;
        left: 85%;
        top: -15px;
        width: 0;
        height: 0;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-bottom: 5px solid #fff;
        clear: both;
    }

    .clear {
        clear: both;
    }
</style>


<style>
    /*the container must be positioned relative:*/
    .custom-select {
        position: relative;
        font-family: Arial;
    }

        .custom-select select {
            display: none; /*hide original SELECT element:*/
        }

        .select-selected:after {
            position: absolute;
            content: "";
            top: 14px;
            right: 10px;
            width: 0;
            height: 0;
            border: 6px solid transparent;
            border-color: #000 transparent transparent transparent;
        }

        /*point the arrow upwards when the select box is open (active):*/
        .select-selected.select-arrow-active:after {
            border-color: transparent transparent #000 transparent;
            top: 7px;
        }

    /*style the items (options), including the selected item:*/
    .select-items div, .select-selected {
        color: #000;
        padding: 8px 16px;
        border: 1px solid transparent;
        border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
        cursor: pointer;
        user-select: none;
        font-size:18px;
    }

    /*style items (options):*/
    .select-items {
        position: absolute;
        background-color: #fff;
        top: 100%;
        left: 0;
        right: 0;
        z-index: 99;
        height: 145px;
        overflow: auto;
        box-shadow: 0 0 25px rgb(0 0 0 / 30%);
    }

    /*hide the items when the select box is closed:*/
    .select-hide {
        display: none;
    }

    .select-items div:hover, .same-as-selected {
        background-color: rgba(0, 0, 0, 0.1);
    }
</style>







<script type="text/javascript">

    $(document).ready(function () {
        $('.minusF').click(function () {
            var $input = $(this).parent().find('input');
            var $inputid = $input.attr('id');
            var count = parseInt($input.val()) - 1;
            if ($inputid != "Adult") {
                count = count <= 0 ? 0 : count;
            }
            else {
                count = count < 1 ? 1 : count;
            }
            $input.val(count);
            $input.change();
            AddAllPax();
            return false;
        });
        $('.plusF').click(function () {
            var $input = $(this).parent().find('input');
            let inpcount = parseInt($input.val()) + 1;
            $input.val(inpcount);
            $input.change();
            AddAllPax();
            return false;
        });

        AddAllPax();

        function AddAllPax() {
            let adultinp = $("#AdultF").val();
            let childinp = $("#ChildF").val();
            let infantinp = $("#InfantF").val();

            $("#sapnTotPaxF").val(parseInt(adultinp) + parseInt(childinp) + parseInt(infantinp) + " Traveler(s)");


        }
    });


</script>

<div>
    <div class="topwaysF" style="display: none">

        <div class="col-md-1 col-xs-4 nopad text-search">
            <label class="mail active">
                <input type="radio" style="display: none;" name="TripTypeF" value="rdbOneWayF" id="rdbOneWayF" checked="checked" />
                One Way</label>
        </div>
        <div class="col-md-2 col-xs-4 nopad text-search">
            <label class="mail">
                <input type="radio" name="TripTypeF" style="display: none;" value="rdbRoundTripF" id="rdbRoundTripF" />
                Round Trips</label>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var selector = '.topwaysF div label'; //mk
            $(selector).bind('click', function () {
                $(selector).removeClass('active');
                $(this).addClass('active');
            });

        });
    </script>

</div>


<div class="tab-content fxd" style="display:none;">

    <div class="tab-pane active" id="SearchAreaTabs-3">
        <div class="theme-search-area theme-search-area-stacked">
            <div class="theme-search-area-form">

                <div class="row">
                    <div class="col-md-4">
                        <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr">
                            <div class="theme-search-area-section-inner">
                                 <i class="theme-search-area-section-icon icofont-location-pin" style="z-index: 1;"></i>

                                        <%--<asp:DropDownList ID="Sector1" type="text" runat="server" class="selectpicker aumd-tb div theme-search-area-section-input" data-show-subtext="true" data-live-search="true">
                                            <asp:ListItem Value="">--Select FDD Sector--</asp:ListItem>
                                        </asp:DropDownList>--%>
                                        <asp:DropDownList ID="Sector" CssClass="theme-search-area-section-input" runat="server"></asp:DropDownList>
                                        <span class="validation"></span>
                                 

                              
                            </div>
                        </div>
                    </div>
                    <%-- <script type="text/javascript">
                         $('#<%=Sector.ClientID%>').chosen();
                     </script>--%>

                    <div class="form-group" style="display: none">
                        <label for="exampleInputEmail1">
                            Search Sector:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCityFD" class="form-control" placeholder="Enter Your Departure City" value="" onclick="this.value = '';" id="txtDepCityFD" />
                            <input type="hidden" id="hidtxtDepCityFD" name="hidtxtDepCityFD" value="TT" />
                        </div>
                    </div>

                    <div class="form-group" style="display: none">
                        <label for="exampleInputEmail1">
                            Leaving From:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-map-marker fa-lg yellow"></i>
                            </div>
                            <input type="text" name="txtDepCity1F" class="form-control" placeholder="Enter Your Departure City" value="New Delhi, India-Indira Gandhi Intl(DEL)" onclick="this.value = '';" id="txtDepCity1F" />
                            <input type="hidden" id="hidtxtDepCity1F" name="hidtxtDepCity1F" value="ZZZ,IN" />
                        </div>
                    </div>

                    <div class="col-md-6">
               <div class="row">
                    <div class="col-md-6 " id="TravellerF">
                        <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr quantity-selector" data-increment="Passengers">
                            <div class="theme-search-area-section-inner">
                               
                                      <i class="theme-search-area-section-icon icofont-users-alt-4"></i>
                                    <input class="theme-search-area-section-input" id="sapnTotPaxF" placeholder=" Traveller" type="text" disabled="disabled" />
                                   <%-- <label class="aumd-tb-label" for="sapnTotPaxF">Traveller(s)</label>--%>
                                    <span class="validation"></span>
                             
                            </div>
                        </div>
                        

                        <div id="boxF" style="display: none;">
                            <div id="div_Adult_Child_Infant" class="myText">
                                <div class="innr_pnl dflex">
                                    <div class="main_dv pax-limit">
                                        <label>
                                            <span>Adult</span>

                                        </label>
                                        <div class="number">
                                            <span class="minusF">-</span>
                                            <input type="text" class="inp" value="1" min="1" name="Adult" id="AdultF" />
                                            <span class="plusF">+</span>
                                        </div>

                                    </div>
                                    <div class="main_dv pax-limit">
                                        <label>
                                            <span>Child<span class="light-grey">(2+ 12 yrs)</span></span>

                                        </label>

                                        <div class="number">
                                            <span class="minusF">-</span>
                                            <input type="text" class="inp" value="0" min="0" name="Child" id="ChildF" />
                                            <span class="plusF">+</span>
                                        </div>

                                    </div>
                                    <div class="main_dv pax-limit">

                                        <label>
                                            <span>Infant <span class="light-grey">(below 2 yrs)</span></span>

                                        </label>

                                        <div class="number">
                                            <span class="minusF">-</span>
                                            <input type="text" class="inp" value="0" min="0" name="Infant" id="InfantF" />
                                            <span class="plusF Infant">+</span>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                     <div class="col-md-6">
                        <button type="button" id="btnOpenCal" value="Search Flight" class="theme-search-area-submit _mt-0 theme-search-area-submit-no-border theme-search-area-submit-curved " style="height: 60px;">Open Calander <i class="icofont-ui-calendar"></i></button>
                    </div>
                   </div>
                         </div>
                    <div class="col-md-6 hidden">
                        <button type="button" id="btnSearchF" value="Search Flight" class="btn btn-danger">Search Flight</button>
                    </div>
                   
                </div>
               


                <%--   <div class="row" data-gutter="none">
                    <div class="col-md-12">
                        <input type="checkbox" class="chkNonstop" name="chkNonstop" id="chkNonstop" value="Y" style="display: none" />
                        <button type="button" id="btnSearchF" value="Search" class="theme-search-area-submit _mt-0 theme-search-area-submit-no-border theme-search-area-submit-curved ">Search</button>

                    </div>
                </div>--%>
            </div>
        </div>
    </div>


    





    <div class="onewayss col-md-5 nopad text-search mltcs" style="display: none">
        <div class="form-group">
            <label for="exampleInputEmail1">
                Going To:</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-map-marker fa-lg yellow"></i>
                </div>
                <input type="text" name="txtArrCity1F" onclick="this.value = '';" id="txtArrCity1F" value="New Delhi, India-Indira Gandhi Intl(DEL)" class="form-control" placeholder="Enter Your Destination City" />
                <input type="hidden" id="hidtxtArrCity1F" name="hidtxtArrCity1F" value="ZZZ,IN" />
            </div>
        </div>
    </div>
    <div class="col-md-2 nopad text-search mrgs" id="oneF" style="display: none">
        <div class="form-group">
            <label for="exampleInputEmail1">
                Depart Date:</label>
            <div class="input-group">

                <div class="input-group-addon">
                    <i class="fa fa-calendar yellow"></i>
                </div>
                <input type="text" class="form-control" placeholder="dd/mm/yyyy" name="txtDepDateF" id="txtDepDateF" value=""
                    readonly="readonly" />
                <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDateF" value="" />

            </div>
        </div>
    </div>
    <div class="col-md-2 nopad text-search mrgs" id="ReturnF" style="display: none">
        <div class="form-group" id="trRetDateRowF">
            <label for="exampleInputEmail1">
                Return Date:</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar yellow"></i>
                </div>
                <input type="text" placeholder="dd/mm/yyyy" name="txtRetDateF" id="txtRetDateF" class=" form-control" value=""
                    readonly="readonly" />
                <input type="hidden" name="hidtxtRetDateF" id="hidtxtRetDateF" value="" />


            </div>
        </div>
    </div>
    <div style="display: none;" id="twoF">
        <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity2F">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtDepCity2F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity2F" />
                    <input type="hidden" id="hidtxtDepCity2F" name="hidtxtDepCity2F" value="" />
                </div>
            </div>
        </div>

        <div class="onewayss col-md-4 nopad text-search mltcs">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtArrCity2F" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity2F" />
                    <input type="hidden" id="hidtxtArrCity2F" name="hidtxtArrCity2F" value="" />
                </div>
            </div>
        </div>
        <div class="col-md-2 nopad text-search mrgs" id="DivArrCity2F">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar yellow"></i>
                    </div>
                    <input type="text" name="txtDepDate2" id="txtDepDate2F" class=" form-control" placeholder="dd/mm/yyyy" readonly="readonly" value="" />
                    <input type="hidden" name="hidtxtDepDate2F" id="hidtxtDepDate2F" value="" />
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;" id="threeF">

        <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity3F">
            <div class="form-group">

                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtDepCity3F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity3F" />
                    <input type="hidden" id="hidtxtDepCity3F" name="hidtxtDepCity3F" value="" />
                </div>
            </div>
        </div>
        <div class="onewayss col-md-4 nopad text-search mltcs">
            <div class="form-group">

                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtArrCity3F" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity3F" />
                    <input type="hidden" id="hidtxtArrCity3F" name="hidtxtArrCity3F" value="" />
                </div>
            </div>
        </div>
        <div class="col-md-2 nopad text-search mrgs" id="DivArrCity3F">
            <div class="form-group">

                <div class="input-group">

                    <div class="input-group-addon">
                        <i class="fa fa-calendar yellow"></i>
                    </div>
                    <input type="text" name="txtDepDate3F" id="txtDepDate3F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                    <input type="hidden" name="hidtxtDepDate3F" id="hidtxtDepDate3F" value="" />
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;" id="four">
        <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity4F">
            <div class="form-group">

                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtDepCity4F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity4F" />
                    <input type="hidden" id="hidtxtDepCity4F" name="hidtxtDepCity4F" value="" />
                </div>
            </div>
        </div>
        <div class="onewayss col-md-4 nopad text-search mltcs">
            <div class="form-group">

                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtArrCity4F" class="form-control" placeholder="Enter Your Destination City" id="txtArrCity4F" />
                    <input type="hidden" id="hidtxtArrCity4F" name="hidtxtArrCity4F" value="" />
                </div>
            </div>
        </div>
        <div class="col-md-2 nopad text-search mrgs" id="DivArrCity4F">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar yellow"></i>
                    </div>
                    <input type="text" name="txtDepDate4F" id="txtDepDate4F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                    <input type="hidden" name="hidtxtDepDate4F" id="hidtxtDepDate4F" value="" />
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;" id="five">
        <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity5">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtDepCity5F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity5F" />
                    <input type="hidden" id="hidtxtDepCity5F" name="hidtxtDepCity5F" value="" />
                </div>
            </div>
        </div>
        <div class="onewayss col-md-4 nopad text-search mltcs">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtArrCity5F" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity5F" />
                    <input type="hidden" id="hidtxtArrCity5F" name="hidtxtArrCity5F" value="" />
                </div>
            </div>
        </div>
        <div class="col-md-2 nopad text-search mrgs" id="DivArrCity5F">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar yellow"></i>
                    </div>
                    <input type="text" name="txtDepDate5" id="txtDepDate5F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                    <input type="hidden" name="hidtxtDepDate5F" id="hidtxtDepDate5F" value="" />
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;" id="six">
        <div class="onewayss col-md-4 nopad text-search mltcs" id="DivDepCity6F">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtDepCity6F" class="form-control" placeholder="Enter Your Departure City" id="txtDepCity6F" />
                    <input type="hidden" id="hidtxtDepCity6F" name="hidtxtDepCity6" value="" />
                </div>
            </div>
        </div>

        <div class="onewayss col-md-4 nopad text-search mltcs">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-map-marker fa-lg yellow"></i>
                    </div>
                    <input type="text" name="txtArrCity6F" class="form-control" placeholder="Enter Your Destination City" onclick="this.value = '';" id="txtArrCity6F" />
                    <input type="hidden" id="hidtxtArrCity6F" name="hidtxtArrCity6F" value="" />
                </div>
            </div>
        </div>
        <div class="col-md-2 nopad text-search mrgs" id="ArrCity6F">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar yellow"></i>
                    </div>
                    <input type="text" name="txtDepDate6F" id="txtDepDate6F" class="form-control" placeholder="dd/mm/yyyy" readonly="readonly" />
                    <input type="hidden" name="hidtxtDepDate6F" id="hidtxtDepDate6F" value="" />
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="row col-md-5 col-xs-12 pull-right" id="addF" style="display: none">
        <div class="col-md-4 col-xs-4 text-search text-right">
            <a id="plusF" class="pulse text-search">Add City</a>
        </div>
        <div class="col-md-4 col-xs-4 text-search  text-right">
            <a id="minusF" class="pulse text-search">Remove City</a>
        </div>
    </div>


    <div class="row" style="display: none">
        <div class="text-search col-md-5 col-xs-12" style="padding-bottom: 10px; cursor: pointer;" id="advtravelF">Advanced options <i class="fa fa-chevron-right" aria-hidden="true"></i></div>
        <div class="col-md-12 advopt" id="advtravelssF" style="display: none; overflow: auto;">
            <div class="col-md-3 nopad text-search">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Airlines</label>
                    <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirlineF" />
                    <input type="hidden" id="hidtxtAirlineF" name="hidtxtAirlineF" value="" />

                </div>
            </div>

            <div class="col-md-3 col-xs-12 text-search">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Class Type</label>
                    <select name="CabinF" class="form-control" id="CabinF">
                        <option value="" selected="selected">--All--</option>
                        <option value="C">Business</option>
                        <option value="Y">Economy</option>
                        <option value="F">First</option>
                        <option value="W">Premium Economy</option>
                    </select>

                </div>
            </div>
        </div>
    </div>







    <div class="clear1"></div>
    <div class="col-md-3 col-xs-12 text-search" id="trAdvSearchRowF" style="display: none">
        <div class="lft ptop10">
            All Fare Classes
        </div>
        <div class="lft mright10">

            <input type="checkbox" name="chkAdvSearchF" id="chkAdvSearchF" value="True" />
        </div>
        <div class="large-4 medium-4 small-12 columns">
            Gds Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="GDS_RTFF" id="GDS_RTFF" value="True" />
                                </span>
        </div>

        <div class="large-4 medium-4 small-12 columns">
            Lcc Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="LCC_RTFF" id="LCC_RTFF" value="True" />
                                </span>
        </div>

    </div>
</div>


<%--<script type="text/javascript">
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });
</script>--%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>



<script>
    var x, i, j, l, ll, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    l = x.length;
    for (i = 0; i < l; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        ll = selElmnt.length;
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < ll; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h, sl, yl;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this.parentNode.previousSibling;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        yl = y.length;
                        for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, xl, yl, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        xl = x.length;
        yl = y.length;
        for (i = 0; i < yl; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < xl; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);
</script>

<script>
    $(document).ready(function () {
        $("#advtravelF").click(function () {
            $("#advtravelssF").slideToggle();
        });


        $("#TravellerF").click(function () {
            $("#boxF").toggle();
        });
      

    });
</script>

<script type="text/javascript">
    function plusF() {
        document.getElementById("sapnTotPaxF").value = (parseInt(document.getElementById("AdultF").value.split(' ')[0]) + parseInt(document.getElementById("ChildF").value.split(' ')[0]) + parseInt(document.getElementById("InfantF").value.split(' ')[0])).toString() + ' Traveller';
    }
    plusF();
</script>
<script type="text/javascript">
    var myDate = new Date();
    var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    document.getElementById("txtDepDateF").value = currDate;
    document.getElementById("hidtxtDepDateF").value = currDate;
    document.getElementById("txtRetDateF").value = currDate;
    document.getElementById("hidtxtRetDateF").value = currDate;
    var UrlBase = '<%=ResolveUrl("~/") %>';
</script>

<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Search3F.js") %>"></script>

<script type="text/javascript">


    $(function () {
        $("#CB_GroupSearchF").click(function () {

            if ($(this).is(":checked")) {
                // $("#box").hide();
                $("#TravellerF").hide();
                $("#rdbRoundTripF").attr("checked", true);
                $("#rdbOneWayF").attr("checked", false);

            } else {
                // $("#box").show();
                $("#TravellerF").show();
            }
        });
    });

</script>


