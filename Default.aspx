﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>


    <div class='' style='padding: 5px;'><span style='font-weight: 600; font-size: 17px;'>Return Flight</span></div>
   

                <div class='flt-details'>
                    "
                <div class='fl width100 padTB10'>
                    "
                <div class='fl width100 padLR10 padTB10' style='font-size: 13px; background-color: #f2f2f2; font-weight: bold; padding: 5px 10px; box-shadow: 0 1px 4px 0 rgb(0 0 0/14%); border-radius: 3px; margin-bottom: 10px;'><span class='fl mobdn padL10'>" & IBDS.Tables(0).Rows(i)("DepartureLocation") & " <i class='icofont-long-arrow-right'></i>" & IBDS.Tables(0).Rows(i)("ArrivalLocation") & "  on  " & IBDS.Tables(0).Rows(i)("Departure_Date") & "</span><span class='fr'>" & IBDS.Tables(0).Rows(i)("AdtFareType") & "</span></div>
                    "


                <div class='borderAll posRel whiteBg brRadius5 fl width100 fl padTB10'>
                    "
                <div class='bkHalfCircleTop mobdn bkHalfCirctop posAbs'></div>
                    "
                <div class='col-md-2'><span class='db padB5'>
                    <img alt='flight-icon' class='flightImagesNew' src='../Airlogo/sm" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif' /></span><span class='db greyLt ico12'>" & IBDS.Tables(0).Rows(i)("AirlineName") & " (" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & IBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</span><span class='db greyLter padT2 ico11'>" & RBD & "</span><span class='db greyLter padT2 ico11'>" & CABIN & "</span></div>
                    "


                <div class='col-md-3 col-sm-3 col-xs-4 padL20'>
                    "
                <span class='co12 db grey'>" & IBDS.Tables(0).Rows(i)("Departure_Date") & "</span>"
                <span class='ico20 db padT5 padB10'>" & IBDS.Tables(0).Rows(i)("DepartureLocation") & "&nbsp;<span class='fb mobdb'>" & MultiValueFunction_Dom(IBDS.Tables(0), "Depall", i) & "</span></span>"

                <span class='ico13 db greyLter lh1-2 mobdn'>" & IBDS.Tables(0).Rows(i)("DepartureTerminal") & "</span>"
                </div>
                    "




                <div class='col-md-4 col-sm-4 col-xs-4 mobdn txtCenter padLR20 padT20'>
                    "
                <span class='db ico20 backgroundLn'><i class='oval-2 fl'></i><span class='padLR10 whiteBg'>" & IBDS.Tables(0).Rows(i)("Tot_Dur") & "h</span><i class='icofont-airplane icofont-rotate-90 fr ico22 blue icon-flight_line1' style='margin-top: 5px;'></i></span>"
                <span class='ico12 greyLter padT5 txtCap'>" & IBDS.Tables(0).Rows(i)("Stops") & "</span>"
                </div>
                    "




                <div class='col-md-3 col-sm-3 col-xs-4 padL10 mobTxtRight' style='text-align: right;'>
                    "
                <span class='co12 db grey'>" & IBDS.Tables(0).Rows(i)("Arrival_Date") & "</span>"
                <span class='ico20 db padT5 padB10'>" & MultiValueFunction_Dom(IBDS.Tables(0), "Arrall", i) & "&nbsp;<span class='fb mobdb'>" & IBDS.Tables(0).Rows(i)("ArrivalLocation") & "</span></span>"

                <span class='ico13 db greyLter lh1-2 mobdn'>" & IBDS.Tables(0).Rows(i)("ArrivalTerminal") & "</span>"

                </div>
                    "

                <div class='bkHalfCircle bkHalfCircbot mobdn posAbs'></div>
                    "
                </div>
                    "
                </div>
                    "
                </div>
    "
</body>
</html>
